# blogs

#### 介绍
The back-end code for the blog

#### 软件架构
软件架构说明


#### 安装教程

1.  blogs Java代码
2.  devtools 代码工具
3.  vue-admin 后台管理前端

#### 使用说明

#### 	工具目录：Java代码对应的resouce资源目录下

1.  ###redis : 
    redis-server.exe 双击启动
    
2.  ###swagger：
    http://localhost:8808/doc.html
    
3. ```
   ###ImageServer
   1.安装serv-u 
   2.破解serv-u
       安装目录里面，有对应的破解方法
   3.配置serv-u（Ftp服务器）
       1.配置一个域
       2.创建一个用户
           1>.配置用户的存贮的根目录
           2>.配置用户的访问目录权限
   4.数据库配置Ftp服务器(也就是图片服务器)
       表：b_sys_ftp
       ftp_username：对应上面配置serv-u 用户的用户名
       ftp_password：对应上面配置serv-u 用户的密码
       ftp_port：22（端口固定写死）
       ftp_ip：本地ip(安装serv-u的电脑ip，默认：localhost)
   5.nginx 配置
       1.配置server 端口，默认：8809
       2.配置root serv-u 用户的存储目录
   ```


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
