package io.renren.utils;
import org.apache.velocity.app.event.ReferenceInsertionEventHandler;
import org.apache.velocity.runtime.RuntimeServices;
import org.apache.velocity.util.RuntimeServicesAware;
public class VelocityEscapeHtmlOutput implements ReferenceInsertionEventHandler, RuntimeServicesAware {
	private RuntimeServices rs = null;

	public Object referenceInsert(String reference, Object value) {
		// 呵呵，这里... 凡是以 $#开头的reference，其值直接返回(^_^)
		if (reference.startsWith("$#")) {
			return value;
		}
		// 其它默认转义
		return escapeHtml(value);
	}

	public void setRuntimeServices(RuntimeServices rs) {
		this.rs = rs;
	}

	protected RuntimeServices getRuntimeServices() {
		return this.rs;
	}
	
	/**
	 * 转义HTML字符串
	 * 
	 * @param str
	 * @return
	 */
	private static Object noescapeHtml(Object value) {
		if (value == null) {
			return null;
		}
		
		return value;
	}

	/**
	 * 转义HTML字符串
	 * 
	 * @param str
	 * @return
	 */
	private static Object escapeHtml(Object value) {
		if (value == null) {
			return null;
		}

		if (!(value instanceof String)) {
			return value;
		}

		String str = value.toString();
		StringBuilder sb = new StringBuilder(str.length() + 30);

		for (int i = 0, len = str.length(); i < len; i++) {
			char c = str.charAt(i);
			// 去除不可见字符
			if ((int) c < 32) {
				continue;
			}

			switch (c) {
			case '<':
				sb.append("&#60;");
				break;
			case '>':
				sb.append("&#62;");
				break;
			case '&':
				sb.append("&#38;");
				break;
			case '"':
				sb.append("&#34;");
				break;
			case '\'':
				sb.append("&#39;");
				break;
			case '/':
				sb.append("&#47;");
				break;
			default:
				sb.append(c);
				break;
			}
		}

		str = null;

		return sb.toString();
	}
}