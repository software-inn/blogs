import { getMenuAll } from '@/api/menu'
import Layout from '@/layout'

const getDefaultState = () => {
  return {
    menuList: [],
    menuRouters: []
  }
}

const state = getDefaultState()

const mutations = {
  RE_SETSTATE: (state) => {
    Object.assign(state, getDefaultState())
  },
  SET_MENU: (state, menuList) => {
    state.menuList = menuList
  },
  SET_ROUTERS: (state, menuList) => {
    const routers = [] // 定义一个空数组，这个是用来装真正路由数据的
    menuList.forEach((m, i) => {
      if (m.parentId === 0) {
        menuList[i].fullPath = m.path
        const module = {
          path: m.path,
          redirect: m.path + '/index',
          hidden: m.display === 1,
          component: Layout,
          meta: { id: m.id, title: m.title, fullPath: m.path, icon: m.icon },
          alwaysShow: false
        }
        routers.push(module)
      }
    })
    convertTree(routers, menuList) // 用递归填充
    // 设置404 页面 必须放在最后一行
    routers.push({ path: '*', redirect: '/404', hidden: true })
    state.menuRouters = routers
  }
}

// 定义一个递归方法
function convertTree(routers, menuList) {
  routers.forEach(r => {
    menuList.forEach((m, i) => {
      if (m.parentId === r.meta.id) {
        r.alwaysShow = true
        if (!r.children) r.children = []
        m.fullPath = r.meta.fullPath + m.path
        const menu = {
          path: m.path,
          hidden: m.display === 1,
          component: (resolve) => require(['@/views' + r.meta.fullPath + m.path + '/index'], resolve),
          meta: { id: m.id, title: m.title, fullPath: r.meta.fullPath + m.path, icon: m.icon },
          alwaysShow: false
        }
        if (r.children.length === 0) {
          menu.path = 'index'
        }
        r.children.push(menu)
      }
    })
    if (r.children) convertTree(r.children, menuList)
  })
}

const actions = {

  getMenu: function({ commit }) {
    return new Promise((resolve, reject) => {
      getMenuAll({}).then(res => {
        commit('SET_MENU', res.data)
        commit('SET_ROUTERS', res.data)
        resolve(res)
      }).catch(err => {
        reject(err)
      })
    })
  }

}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
