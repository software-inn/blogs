import request from '@/utils/request'

export function getArticleList(data) {
  return request({
    url: '/article/list',
    method: 'post',
    data
  })
}

export function fetchArticle(id) {
  return request({
    url: '/article/get/' + id,
    method: 'get'
  })
}

export function saveArticle(data) {
  return request({
    url: '/article/save',
    method: 'post',
    data
  })
}

export function editArticle(data) {
  return request({
    url: '/article/editSave',
    method: 'post',
    data
  })
}

