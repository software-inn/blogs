import request from '@/utils/request'

export function getLog(data) {
  return request({
    url: '/sysLog/list',
    method: 'POST',
    data
  })
}

export function del(id) {
  return request({
    url: '/sysLog/delete',
    method: 'POST',
    data: { id: id }
  })
}
