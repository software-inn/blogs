import request from '@/utils/request'

export function getList(data) {
  return request({
    url: '/articleClass/list',
    method: 'POST',
    data
  })
}

export function get(id) {
  return request({
    url: '/articleClass/get/' + id,
    method: 'get'
  })
}

export function save(data) {
  return request({
    url: '/articleClass/save',
    method: 'POST',
    data
  })
}

export function edit(data) {
  return request({
    url: '/articleClass/editSave',
    method: 'POST',
    data
  })
}

export function del(id) {
  return request({
    url: '/articleClass/delete',
    method: 'POST',
    data: { id: id }
  })
}

export function getAll(data) {
  return request({
    url: '/articleClass/getAll',
    method: 'POST',
    data
  })
}
