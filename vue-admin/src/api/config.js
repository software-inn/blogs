import request from '@/utils/request'

export function save(data) {
  return request({
    url: '/sysConfig/save',
    method: 'POST',
    data
  })
}

export function get() {
  return request({
    url: '/sysConfig/get/' + 1,
    method: 'GET'
  })
}
