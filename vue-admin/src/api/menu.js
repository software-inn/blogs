import request from '@/utils/request'

export function getMenu(data) {
  return request({
    url: '/sysMenu/list',
    method: 'post',
    data
  })
}

export function getMenuAll(data) {
  return request({
    url: '/sysMenu/all',
    method: 'post',
    data
  })
}

export function get(id) {
  return request({
    url: '/sysMenu/get/' + id,
    method: 'get'
  })
}

export function save(data) {
  return request({
    url: '/sysMenu/save',
    method: 'POST',
    data
  })
}

export function edit(data) {
  return request({
    url: '/sysMenu/editSave',
    method: 'POST',
    data
  })
}

export function del(id) {
  return request({
    url: '/sysMenu/delete',
    method: 'POST',
    data: { id: id }
  })
}
