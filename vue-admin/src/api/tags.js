import request from '@/utils/request'

export function getList(data) {
  return request({
    url: '/tags/list',
    method: 'POST',
    data
  })
}

export function get(id) {
  return request({
    url: '/tags/get/' + id,
    method: 'get'
  })
}

export function save(data) {
  return request({
    url: '/tags/save',
    method: 'POST',
    data
  })
}

export function edit(data) {
  return request({
    url: '/tags/editSave',
    method: 'POST',
    data
  })
}

export function del(id) {
  return request({
    url: '/tags/delete',
    method: 'POST',
    data: { id: id }
  })
}

export function getTagsAll(data) {
  return request({
    url: '/tags/all',
    method: 'POST',
    data
  })
}
