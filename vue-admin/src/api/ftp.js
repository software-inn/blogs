import request from '@/utils/request'

export function save(data) {
  return request({
    url: '/sysFtp/save',
    method: 'POST',
    data
  })
}

export function get() {
  return request({
    url: '/sysFtp/get/' + 1,
    method: 'GET'
  })
}
