/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50728
Source Host           : localhost:3306
Source Database       : blogs

Target Server Type    : MYSQL
Target Server Version : 50728
File Encoding         : 65001

Date: 2021-01-05 22:32:33
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `b_article`
-- ----------------------------
DROP TABLE IF EXISTS `b_article`;
CREATE TABLE `b_article` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '域模型id，这里为自增类型',
  `add_time` datetime DEFAULT NULL COMMENT '添加时间，这里为长时间格式',
  `del_state` int(11) DEFAULT '0' COMMENT '是否删除,默认为0未删除，-1表示删除状态',
  `hot` int(11) DEFAULT '0' COMMENT '是否热门显示,默认为0 否 ，1是',
  `title` varchar(255) DEFAULT NULL COMMENT '标题',
  `cover_image_id` bigint(20) DEFAULT NULL COMMENT '封面图片',
  `comment` varchar(255) DEFAULT NULL COMMENT '描述',
  `content` longtext COMMENT '内容',
  `like` int(11) DEFAULT '0' COMMENT '赞',
  `tread` int(11) DEFAULT '0' COMMENT '踩',
  `collect` int(11) DEFAULT '0' COMMENT '收藏',
  `class_id` bigint(20) DEFAULT '0' COMMENT '分类',
  `vista` int(11) DEFAULT '0' COMMENT '访问量',
  `level` int(11) DEFAULT '0' COMMENT '推荐指数',
  `status` int(11) DEFAULT '0' COMMENT '1发布0草稿',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='文章';

-- ----------------------------
-- Records of b_article
-- ----------------------------
INSERT INTO `b_article` VALUES ('1', '2020-12-15 00:03:16', '0', '1', '有道无术8888888', null, '有道无术，上术可求', '<p>有道无术，上术可求</p>', '10', '0', '10', '1', '1000', '0', null);
INSERT INTO `b_article` VALUES ('2', null, '0', '0', 'aaaaaaaaaaaaaaaaa', null, 'sssssssssssssssssss', null, '0', '0', '0', '2', '0', '5', null);
INSERT INTO `b_article` VALUES ('5', '2020-12-25 08:04:44', '0', '0', '张国荣', '34', '张国荣', '<p><img src=\"http://localhost:8809/upload/article/bfec86c3-7940-4b31-ba0e-93c60a1a2bbc.jpg\"></p>', '0', '0', '0', '1', '0', '5', null);

-- ----------------------------
-- Table structure for `b_article_class`
-- ----------------------------
DROP TABLE IF EXISTS `b_article_class`;
CREATE TABLE `b_article_class` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '域模型id，这里为自增类型',
  `add_time` datetime DEFAULT NULL COMMENT '添加时间，这里为长时间格式',
  `del_state` int(11) DEFAULT '0' COMMENT '是否删除,默认为0未删除，-1表示删除状态',
  `display` int(11) DEFAULT '0' COMMENT '是否显示,默认为0 显示 ，1隐藏',
  `icon_id` bigint(20) DEFAULT NULL COMMENT '图标id',
  `comment` varchar(255) DEFAULT NULL COMMENT '描述',
  `sequence` int(11) DEFAULT '0' COMMENT '排序，从小到大',
  `class_name` varchar(255) DEFAULT NULL COMMENT '分类名称',
  `class_type` int(11) DEFAULT NULL COMMENT '类型，用于扩展',
  `parent_id` bigint(20) DEFAULT NULL COMMENT '父级id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='分类';

-- ----------------------------
-- Records of b_article_class
-- ----------------------------
INSERT INTO `b_article_class` VALUES ('1', '2020-12-21 19:39:03', '0', '0', '18', 'vue', '0', 'vue', null, '3');
INSERT INTO `b_article_class` VALUES ('2', '2020-12-18 08:00:00', '0', '0', '20', null, '0', 'aasaa', null, '1');
INSERT INTO `b_article_class` VALUES ('3', '2020-12-09 08:00:00', '1', '0', null, null, '0', '8888', null, '0');
INSERT INTO `b_article_class` VALUES ('4', null, '1', '0', null, '啊啊啊啊啊啊', '0', '啊啊啊', null, '3');
INSERT INTO `b_article_class` VALUES ('5', '2020-12-25 08:00:00', '1', '1', null, '啊啊啊啊啊啊', '0', '啊啊啊', null, '2');
INSERT INTO `b_article_class` VALUES ('6', '2020-12-09 08:00:00', '1', '0', '19', '88888888888', '0', 'ddddddddddd', null, '0');

-- ----------------------------
-- Table structure for `b_article_tags`
-- ----------------------------
DROP TABLE IF EXISTS `b_article_tags`;
CREATE TABLE `b_article_tags` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '域模型id，这里为自增类型',
  `add_time` datetime DEFAULT NULL COMMENT '添加时间，这里为长时间格式',
  `del_state` int(11) DEFAULT '0' COMMENT '是否删除,默认为0未删除，-1表示删除状态',
  `article_id` bigint(20) DEFAULT NULL COMMENT '文章id',
  `tags_id` bigint(20) DEFAULT NULL COMMENT '标签id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COMMENT='文章与标签中间表';

-- ----------------------------
-- Records of b_article_tags
-- ----------------------------
INSERT INTO `b_article_tags` VALUES ('7', '2020-12-25 00:30:51', '0', '5', '2');
INSERT INTO `b_article_tags` VALUES ('8', '2020-12-25 00:39:50', '0', '1', '1');
INSERT INTO `b_article_tags` VALUES ('9', '2020-12-25 00:39:50', '0', '1', '2');
INSERT INTO `b_article_tags` VALUES ('10', '2020-12-25 00:40:06', '0', '2', '1');

-- ----------------------------
-- Table structure for `b_attachment`
-- ----------------------------
DROP TABLE IF EXISTS `b_attachment`;
CREATE TABLE `b_attachment` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '域模型id，这里为自增类型',
  `add_time` datetime DEFAULT NULL COMMENT '添加时间，这里为长时间格式',
  `del_state` int(11) DEFAULT '0' COMMENT '是否删除,默认为0未删除，-1表示删除状态',
  `ext` varchar(255) DEFAULT NULL COMMENT ' 扩展名，不包括.',
  `height` int(11) DEFAULT NULL COMMENT ' 高度',
  `info` varchar(255) DEFAULT NULL COMMENT ' 附件说明',
  `name` varchar(255) DEFAULT NULL COMMENT ' 附件名称',
  `path` varchar(255) DEFAULT NULL COMMENT ' 存放路径',
  `size` decimal(12,2) DEFAULT NULL COMMENT ' 附件大小',
  `width` int(11) DEFAULT NULL COMMENT ' 宽度',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8 COMMENT='图片表';

-- ----------------------------
-- Records of b_attachment
-- ----------------------------
INSERT INTO `b_attachment` VALUES ('1', '2020-12-12 23:03:16', '0', '1', '1', '1', '70cFvnSh_Q1YnxGkpoWK1HF6hhy/it/u=3531671336,3780835954&fm=26&gp=0.jpg', 'https://dss2.bdstatic.com/', '1.00', '1');
INSERT INTO `b_attachment` VALUES ('2', '2020-12-13 00:56:47', '0', '2', '1', '1', '70cFvnSh_Q1YnxGkpoWK1HF6hhy/it/u=3531671336,3780835954&fm=26&gp=0.jpg', 'https://dss2.bdstatic.com/', '1.00', '1');
INSERT INTO `b_attachment` VALUES ('3', '2020-12-16 00:00:21', '0', 'jpg', '1030', null, '06f535f7-9f23-4034-9669-3d408403574b.jpg', '/upload/article', '106045.00', '1455');
INSERT INTO `b_attachment` VALUES ('4', '2020-12-16 00:29:47', '0', 'jpg', '1030', null, '68b931bb-bf06-4261-a7a3-8dc242e37f83.jpg', '/upload/article', '106045.00', '1455');
INSERT INTO `b_attachment` VALUES ('5', '2020-12-16 00:32:21', '0', 'jpg', '1030', null, '84c090e9-48d4-4929-968d-63d38f10ace1.jpg', '/upload/article', '106045.00', '1455');
INSERT INTO `b_attachment` VALUES ('6', '2020-12-16 00:40:22', '0', 'jpg', '1030', null, 'fb920c8c-085e-4e6f-8a0b-50079f4c207f.jpg', '/upload/article', '106045.00', '1455');
INSERT INTO `b_attachment` VALUES ('7', '2020-12-16 00:41:13', '0', 'jpg', '1030', null, '0111d7be-eded-4928-8969-c99eb7d96963.jpg', '/upload/article', '106045.00', '1455');
INSERT INTO `b_attachment` VALUES ('8', '2020-12-16 01:06:17', '0', 'jpg', '1030', null, '45da15cf-398b-486a-aea5-52ccda1286d0.jpg', '/upload/article', '106045.00', '1455');
INSERT INTO `b_attachment` VALUES ('9', '2020-12-16 01:07:44', '0', 'jpg', '1030', null, 'caddde28-2966-42bd-9666-ea5711a4f397.jpg', '/upload/article', '106045.00', '1455');
INSERT INTO `b_attachment` VALUES ('10', '2020-12-16 23:20:05', '0', 'jpg', '1030', null, '5c2d0355-a919-4779-b23c-7584372506fe.jpg', '/upload/article', '106045.00', '1455');
INSERT INTO `b_attachment` VALUES ('11', '2020-12-22 00:30:15', '0', 'jpg', '1030', null, '38e0bdec-31cf-43cb-8cc5-ba4630ad0232.jpg', '/upload/article', '106045.00', '1455');
INSERT INTO `b_attachment` VALUES ('12', '2020-12-22 00:39:12', '0', 'jpg', '1030', null, 'a24738bb-0fba-4249-955e-abc1d82aacf3.jpg', '/upload/article', '106045.00', '1455');
INSERT INTO `b_attachment` VALUES ('13', '2020-12-22 00:44:49', '0', 'jpg', '1030', null, '08bc2fbd-eeae-457e-9d94-e932d6f84d4a.jpg', '/upload/article', '106045.00', '1455');
INSERT INTO `b_attachment` VALUES ('14', '2020-12-22 00:47:23', '0', 'jpg', '1030', null, '443552cc-165e-4c62-8316-4a4888bb5f16.jpg', '/upload/article', '106045.00', '1455');
INSERT INTO `b_attachment` VALUES ('15', '2020-12-22 00:51:18', '0', 'jpg', '1030', null, 'b0c4459d-d8a1-4778-a3ba-c88c729878ab.jpg', '/upload/article', '106045.00', '1455');
INSERT INTO `b_attachment` VALUES ('16', '2020-12-22 00:52:55', '0', 'jpg', '1030', null, 'cf99bd2f-cae6-4f30-8b73-796d4bce2d72.jpg', '/upload/article', '106045.00', '1455');
INSERT INTO `b_attachment` VALUES ('17', '2020-12-22 00:56:38', '0', 'jpg', '1030', null, '09896efb-d377-4eff-be2d-2201e2b51e92.jpg', '/upload/article', '106045.00', '1455');
INSERT INTO `b_attachment` VALUES ('18', '2020-12-22 01:04:19', '0', 'jpg', '1030', null, '2f962795-68f8-46ab-b96f-fa229b649ba0.jpg', '/upload/article', '106045.00', '1455');
INSERT INTO `b_attachment` VALUES ('19', '2020-12-23 19:55:54', '0', 'jpg', '900', null, '3fd3d713-6b78-49fd-a338-f1e9673d29d2.jpg', '/upload/article', '123536.00', '1440');
INSERT INTO `b_attachment` VALUES ('20', '2020-12-23 23:47:11', '0', 'jpg', '900', null, 'dac56dac-57b0-4bb0-81f1-f2e92f8696de.jpg', '/upload/article', '123536.00', '1440');
INSERT INTO `b_attachment` VALUES ('21', '2020-12-24 00:21:34', '0', 'jpg', '1030', null, '90180bf1-2993-489a-97ae-9f7b3f55a2f7.jpg', '/upload/article', '106045.00', '1455');
INSERT INTO `b_attachment` VALUES ('22', '2020-12-24 00:38:36', '0', 'jpg', '1030', null, 'b2e32b73-845f-47b6-8dec-2d3841ea7469.jpg', '/upload/article', '106045.00', '1455');
INSERT INTO `b_attachment` VALUES ('23', '2020-12-24 00:43:10', '0', 'jpg', '400', null, '373f8fbe-643a-408a-8667-da0c9f0d7052.jpg', '/upload/article', '16724.00', '500');
INSERT INTO `b_attachment` VALUES ('24', '2020-12-24 00:50:47', '0', 'jpg', '1030', null, '8ca3db47-b4e1-4c8c-9919-6fea8982302f.jpg', '/upload/article', '106045.00', '1455');
INSERT INTO `b_attachment` VALUES ('25', '2020-12-24 22:05:33', '0', 'jpg', '1030', null, 'f642a958-06a9-49a2-b88f-e4d2e1798f2a.jpg', '/upload/article', '106045.00', '1455');
INSERT INTO `b_attachment` VALUES ('26', '2020-12-24 22:06:13', '0', 'jpg', '1030', null, '13f63544-9c6f-4d07-9751-fb724f6079ec.jpg', '/upload/article', '106045.00', '1455');
INSERT INTO `b_attachment` VALUES ('27', '2020-12-24 22:07:47', '0', 'jpg', '1030', null, '2640f4e9-f2c1-4338-bf13-133696ffbb33.jpg', '/upload/article', '106045.00', '1455');
INSERT INTO `b_attachment` VALUES ('28', '2020-12-24 22:09:16', '0', 'jpg', '1030', null, 'cf524dc0-2c42-4ef4-95b8-fa14b2afe11d.jpg', '/upload/article', '106045.00', '1455');
INSERT INTO `b_attachment` VALUES ('29', '2020-12-24 22:09:28', '0', 'jpg', '1030', null, '24067ef8-21a3-4eed-9bdb-85f1da39424c.jpg', '/upload/article', '106045.00', '1455');
INSERT INTO `b_attachment` VALUES ('30', '2020-12-24 23:46:01', '0', 'jpg', '1030', null, 'e28e91f6-e270-4ef6-83dc-fb7e4b9a02e6.jpg', '/upload/article', '106045.00', '1455');
INSERT INTO `b_attachment` VALUES ('31', '2020-12-24 23:47:25', '0', 'jpg', '1030', null, 'f072a80e-9a1d-4169-9f26-c41f8c8e7367.jpg', '/upload/article', '106045.00', '1455');
INSERT INTO `b_attachment` VALUES ('32', '2020-12-25 00:00:20', '0', 'jpg', '1030', null, '1c9b1599-b54d-4833-9065-a93bf7fc288e.jpg', '/upload/article', '106045.00', '1455');
INSERT INTO `b_attachment` VALUES ('33', '2020-12-25 00:00:56', '0', 'jpg', '1030', null, '7a0128b8-5239-43b7-ae42-4e5db360a924.jpg', '/upload/article', '106045.00', '1455');
INSERT INTO `b_attachment` VALUES ('34', '2020-12-25 00:04:57', '0', 'jpg', '1030', null, '1bd69c2d-f4bd-43c6-a586-a0a30918ed16.jpg', '/upload/article', '106045.00', '1455');
INSERT INTO `b_attachment` VALUES ('35', '2020-12-25 00:05:07', '0', 'jpg', '900', null, 'a63798b3-343c-4198-a807-011b36108404.jpg', '/upload/article', '123536.00', '1440');
INSERT INTO `b_attachment` VALUES ('36', '2020-12-25 00:14:09', '0', 'jpg', '1030', null, '26eb12a2-79e4-46e3-a635-36e2533fb26a.jpg', '/upload/article', '106045.00', '1455');
INSERT INTO `b_attachment` VALUES ('37', '2020-12-25 00:17:43', '0', 'jpg', '1030', null, 'bfec86c3-7940-4b31-ba0e-93c60a1a2bbc.jpg', '/upload/article', '106045.00', '1455');
INSERT INTO `b_attachment` VALUES ('38', '2020-12-27 22:25:41', '0', 'jpg', '1030', null, 'a576bd7f-5038-4b68-8ffd-7bdc4c6469fb.jpg', '/upload/article', '106045.00', '1455');
INSERT INTO `b_attachment` VALUES ('39', '2020-12-27 22:26:24', '0', 'jpg', '1030', null, '64e9667d-8374-440f-903c-77f3addf72f3.jpg', '/upload/article', '106045.00', '1455');
INSERT INTO `b_attachment` VALUES ('40', '2020-12-27 22:27:27', '0', 'jpg', '1030', null, '7c4296f4-0642-492e-9e66-9c61ddfd868c.jpg', '/upload/article', '106045.00', '1455');
INSERT INTO `b_attachment` VALUES ('41', '2020-12-27 22:29:14', '0', 'jpg', '1030', null, '1aaabf4c-139d-4c4e-b139-86bd86d1e478.jpg', '/upload/article', '106045.00', '1455');
INSERT INTO `b_attachment` VALUES ('42', '2020-12-27 22:33:25', '0', 'jpg', '1030', null, '082d5904-21a0-4281-97c0-6cb931dbbc2b.jpg', '/upload/article', '106045.00', '1455');

-- ----------------------------
-- Table structure for `b_counter`
-- ----------------------------
DROP TABLE IF EXISTS `b_counter`;
CREATE TABLE `b_counter` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `count` int(11) DEFAULT NULL COMMENT '数字',
  `del_state` int(11) DEFAULT '0' COMMENT '是否删除 0 否 1是',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COMMENT='计数器';

-- ----------------------------
-- Records of b_counter
-- ----------------------------
INSERT INTO `b_counter` VALUES ('1', '1', '0');
INSERT INTO `b_counter` VALUES ('2', '10', '0');

-- ----------------------------
-- Table structure for `b_data_dictionary`
-- ----------------------------
DROP TABLE IF EXISTS `b_data_dictionary`;
CREATE TABLE `b_data_dictionary` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `parent_id` bigint(192) DEFAULT '0' COMMENT '父级',
  `numbers` varchar(300) DEFAULT NULL COMMENT '数据编码',
  `label` varchar(300) DEFAULT NULL COMMENT '标签',
  `name` varchar(300) DEFAULT NULL COMMENT '名称',
  `value` varchar(300) DEFAULT NULL COMMENT '内容',
  `sort` int(11) DEFAULT NULL COMMENT '排序',
  `created_time` datetime DEFAULT NULL COMMENT '创建日期',
  `created_id` bigint(20) DEFAULT NULL COMMENT '创建人id',
  `created_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '创建人名称',
  `updated_time` datetime DEFAULT NULL COMMENT '修改日期',
  `updated_id` bigint(20) DEFAULT NULL COMMENT '修改人id',
  `updated_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '修改人名称',
  `del_state` int(11) DEFAULT '0' COMMENT '是否删除,默认为0未删除，1表示删除状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='数据字典表';

-- ----------------------------
-- Records of b_data_dictionary
-- ----------------------------

-- ----------------------------
-- Table structure for `b_question`
-- ----------------------------
DROP TABLE IF EXISTS `b_question`;
CREATE TABLE `b_question` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '域模型id，这里为自增类型',
  `add_time` datetime DEFAULT NULL COMMENT '添加时间，这里为长时间格式',
  `del_state` int(11) DEFAULT '0' COMMENT '是否删除,默认为0未删除，-1表示删除状态',
  `source` varchar(255) DEFAULT NULL COMMENT '数据来源',
  `sequence` int(11) DEFAULT '0' COMMENT '排序，从小到大',
  `type` int(11) DEFAULT NULL COMMENT '类型，0单选 1判断  2多选  3解答题',
  `level` int(11) DEFAULT NULL COMMENT '难度 0 1 2 3 4 等级别',
  `class_id` int(11) DEFAULT '0' COMMENT '分类',
  `title` varchar(255) DEFAULT NULL COMMENT '标题',
  `detail` longtext COMMENT '问题详情',
  `parse` longtext COMMENT '问题解析',
  `content` longtext COMMENT '内容',
  `like` int(11) DEFAULT '0' COMMENT '赞',
  `tread` int(11) DEFAULT '0' COMMENT '踩',
  `collect` int(11) DEFAULT '0' COMMENT '收藏',
  `vista` bigint(20) DEFAULT '0' COMMENT '访问量',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='问题';

-- ----------------------------
-- Records of b_question
-- ----------------------------

-- ----------------------------
-- Table structure for `b_question_result`
-- ----------------------------
DROP TABLE IF EXISTS `b_question_result`;
CREATE TABLE `b_question_result` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '域模型id，这里为自增类型',
  `add_time` datetime DEFAULT NULL COMMENT '添加时间，这里为长时间格式',
  `del_state` int(11) DEFAULT '0' COMMENT '是否删除,默认为0未删除，-1表示删除状态',
  `question_id` bigint(20) DEFAULT NULL COMMENT '问题id',
  `sequence` varchar(11) DEFAULT NULL COMMENT '答案顺序',
  `content` longtext COMMENT '答案内容',
  `right` int(11) DEFAULT NULL COMMENT '0 否 1 正确',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='问题答案-基本用于选择题';

-- ----------------------------
-- Records of b_question_result
-- ----------------------------

-- ----------------------------
-- Table structure for `b_question_tags`
-- ----------------------------
DROP TABLE IF EXISTS `b_question_tags`;
CREATE TABLE `b_question_tags` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '域模型id，这里为自增类型',
  `add_time` datetime DEFAULT NULL COMMENT '添加时间，这里为长时间格式',
  `del_state` int(11) DEFAULT '0' COMMENT '是否删除,默认为0未删除，-1表示删除状态',
  `question_id` bigint(20) DEFAULT NULL COMMENT '文章id',
  `tags_id` bigint(20) DEFAULT NULL COMMENT '标签id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='问题与标签中间表';

-- ----------------------------
-- Records of b_question_tags
-- ----------------------------

-- ----------------------------
-- Table structure for `b_sys_config`
-- ----------------------------
DROP TABLE IF EXISTS `b_sys_config`;
CREATE TABLE `b_sys_config` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '域模型id，这里为自增类型',
  `add_time` datetime DEFAULT NULL COMMENT '添加时间，这里为长时间格式',
  `del_state` int(11) DEFAULT '0' COMMENT '是否删除,默认为0未删除，-1表示删除状态',
  `email_enable` int(1) DEFAULT NULL COMMENT ' 邮件是否开启0否1是',
  `email_host` varchar(255) DEFAULT NULL COMMENT ' stmp服务器',
  `email_port` int(11) DEFAULT NULL COMMENT ' stmp端口',
  `email_pwss` varchar(255) DEFAULT NULL COMMENT ' 邮箱密码',
  `email_user` varchar(255) DEFAULT NULL COMMENT ' 邮箱用户名',
  `every_answer_integrate` int(11) DEFAULT NULL COMMENT ' 答题(赠送积分)',
  `image_file_size` int(11) DEFAULT NULL COMMENT ' 允许图片上传的最大值',
  `login_integrate` int(11) DEFAULT NULL COMMENT ' 会员每日登陆(赠送积分)',
  `upload_file_path` varchar(255) DEFAULT NULL COMMENT ' 用户上传文件路径',
  `upload_test_bank_path` varchar(255) DEFAULT NULL COMMENT ' 试题文件路径',
  `weixin_appId` varchar(255) DEFAULT NULL COMMENT ' 微信App Id，申请开发者成功后微信生成的AppId',
  `weixin_appSecret` varchar(255) DEFAULT NULL COMMENT ' 微信AppSecret，申请开发者成功后微信生成的AppSecret',
  `Website_ico_id` bigint(20) DEFAULT NULL COMMENT ' 网站ICO图片',
  `admin_left_logo_id` bigint(20) DEFAULT NULL COMMENT '后台管理左上角Logo',
  `user_icon_id` bigint(20) DEFAULT NULL COMMENT ' 默认用户图标',
  `website_logo_id` bigint(20) DEFAULT NULL COMMENT ' 网站logo',
  `admin_url` longtext COMMENT '管理url',
  `index_url` longtext COMMENT '首页url',
  `sharding_strategy` bigint(20) DEFAULT NULL COMMENT '分库分表策略,也就是按照多大的数据量分一个库表，填写数字，最优是一百万：1000000，默认一百万分一个库表。',
  `static_version` varchar(20) DEFAULT NULL COMMENT '静态资源版本',
  `image_server_address` varchar(255) DEFAULT NULL COMMENT '图片服务器（也就是nginx代理的地址）',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='系统配置表';

-- ----------------------------
-- Records of b_sys_config
-- ----------------------------
INSERT INTO `b_sys_config` VALUES ('1', '2020-12-15 22:36:38', '0', '1', null, null, null, null, null, '500000', '5', '/upload', null, null, null, null, null, null, null, null, null, null, '0', 'http://localhost:8809');

-- ----------------------------
-- Table structure for `b_sys_ftp`
-- ----------------------------
DROP TABLE IF EXISTS `b_sys_ftp`;
CREATE TABLE `b_sys_ftp` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '域模型id，这里为自增类型',
  `add_time` datetime DEFAULT NULL COMMENT '添加时间，这里为长时间格式',
  `del_state` int(11) DEFAULT '0' COMMENT '是否删除,默认为0未删除，-1表示删除状态',
  `ftp_addr` varchar(255) DEFAULT NULL COMMENT ' FTP服务器地址,优先使用服务器地址访问，如果没有域名，则使用服务器ip访问，需带有http:',
  `ftp_amount` int(11) DEFAULT '100' COMMENT ' ftp所拥有用户数量上限，默认100',
  `ftp_ip` varchar(255) DEFAULT NULL COMMENT ' FTP ip地址,用于图片上传，不可带有http:',
  `ftp_name` varchar(255) DEFAULT NULL COMMENT ' FTP服务器名称',
  `ftp_password` varchar(255) DEFAULT NULL COMMENT ' 链接FTP用户密码',
  `ftp_sequence` int(11) DEFAULT '0' COMMENT ' 排序',
  `ftp_system` int(11) DEFAULT '0' COMMENT ' 是否为当前使用的系统服务器，1为是，系统可以有多个系统ftp，只能同时使用一个',
  `ftp_type` int(11) DEFAULT '0' COMMENT ' 服务器类型，0为用户服务器，1为系统服务器，系统服务器可以有多个，但同时只能使用一个',
  `ftp_username` varchar(255) DEFAULT NULL COMMENT ' 链接FTP用户名',
  `ftp_port` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='图片资源FTP服务器表';

-- ----------------------------
-- Records of b_sys_ftp
-- ----------------------------
INSERT INTO `b_sys_ftp` VALUES ('1', '2018-05-04 17:28:35', '0', 'http://localhost', '100', 'localhost', '图片服务器', '123456', '0', '1', '0', 'user', '22');

-- ----------------------------
-- Table structure for `b_sys_log`
-- ----------------------------
DROP TABLE IF EXISTS `b_sys_log`;
CREATE TABLE `b_sys_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) DEFAULT NULL COMMENT '用户名',
  `operation` varchar(50) DEFAULT NULL COMMENT '用户操作',
  `method` varchar(200) DEFAULT NULL COMMENT '请求方法',
  `params` varchar(5000) DEFAULT NULL COMMENT '请求参数',
  `time` bigint(20) NOT NULL COMMENT '执行时长(毫秒)',
  `ip` varchar(64) DEFAULT NULL COMMENT 'IP地址',
  `add_time` datetime DEFAULT NULL COMMENT '创建时间',
  `del_state` int(11) DEFAULT '0' COMMENT '删除状态 0 否 1 是',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COMMENT='系统日志';

-- ----------------------------
-- Records of b_sys_log
-- ----------------------------
INSERT INTO `b_sys_log` VALUES ('1', 'admin', '新增系统配置表', 'com.dispenser.blogs.controller.SysConfigController.save()', '[{\"id\":1,\"addTime\":\"Dec 15, 2020 10:36:38 PM\",\"delState\":0,\"emailEnable\":true,\"imageFileSize\":500000,\"uploadFilePath\":\"/upload\",\"staticVersion\":\"0\",\"imageServerAddress\":\"http://localhost:8809\"}]', '81', '0:0:0:0:0:0:0:1', '2020-12-27 21:32:53', '0');
INSERT INTO `b_sys_log` VALUES ('2', 'admin', '新增系统配置表', 'com.dispenser.blogs.controller.SysConfigController.save()', '[{\"id\":1,\"addTime\":\"Dec 15, 2020 10:36:38 PM\",\"delState\":0,\"emailEnable\":true,\"imageFileSize\":500000,\"loginIntegrate\":5,\"uploadFilePath\":\"/upload\",\"staticVersion\":\"0\",\"imageServerAddress\":\"http://localhost:8809\"}]', '107', '0:0:0:0:0:0:0:1', '2020-12-27 21:50:10', '0');
INSERT INTO `b_sys_log` VALUES ('3', 'admin', '修改文件服务器FTP', 'com.dispenser.blogs.controller.SysFtpController.save()', '[{\"id\":1,\"addTime\":\"May 4, 2018 5:28:35 PM\",\"delState\":0,\"ftpAddr\":\"http://localhost\",\"ftpAmount\":100,\"ftpIp\":\"localhost\",\"ftpName\":\"图片服务器\",\"ftpPassword\":\"123456\",\"ftpSequence\":0,\"ftpSystem\":1,\"ftpType\":0,\"ftpUsername\":\"user\",\"ftpPort\":22}]', '103', '0:0:0:0:0:0:0:1', '2020-12-27 22:16:34', '0');
INSERT INTO `b_sys_log` VALUES ('4', 'admin', '修改文件服务器FTP', 'com.dispenser.blogs.controller.SysFtpController.save()', '[{\"id\":1,\"addTime\":\"May 4, 2018 5:28:35 PM\",\"delState\":0,\"ftpAddr\":\"http://localhost\",\"ftpAmount\":100,\"ftpIp\":\"localhost\",\"ftpName\":\"图片服务器\",\"ftpPassword\":\"123456\",\"ftpSequence\":0,\"ftpSystem\":1,\"ftpType\":0,\"ftpUsername\":\"user\",\"ftpPort\":22}]', '72', '0:0:0:0:0:0:0:1', '2020-12-27 22:28:39', '0');
INSERT INTO `b_sys_log` VALUES ('5', 'admin', '修改系统菜单表', 'com.dispenser.blogs.controller.SysMenuController.editSave()', '[{\"id\":23,\"addTime\":\"Jan 2, 2021 8:28:24 PM\",\"delState\":0,\"title\":\"编辑菜单\",\"path\":\"/menuEdit\",\"value\":\"menuEdit\",\"parentId\":8,\"sequence\":0,\"icon\":\"user\",\"display\":0}]', '40', '0:0:0:0:0:0:0:1', '2021-01-02 20:35:33', '0');
INSERT INTO `b_sys_log` VALUES ('6', 'admin', '修改系统菜单表', 'com.dispenser.blogs.controller.SysMenuController.editSave()', '[{\"id\":23,\"addTime\":\"Jan 2, 2021 8:28:24 PM\",\"delState\":0,\"title\":\"编辑菜单\",\"path\":\"/menuEdit\",\"value\":\"menuEdit\",\"parentId\":8,\"sequence\":0,\"icon\":\"user\",\"display\":1}]', '74', '0:0:0:0:0:0:0:1', '2021-01-02 20:35:48', '0');
INSERT INTO `b_sys_log` VALUES ('7', 'admin', '新增系统菜单表', 'com.dispenser.blogs.controller.SysMenuController.save()', '[{\"addTime\":\"Jan 3, 2021 4:36:23 AM\",\"delState\":0,\"title\":\"/8888888\",\"path\":\"8888\",\"value\":\"8888\",\"parentId\":0,\"sequence\":0,\"icon\":\"user\",\"display\":0}]', '106', '0:0:0:0:0:0:0:1', '2021-01-02 20:36:38', '0');
INSERT INTO `b_sys_log` VALUES ('8', 'admin', '删除系统菜单表', 'com.dispenser.blogs.controller.SysMenuController.delete()', '[{\"id\":24}]', '85', '0:0:0:0:0:0:0:1', '2021-01-02 20:36:54', '0');

-- ----------------------------
-- Table structure for `b_sys_menu`
-- ----------------------------
DROP TABLE IF EXISTS `b_sys_menu`;
CREATE TABLE `b_sys_menu` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT COMMENT '域模型id，这里为自增类型',
  `add_time` datetime DEFAULT NULL COMMENT '添加时间，这里为长时间格式',
  `del_state` int(11) DEFAULT '0' COMMENT '是否删除,默认为0未删除，-1表示删除状态',
  `title` varchar(255) DEFAULT NULL COMMENT '菜单名称',
  `path` varchar(255) DEFAULT NULL COMMENT '菜单地址',
  `value` varchar(255) DEFAULT NULL COMMENT '英文名称',
  `parent_id` int(11) DEFAULT '0' COMMENT '父菜单',
  `sequence` int(11) DEFAULT NULL COMMENT '排序',
  `icon` varchar(255) DEFAULT NULL COMMENT '菜单图标',
  `display` int(11) DEFAULT '0' COMMENT '隐藏显示 0 显示 1 隐藏',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='系统菜单表';

-- ----------------------------
-- Records of b_sys_menu
-- ----------------------------
INSERT INTO `b_sys_menu` VALUES ('1', '2020-12-13 00:38:29', '0', '系统设置', '/config', 'config', '0', '0', 'user', '0');
INSERT INTO `b_sys_menu` VALUES ('2', '2020-12-13 00:57:32', '0', '基础配置', '/sysConfig', 'sysConfig', '1', '0', 'user', '0');
INSERT INTO `b_sys_menu` VALUES ('3', '2020-12-13 15:48:19', '0', '用户管理', '/user', 'user', '0', '0', 'user', '0');
INSERT INTO `b_sys_menu` VALUES ('4', '2020-12-13 15:49:02', '0', '用户信息', '/info', 'info', '3', '0', 'user', '0');
INSERT INTO `b_sys_menu` VALUES ('5', '2020-12-13 15:51:11', '0', '文章管理', '/article', 'article', '0', '0', 'user', '0');
INSERT INTO `b_sys_menu` VALUES ('6', '2020-12-13 15:53:54', '0', '文章列表', '/articleList', 'larticleList', '5', '0', 'user', '0');
INSERT INTO `b_sys_menu` VALUES ('7', '2020-12-13 15:52:55', '0', '发布文章', '/articleAdd', 'articleAdd', '5', '0', 'user', '0');
INSERT INTO `b_sys_menu` VALUES ('8', '2020-12-13 15:55:51', '0', '菜单管理', '/menu', 'menu', '0', '0', 'user', '0');
INSERT INTO `b_sys_menu` VALUES ('9', '2020-12-13 15:56:34', '0', '菜单列表', '/menuList', 'menuList', '8', '0', 'user', '0');
INSERT INTO `b_sys_menu` VALUES ('10', '2020-12-14 23:09:30', '0', '分类管理', '/class', 'class', '0', '0', 'user', '0');
INSERT INTO `b_sys_menu` VALUES ('11', '2020-12-14 23:10:08', '0', '分类列表', '/classList', 'classList', '10', '0', 'user', '0');
INSERT INTO `b_sys_menu` VALUES ('12', '2020-12-14 23:11:01', '0', '标签管理', '/tags', 'tags', '0', '0', 'user', '0');
INSERT INTO `b_sys_menu` VALUES ('13', '2020-12-14 23:12:37', '0', '标签列表', '/tagsList', 'tagsList', '12', '0', 'user', '0');
INSERT INTO `b_sys_menu` VALUES ('14', '2020-12-19 17:51:52', '0', '日志管理', '/log', 'log', '0', '0', 'user', '0');
INSERT INTO `b_sys_menu` VALUES ('15', '2020-12-19 17:52:26', '0', '日志列表', '/logList', 'logList', '14', '0', 'user', '0');
INSERT INTO `b_sys_menu` VALUES ('16', '2020-12-19 19:15:03', '0', '添加分类', '/classAdd', 'classAdd', '10', '0', 'user', '0');
INSERT INTO `b_sys_menu` VALUES ('17', '2020-12-20 00:19:16', '0', '编辑文章', '/articleEdit', 'articleEdit', '5', '0', 'user', '1');
INSERT INTO `b_sys_menu` VALUES ('18', '2020-12-22 00:12:55', '0', '编辑分类', '/classEdit', 'classEdit', '10', '0', 'user', '1');
INSERT INTO `b_sys_menu` VALUES ('19', '2020-12-24 00:19:25', '0', '添加标签', '/tagsAdd', 'tagsAdd', '12', '0', 'user', '0');
INSERT INTO `b_sys_menu` VALUES ('20', '2020-12-24 00:20:05', '0', '编辑标签', '/tagsEdit', 'tagsEdit', '12', '0', 'user', '1');
INSERT INTO `b_sys_menu` VALUES ('21', '2020-12-27 21:54:16', '0', '文件服务', '/sysFtp', 'sysFtp', '1', '0', 'user', '0');
INSERT INTO `b_sys_menu` VALUES ('22', '2021-01-02 20:27:35', '0', '添加菜单', '/menuAdd', 'menuAdd', '8', '0', 'user', '0');
INSERT INTO `b_sys_menu` VALUES ('23', '2021-01-02 20:28:24', '0', '编辑菜单', '/menuEdit', 'menuEdit', '8', '0', 'user', '1');
INSERT INTO `b_sys_menu` VALUES ('24', '2021-01-03 04:36:23', '1', '/8888888', '8888', '8888', '0', '0', 'user', '0');

-- ----------------------------
-- Table structure for `b_tags`
-- ----------------------------
DROP TABLE IF EXISTS `b_tags`;
CREATE TABLE `b_tags` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '域模型id，这里为自增类型',
  `add_time` datetime DEFAULT NULL COMMENT '添加时间，这里为长时间格式',
  `del_state` int(11) DEFAULT '0' COMMENT '是否删除,默认为0未删除，-1表示删除状态',
  `display` int(11) DEFAULT '0' COMMENT '是否显示,默认为0 显示 ，1隐藏',
  `icon_id` bigint(20) DEFAULT NULL COMMENT '图标id',
  `comment` varchar(255) DEFAULT NULL COMMENT '描述',
  `sequence` int(11) DEFAULT '0' COMMENT '排序，从小到大',
  `tags_name` varchar(255) DEFAULT NULL COMMENT '标签名称',
  `tags_type` int(11) DEFAULT NULL COMMENT '类型，用于扩展',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='标签';

-- ----------------------------
-- Records of b_tags
-- ----------------------------
INSERT INTO `b_tags` VALUES ('1', '2020-12-17 08:00:00', '0', '0', '23', '算得上是', '0', '算得上是', null);
INSERT INTO `b_tags` VALUES ('2', '2020-12-17 08:00:00', '0', '0', '24', '888', '0', '8888', null);

-- ----------------------------
-- Table structure for `b_user`
-- ----------------------------
DROP TABLE IF EXISTS `b_user`;
CREATE TABLE `b_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '域模型id，这里为自增类型',
  `add_time` datetime DEFAULT NULL COMMENT '添加时间，这里为长时间格式',
  `del_state` int(11) DEFAULT '0' COMMENT '是否删除,默认为0未删除，-1表示删除状态',
  `birthday` datetime DEFAULT NULL COMMENT ' 出生日期',
  `email` varchar(255) DEFAULT NULL COMMENT ' 邮箱地址',
  `last_login_date` datetime DEFAULT NULL COMMENT ' 上次登陆时间',
  `last_login_ip` varchar(255) DEFAULT NULL COMMENT ' 上次登录IP',
  `login_count` int(11) NOT NULL COMMENT ' 登录次数',
  `login_date` datetime DEFAULT NULL COMMENT ' 登陆时间',
  `login_ip` varchar(255) DEFAULT NULL COMMENT ' 登陆Ip',
  `mobile` varchar(255) DEFAULT NULL COMMENT ' 手机支付密码，用户手机端使用预存款支付时需要输入支付密码，如果用户没有设置支付密码需要输入登录密码',
  `nick_name` varchar(255) DEFAULT NULL COMMENT ' 昵称',
  `open_id` varchar(255) DEFAULT NULL COMMENT ' 微信公众平台使用的openid',
  `password` varchar(255) DEFAULT NULL COMMENT ' 手机支付密码，用户手机端使用预存款支付时需要输入支付密码，如果用户没有设置支付密码需要输入登录密码',
  `sex` int(11) DEFAULT '-1' COMMENT ' 性别 1为男、0为女、-1为保密',
  `user_name` varchar(255) DEFAULT NULL COMMENT ' 用户名',
  `user_type` int(11) DEFAULT '0' COMMENT ' 用户类别，默认为0个人用户，1管理员',
  `union_id` varchar(255) DEFAULT NULL COMMENT ' 微信公众平台使用unionId',
  `age` int(11) DEFAULT '0' COMMENT ' 用户年龄',
  `signature` longtext COMMENT '个性签名',
  `integral` bigint(20) DEFAULT '0' COMMENT '用户积分',
  `attachment_id` bigint(20) DEFAULT NULL COMMENT '用户头像',
  `clock` int(11) DEFAULT '0' COMMENT '用户打卡次数',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='用户表';

-- ----------------------------
-- Records of b_user
-- ----------------------------
INSERT INTO `b_user` VALUES ('1', '2020-12-12 13:02:55', '0', '2020-12-12 13:02:59', '9543482@qq.com', '2020-12-12 13:03:10', '192.168.0.21', '10', '2021-01-02 20:44:41', '0:0:0:0:0:0:0:1', '1768888888', '掌柜', null, 'e10adc3949ba59abbe56e057f20f883e', '1', 'admin', '1', null, '18', '有道无术', '100', '1', '0');
