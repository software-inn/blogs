###redis : 
    redis-server.exe 双击启动
###swagger：
    http://localhost:8808/doc.html
###ImageServer
    1.安装serv-u 
    2.破解serv-u
        安装目录里面，有对应的破解方法
    3.配置serv-u（Ftp服务器）
        1.配置一个域
        2.创建一个用户
            1>.配置用户的存贮的根目录
            2>.配置用户的访问目录权限
    4.数据库配置Ftp服务器(也就是图片服务器)
        表：b_sys_ftp
        ftp_username：对应上面配置serv-u 用户的用户名
        ftp_password：对应上面配置serv-u 用户的密码
        ftp_port：22（端口固定写死）
        ftp_ip：本地ip(安装serv-u的电脑ip，默认：localhost)
    5.nginx 配置
        1.配置server 端口，默认：8809
        2.配置root serv-u 用户的存储目录
        