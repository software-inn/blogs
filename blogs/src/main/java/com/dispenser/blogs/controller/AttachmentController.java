package com.dispenser.blogs.controller;

import com.baomidou.mybatisplus.extension.api.ApiController;
import com.dispenser.blogs.common.ApiResult;
import com.dispenser.blogs.common.BeanUtil;
import com.dispenser.blogs.model.dto.AttachmentDTO;
import com.dispenser.blogs.model.qo.AttachmentQO;
import com.dispenser.blogs.model.vo.AttachmentVO;
import com.dispenser.blogs.service.AttachmentService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;



/**
 * 图片表
 *
 */
@RestController
@RequestMapping("/api/attachment")
@Api(tags = "图片表")
public class AttachmentController extends ApiController {


    @Autowired
    private AttachmentService attachmentService;

    /*
     * 分页获取图片表列表
	 */
    @PostMapping("/list")
    @ApiOperation(value = "图片表分页")
    public ApiResult list(@RequestBody AttachmentQO qo) {
        return attachmentService.list(qo);
    }

    /**
   * 根据id查询图片表
   */
    @GetMapping("/get/{id}")
    @ApiOperation(value = "根据id查询图片表")
    public ApiResult get(@PathVariable(name = "id", required = true) Long id) {
        return attachmentService.get(id);
    }

    /**
     * 新增图片表
     */
    @PostMapping("/save")
    @ApiOperation(value = "新增图片表")
    public ApiResult save(@RequestBody AttachmentVO vo) {
        AttachmentDTO dto = BeanUtil.copyProperties(vo, AttachmentDTO.class);
        return attachmentService.save(dto);
    }

    /**
     * 修改图片表
     */
    @PostMapping("/editSave")
    @ApiOperation(value = "修改图片表")
    public ApiResult editSave(@RequestBody AttachmentVO vo) {
        AttachmentDTO dto = BeanUtil.copyProperties(vo, AttachmentDTO.class);
        return attachmentService.update(dto);
    }

    /**
     * 逻辑删除图片表
     */
    @PostMapping("/delete")
    @ApiOperation(value = "根据id删除进度计划")
    public ApiResult delete(@RequestBody AttachmentVO vo) {
        AttachmentDTO dto = BeanUtil.copyProperties(vo, AttachmentDTO.class);
        return attachmentService.delete(dto.getId());
    }
}
