package com.dispenser.blogs.controller;

import com.baomidou.mybatisplus.extension.api.ApiController;
import com.dispenser.blogs.common.ApiResult;
import com.dispenser.blogs.common.BeanUtil;
import com.dispenser.blogs.model.dto.ArticleClassDTO;
import com.dispenser.blogs.model.qo.ArticleClassQO;
import com.dispenser.blogs.model.vo.ArticleClassVO;
import com.dispenser.blogs.service.ArticleClassService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;



/**
 * 分类
 *
 */
@RestController
@RequestMapping("/api/articleClass")
@Api(tags = "分类")
public class ArticleClassController extends ApiController {


    @Autowired
    private ArticleClassService articleClassService;

    /*
     * 分类所有数据
	 */
    @PostMapping("/getAll")
    @ApiOperation(value = "分类所有数据")
    public ApiResult getAll(@RequestBody ArticleClassQO qo) {
        return articleClassService.getAll(qo);
    }


    /*
     * 分页获取分类列表
     */
    @PostMapping("/list")
    @ApiOperation(value = "分类分页")
    public ApiResult list(@RequestBody ArticleClassQO qo) {
        return articleClassService.list(qo);
    }

    /**
   * 根据id查询分类
   */
    @GetMapping("/get/{id}")
    @ApiOperation(value = "根据id查询分类")
    public ApiResult get(@PathVariable(name = "id", required = true) Long id) {
        return articleClassService.get(id);
    }

    /**
     * 新增分类
     */
    @PostMapping("/save")
    @ApiOperation(value = "新增分类")
    public ApiResult save(@RequestBody ArticleClassVO vo) {
        ArticleClassDTO dto = BeanUtil.copyProperties(vo, ArticleClassDTO.class);
        return articleClassService.save(dto);
    }

    /**
     * 修改分类
     */
    @PostMapping("/editSave")
    @ApiOperation(value = "修改分类")
    public ApiResult editSave(@RequestBody ArticleClassVO vo) {
        ArticleClassDTO dto = BeanUtil.copyProperties(vo, ArticleClassDTO.class);
        return articleClassService.update(dto);
    }

    /**
     * 逻辑删除分类
     */
    @PostMapping("/delete")
    @ApiOperation(value = "根据id删除进度计划")
    public ApiResult delete(@RequestBody ArticleClassVO vo) {
        ArticleClassDTO dto = BeanUtil.copyProperties(vo, ArticleClassDTO.class);
        return articleClassService.delete(dto.getId());
    }
}
