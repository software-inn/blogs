package com.dispenser.blogs.controller;

import com.baomidou.mybatisplus.extension.api.ApiController;
import com.dispenser.blogs.annotation.UserAnno;
import com.dispenser.blogs.common.ApiResult;
import com.dispenser.blogs.common.ApiResultUtils;
import com.dispenser.blogs.common.BeanUtil;
import com.dispenser.blogs.model.dto.UserDTO;
import com.dispenser.blogs.model.entity.User;
import com.dispenser.blogs.model.qo.UserQO;
import com.dispenser.blogs.model.vo.UserVO;
import com.dispenser.blogs.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;


/**
 * 用户表
 *
 */
@RestController
@RequestMapping("/api/user")
@Api(tags = "用户表")
public class UserController extends ApiController {


    @Autowired
    private UserService userService;

    /*
     * 分页获取用户表列表
	 */
    @PostMapping("/list")
    @ApiOperation(value = "用户表分页")
    public ApiResult list(@RequestBody UserQO qo) {
        return userService.list(qo);
    }

    /**
     * 用户登录
     */
    @PostMapping("/login")
    @ApiOperation(value = "用户登录")
    public ApiResult login(@RequestBody UserQO qo , HttpServletRequest request) {
        return userService.login(request,qo);
    }
    /**
     * 用户退出
     */
    @PostMapping("/logout")
    @ApiOperation(value = "用户退出")
    public ApiResult logout(@UserAnno User user) {
        return userService.logout(user == null?null:user.getId());
    }
    /**
     * 获取用户登录信息
     */
    @GetMapping("/getInfo")
    @ApiOperation(value = " 获取用户登录信息")
    public ApiResult getInfo(@UserAnno User user) {
        return  userService.get(user.getId());
    }

    /**
   * 根据id查询用户表
   */
    @GetMapping("/get/{id}")
    @ApiOperation(value = "根据id查询用户表")
    public ApiResult get(@PathVariable(name = "id", required = true) Long id) {
        return userService.get(id);
    }

    /**
     * 新增用户表
     */
    @PostMapping("/save")
    @ApiOperation(value = "新增用户表")
    public ApiResult save(@RequestBody UserVO vo) {
        UserDTO dto = BeanUtil.copyProperties(vo, UserDTO.class);
        return userService.save(dto);
    }

    /**
     * 修改用户表
     */
    @PostMapping("/editSave")
    @ApiOperation(value = "修改用户表")
    public ApiResult editSave(@RequestBody UserVO vo) {
        UserDTO dto = BeanUtil.copyProperties(vo, UserDTO.class);
        return userService.update(dto);
    }

    /**
     * 逻辑删除用户表
     */
    @PostMapping("/delete")
    @ApiOperation(value = "根据id删除进度计划")
    public ApiResult delete(@RequestBody UserVO vo) {
        UserDTO dto = BeanUtil.copyProperties(vo, UserDTO.class);
        return userService.delete(dto.getId());
    }
}
