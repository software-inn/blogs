package com.dispenser.blogs.controller;

import com.baomidou.mybatisplus.extension.api.ApiController;
import com.dispenser.blogs.common.ApiResult;
import com.dispenser.blogs.common.BeanUtil;
import com.dispenser.blogs.model.dto.DataDictionaryDTO;
import com.dispenser.blogs.model.qo.DataDictionaryQO;
import com.dispenser.blogs.model.vo.DataDictionaryVO;
import com.dispenser.blogs.service.DataDictionaryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;



/**
 * 数据字典表
 *
 */
@RestController
@RequestMapping("/api/dataDictionary")
@Api(tags = "数据字典表")
public class DataDictionaryController extends ApiController {


    @Autowired
    private DataDictionaryService dataDictionaryService;

    /*
     * 分页获取数据字典表列表
	 */
    @PostMapping("/list")
    @ApiOperation(value = "数据字典表分页")
    public ApiResult list(@RequestBody DataDictionaryQO qo) {
        return dataDictionaryService.list(qo);
    }

    /**
   * 根据id查询数据字典表
   */
    @GetMapping("/get/{id}")
    @ApiOperation(value = "根据id查询数据字典表")
    public ApiResult get(@PathVariable(name = "id", required = true) Long id) {
        return dataDictionaryService.get(id);
    }

    /**
     * 新增数据字典表
     */
    @PostMapping("/save")
    @ApiOperation(value = "新增数据字典表")
    public ApiResult save(@RequestBody DataDictionaryVO vo) {
        DataDictionaryDTO dto = BeanUtil.copyProperties(vo, DataDictionaryDTO.class);
        return dataDictionaryService.save(dto);
    }

    /**
     * 修改数据字典表
     */
    @PostMapping("/editSave")
    @ApiOperation(value = "修改数据字典表")
    public ApiResult editSave(@RequestBody DataDictionaryVO vo) {
        DataDictionaryDTO dto = BeanUtil.copyProperties(vo, DataDictionaryDTO.class);
        return dataDictionaryService.update(dto);
    }

    /**
     * 逻辑删除数据字典表
     */
    @PostMapping("/delete")
    @ApiOperation(value = "根据id删除进度计划")
    public ApiResult delete(@RequestBody DataDictionaryVO vo) {
        DataDictionaryDTO dto = BeanUtil.copyProperties(vo, DataDictionaryDTO.class);
        return dataDictionaryService.delete(dto.getId());
    }
}
