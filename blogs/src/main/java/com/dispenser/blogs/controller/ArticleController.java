package com.dispenser.blogs.controller;

import com.baomidou.mybatisplus.extension.api.ApiController;
import com.dispenser.blogs.common.ApiResult;
import com.dispenser.blogs.common.BeanUtil;
import com.dispenser.blogs.model.dto.ArticleDTO;
import com.dispenser.blogs.model.qo.ArticleQO;
import com.dispenser.blogs.model.vo.ArticleVO;
import com.dispenser.blogs.service.ArticleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;



/**
 * 文章
 *
 */
@RestController
@RequestMapping("/api/article")
@Api(tags = "文章")
public class ArticleController extends ApiController {


    @Autowired
    private ArticleService articleService;

    /*
     * 分页获取文章列表
	 */
    @PostMapping("/list")
    @ApiOperation(value = "文章分页")
    public ApiResult list(@RequestBody ArticleQO qo) {
        return articleService.list(qo);
    }

    /**
   * 根据id查询文章
   */
    @GetMapping("/get/{id}")
    @ApiOperation(value = "根据id查询文章")
    public ApiResult get(@PathVariable(name = "id", required = true) Long id) {
        return articleService.get(id);
    }

    /**
     * 新增文章
     */
    @PostMapping("/save")
    @ApiOperation(value = "新增文章")
    public ApiResult save(@RequestBody ArticleVO vo) {
        ArticleDTO dto = BeanUtil.copyProperties(vo, ArticleDTO.class);
        return articleService.save(dto);
    }

    /**
     * 修改文章
     */
    @PostMapping("/editSave")
    @ApiOperation(value = "修改文章")
    public ApiResult editSave(@RequestBody ArticleVO vo) {
        ArticleDTO dto = BeanUtil.copyProperties(vo, ArticleDTO.class);
        return articleService.update(dto);
    }

    /**
     * 逻辑删除文章
     */
    @PostMapping("/delete")
    @ApiOperation(value = "根据id删除进度计划")
    public ApiResult delete(@RequestBody ArticleVO vo) {
        ArticleDTO dto = BeanUtil.copyProperties(vo, ArticleDTO.class);
        return articleService.delete(dto.getId());
    }
}
