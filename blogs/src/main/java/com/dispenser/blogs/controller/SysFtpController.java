package com.dispenser.blogs.controller;

import com.baomidou.mybatisplus.extension.api.ApiController;
import com.dispenser.blogs.annotation.LogAnno;
import com.dispenser.blogs.common.ApiResult;
import com.dispenser.blogs.common.BeanUtil;
import com.dispenser.blogs.model.dto.SysFtpDTO;
import com.dispenser.blogs.model.qo.SysFtpQO;
import com.dispenser.blogs.model.vo.SysFtpVO;
import com.dispenser.blogs.service.SysFtpService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;


/**
 * 图片资源FTP服务器表
 *
 */
@RestController
@RequestMapping("/api/sysFtp")
@Api(tags = "图片资源FTP服务器表")
public class SysFtpController extends ApiController {


    @Autowired
    private SysFtpService sysFtpService;

    /*
     * 上传图片
     */
    @PostMapping("/uploadFile")
    @ApiOperation(value = "上传图片")
    public ApiResult uploadFile(HttpServletRequest request) {
        MultipartHttpServletRequest multiRequest = (MultipartHttpServletRequest) request;
        String oldFileId = request.getParameter("oldFileId"); // 旧图片id
        String folder = request.getParameter("folder"); // 图片目录
        MultipartFile file = multiRequest.getFile("file"); // 上传文件流对应的name值
        return sysFtpService.uploadFile(file ,oldFileId,folder);
    }

    /*
     * 分页获取图片资源FTP服务器表列表
	 */
    @PostMapping("/list")
    @ApiOperation(value = "图片资源FTP服务器表分页")
    public ApiResult list(@RequestBody SysFtpQO qo) {
        return sysFtpService.list(qo);
    }

    /**
   * 根据id查询图片资源FTP服务器表
   */
    @GetMapping("/get/{id}")
    @ApiOperation(value = "根据id查询图片资源FTP服务器表")
    public ApiResult get(@PathVariable(name = "id", required = true) Long id) {
        return sysFtpService.get(id);
    }

    /**
     * 新增图片资源FTP服务器表
     */
    @LogAnno("修改文件服务器FTP")
    @PostMapping("/save")
    @ApiOperation(value = "新增图片资源FTP服务器表")
    public ApiResult save(@RequestBody SysFtpVO vo) {
        SysFtpDTO dto = BeanUtil.copyProperties(vo, SysFtpDTO.class);
        return sysFtpService.save(dto);
    }

    /**
     * 修改图片资源FTP服务器表
     */
    @LogAnno("修改图片资源FTP服务器表")
    @PostMapping("/editSave")
    @ApiOperation(value = "修改图片资源FTP服务器表")
    public ApiResult editSave(@RequestBody SysFtpVO vo) {
        SysFtpDTO dto = BeanUtil.copyProperties(vo, SysFtpDTO.class);
        return sysFtpService.update(dto);
    }

    /**
     * 逻辑删除图片资源FTP服务器表
     */
    @LogAnno("删除图片资源FTP服务器表")
    @PostMapping("/delete")
    @ApiOperation(value = "根据id删除进度计划")
    public ApiResult delete(@RequestBody SysFtpVO vo) {
        SysFtpDTO dto = BeanUtil.copyProperties(vo, SysFtpDTO.class);
        return sysFtpService.delete(dto.getId());
    }
}
