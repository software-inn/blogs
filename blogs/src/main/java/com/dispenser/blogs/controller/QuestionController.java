package com.dispenser.blogs.controller;

import com.baomidou.mybatisplus.extension.api.ApiController;
import com.dispenser.blogs.common.ApiResult;
import com.dispenser.blogs.common.BeanUtil;
import com.dispenser.blogs.model.dto.QuestionDTO;
import com.dispenser.blogs.model.qo.QuestionQO;
import com.dispenser.blogs.model.vo.QuestionVO;
import com.dispenser.blogs.service.QuestionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;



/**
 * 问题
 *
 */
@RestController
@RequestMapping("/api/question")
@Api(tags = "问题")
public class QuestionController extends ApiController {


    @Autowired
    private QuestionService questionService;

    /*
     * 分页获取问题列表
	 */
    @PostMapping("/list")
    @ApiOperation(value = "问题分页")
    public ApiResult list(@RequestBody QuestionQO qo) {
        return questionService.list(qo);
    }

    /**
   * 根据id查询问题
   */
    @GetMapping("/get/{id}")
    @ApiOperation(value = "根据id查询问题")
    public ApiResult get(@PathVariable(name = "id", required = true) Long id) {
        return questionService.get(id);
    }

    /**
     * 新增问题
     */
    @PostMapping("/save")
    @ApiOperation(value = "新增问题")
    public ApiResult save(@RequestBody QuestionVO vo) {
        QuestionDTO dto = BeanUtil.copyProperties(vo, QuestionDTO.class);
        return questionService.save(dto);
    }

    /**
     * 修改问题
     */
    @PostMapping("/editSave")
    @ApiOperation(value = "修改问题")
    public ApiResult editSave(@RequestBody QuestionVO vo) {
        QuestionDTO dto = BeanUtil.copyProperties(vo, QuestionDTO.class);
        return questionService.update(dto);
    }

    /**
     * 逻辑删除问题
     */
    @PostMapping("/delete")
    @ApiOperation(value = "根据id删除进度计划")
    public ApiResult delete(@RequestBody QuestionVO vo) {
        QuestionDTO dto = BeanUtil.copyProperties(vo, QuestionDTO.class);
        return questionService.delete(dto.getId());
    }
}
