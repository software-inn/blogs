package com.dispenser.blogs.controller;

import com.baomidou.mybatisplus.extension.api.ApiController;
import com.dispenser.blogs.common.ApiResult;
import com.dispenser.blogs.common.BeanUtil;
import com.dispenser.blogs.model.dto.CounterDTO;
import com.dispenser.blogs.model.qo.CounterQO;
import com.dispenser.blogs.model.vo.CounterVO;
import com.dispenser.blogs.service.CounterService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;



/**
 * 
 *
 */
@RestController
@RequestMapping("/api/counter")
@Api(tags = "计数器")
public class CounterController extends ApiController {


    @Autowired
    private CounterService counterService;

    /*
     * 分页获取列表
	 */
    @PostMapping("/list")
    @ApiOperation(value = "分页")
    public ApiResult list(@RequestBody CounterQO qo) {
        return counterService.list(qo);
    }

    /**
   * 根据id查询
   */
    @GetMapping("/get/{id}")
    @ApiOperation(value = "根据id查询")
    public ApiResult get(@PathVariable(name = "id", required = true) Long id) {
        return counterService.get(id);
    }

    /**
     * 新增
     */
    @PostMapping("/save")
    @ApiOperation(value = "新增")
    public ApiResult save(@RequestBody CounterVO vo) {
        CounterDTO dto = BeanUtil.copyProperties(vo, CounterDTO.class);
        return counterService.save(dto);
    }

    /**
     * 修改
     */
    @PostMapping("/editSave")
    @ApiOperation(value = "修改")
    public ApiResult editSave(@RequestBody CounterVO vo) {
        CounterDTO dto = BeanUtil.copyProperties(vo, CounterDTO.class);
        return counterService.update(dto);
    }

    /**
     * 逻辑删除
     */
    @PostMapping("/delete")
    @ApiOperation(value = "根据id删除进度计划")
    public ApiResult delete(@RequestBody CounterVO vo) {
        CounterDTO dto = BeanUtil.copyProperties(vo, CounterDTO.class);
        return counterService.delete(dto.getId());
    }
}
