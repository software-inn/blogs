package com.dispenser.blogs.controller;

import com.baomidou.mybatisplus.extension.api.ApiController;
import com.dispenser.blogs.common.ApiResult;
import com.dispenser.blogs.common.BeanUtil;
import com.dispenser.blogs.model.dto.TagsDTO;
import com.dispenser.blogs.model.qo.TagsQO;
import com.dispenser.blogs.model.vo.TagsVO;
import com.dispenser.blogs.service.TagsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;



/**
 * 标签
 *
 */
@RestController
@RequestMapping("/api/tags")
@Api(tags = "标签")
public class TagsController extends ApiController {


    @Autowired
    private TagsService tagsService;

    /*
     * 分页获取标签列表
     */
    @PostMapping("/all")
    @ApiOperation(value = "标签分页")
    public ApiResult all(@RequestBody TagsQO qo) {
        return tagsService.all(qo);
    }

    /*
     * 分页获取标签列表
     */
    @PostMapping("/list")
    @ApiOperation(value = "标签分页")
    public ApiResult list(@RequestBody TagsQO qo) {
        return tagsService.list(qo);
    }

    /**
   * 根据id查询标签
   */
    @GetMapping("/get/{id}")
    @ApiOperation(value = "根据id查询标签")
    public ApiResult get(@PathVariable(name = "id", required = true) Long id) {
        return tagsService.get(id);
    }

    /**
     * 新增标签
     */
    @PostMapping("/save")
    @ApiOperation(value = "新增标签")
    public ApiResult save(@RequestBody TagsVO vo) {
        TagsDTO dto = BeanUtil.copyProperties(vo, TagsDTO.class);
        return tagsService.save(dto);
    }

    /**
     * 修改标签
     */
    @PostMapping("/editSave")
    @ApiOperation(value = "修改标签")
    public ApiResult editSave(@RequestBody TagsVO vo) {
        TagsDTO dto = BeanUtil.copyProperties(vo, TagsDTO.class);
        return tagsService.update(dto);
    }

    /**
     * 逻辑删除标签
     */
    @PostMapping("/delete")
    @ApiOperation(value = "根据id删除进度计划")
    public ApiResult delete(@RequestBody TagsVO vo) {
        TagsDTO dto = BeanUtil.copyProperties(vo, TagsDTO.class);
        return tagsService.delete(dto.getId());
    }
}
