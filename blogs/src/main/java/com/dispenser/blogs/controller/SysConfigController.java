package com.dispenser.blogs.controller;

import com.baomidou.mybatisplus.extension.api.ApiController;
import com.dispenser.blogs.annotation.LogAnno;
import com.dispenser.blogs.common.ApiResult;
import com.dispenser.blogs.common.BeanUtil;
import com.dispenser.blogs.model.dto.SysConfigDTO;
import com.dispenser.blogs.model.qo.SysConfigQO;
import com.dispenser.blogs.model.vo.SysConfigVO;
import com.dispenser.blogs.service.SysConfigService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;



/**
 * 系统配置表
 *
 */
@RestController
@RequestMapping("/api/sysConfig")
@Api(tags = "系统配置表")
public class SysConfigController extends ApiController {


    @Autowired
    private SysConfigService sysConfigService;

    /*
     * 分页获取系统配置表列表
	 */
    @PostMapping("/list")
    @ApiOperation(value = "系统配置表分页")
    public ApiResult list(@RequestBody SysConfigQO qo) {
        return sysConfigService.list(qo);
    }

    /**
   * 根据id查询系统配置表
   */
    @GetMapping("/get/{id}")
    @ApiOperation(value = "根据id查询系统配置表")
    public ApiResult get(@PathVariable(name = "id", required = true) Long id) {
        return sysConfigService.get(id);
    }

    /**
     * 新增系统配置表
     */
    @LogAnno("修改系统配置表")
    @PostMapping("/save")
    @ApiOperation(value = "新增系统配置表")
    public ApiResult save(@RequestBody SysConfigVO vo) {
        SysConfigDTO dto = BeanUtil.copyProperties(vo, SysConfigDTO.class);
        return sysConfigService.save(dto);
    }

    /**
     * 修改系统配置表
     */
    @LogAnno("修改系统配置表")
    @PostMapping("/editSave")
    @ApiOperation(value = "修改系统配置表")
    public ApiResult editSave(@RequestBody SysConfigVO vo) {
        SysConfigDTO dto = BeanUtil.copyProperties(vo, SysConfigDTO.class);
        return sysConfigService.update(dto);
    }

    /**
     * 逻辑删除系统配置表
     */
    @LogAnno("删除系统配置表")
    @PostMapping("/delete")
    @ApiOperation(value = "根据id删除进度计划")
    public ApiResult delete(@RequestBody SysConfigVO vo) {
        SysConfigDTO dto = BeanUtil.copyProperties(vo, SysConfigDTO.class);
        return sysConfigService.delete(dto.getId());
    }
}
