/**
 * Copyright &copy; 2012-2017 <a href="http://minglisoft.cn">HongHu</a> All rights reserved.
 */
package com.dispenser.blogs.controller;

import com.auth0.jwt.internal.org.bouncycastle.jce.provider.BouncyCastleProvider;
import com.dispenser.blogs.annotation.UserAnno;
import com.dispenser.blogs.common.*;
import com.dispenser.blogs.common.wx.WXCore;
import com.dispenser.blogs.model.entity.Attachment;
import com.dispenser.blogs.model.entity.SysConfig;
import com.dispenser.blogs.model.entity.User;
import com.dispenser.blogs.model.enums.UserEnum;
import com.dispenser.blogs.service.SysConfigService;
import com.dispenser.blogs.service.SysFtpService;
import com.dispenser.blogs.service.UserService;
import lombok.extern.slf4j.Slf4j;
import net.sf.json.JSONObject;
import org.apache.commons.lang.StringUtils;
import org.bouncycastle.util.encoders.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.AlgorithmParameters;
import java.security.Security;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 微信小程序登录Controller
 * @author Administrator
 *
 */
@Slf4j
@RestController
@RequestMapping(value = "/weChat")
public class WeChatLoginController  {

	@Autowired
	private RedisTemplates redisTemplates;
	@Autowired
    private UserService userService;
	@Autowired
    private SysConfigService sysConfigService;
	@Autowired
    private SysFtpService sysFtpService;

	private static final Logger logger = LoggerFactory.getLogger(WeChatLoginController.class);

	/***微信获取登录凭证url***/
	private static final String CODE2SESSION_URL = "https://api.weixin.qq.com/sns/jscode2session";


	/**
	 * 小程序登录
	 * @param js_code 登录code
	 * @param iv  加密算法的初始向量
	 * @param encryptedData 敏感用户数据
	 * @param share_uid 邀请注册用户
	 * @return
	 */
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public ApiResult login(@RequestBody JSONObject json) {
		String js_code = json.optString("js_code");
		String iv = json.optString("iv");
		String encryptedData = json.optString("encryptedData");

		SysConfig sysConfig = sysConfigService.getById(1);
		// 微信APPID、微信密匙
		String appid = sysConfig.getWeixinAppid();
		String secret = sysConfig.getWeixinAppsecret();
		if(StringUtils.isEmpty(appid) || StringUtils.isEmpty(secret)){
			return ApiResultUtils.failureWithMsg("系统未配置appid和secret");
		}

		Map<String,String> params = new HashMap<String, String>();
		params.put("appid", appid);
		params.put("secret", secret);
		params.put("js_code", js_code);
		params.put("grant_type", "authorization_code");
		String jsonStr = HttpClientUtils.doGet(CODE2SESSION_URL, params, "UTF-8");
		JSONObject jsonObj = JSONObject.fromObject(jsonStr);

		// 错误码
		String errcode = jsonObj.optString("errcode");
		if(StringUtils.equals(errcode, "40029")){ // code 无效
			return ApiResultUtils.failureWithMsg("code 无效！");
		}

		// errcode[-1:系统繁忙，此时请开发者稍候再试;45011:频率限制，每个用户每分钟100次]
		if(StringUtils.equals(errcode, "45011") || StringUtils.equals(errcode, "-1")){
			return ApiResultUtils.failureWithMsg("系统繁忙，请稍后再试！");
		}

		String session_key = jsonObj.optString("session_key");
		String openid = jsonObj.optString("openid");

		logger.info("登入获取的用户openid:"+openid);

		// 会话密钥和用户唯一标识为空，表示服务请求失败，记录日志
		if(StringUtils.isEmpty(session_key) || StringUtils.isEmpty(openid)){
			return ApiResultUtils.failureWithMsg("code无效！");
		}

		// 解析小程序加密用户数据
		String userInfo = WXCore.decrypt(appid, encryptedData, session_key, iv);
		if(StringUtils.isEmpty(userInfo)){
			return ApiResultUtils.failureWithMsg("获取用户数据失败！");
		}
		JSONObject userJson = JSONObject.fromObject(userInfo);
		// 开放平台的唯一标识符
		String unionid = userJson.optString("unionId");
		if(StringUtils.isEmpty(unionid)){
			return ApiResultUtils.failureWithMsg("获取用户数据失败！");
		}

		boolean phone=false;
		// 根据开放平台的唯一标识签，查询用户对象是否存在，不存在注册一条数据
		User user = userService.selectByUnionid(unionid);
		if (user!=null&&user.getUserName()!=null&&StringUtils.isNotBlank(user.getMobile())) {
			phone=true;
		}

		//boolean flag = false;   //true 则不需要授权手机号
		if(user == null){
			//flag = true;
			user = new User();
			user.setOpenId(openid);
			user.setNickName(userJson.optString("nickName"));
			user.setUserName(onlyUserName());
			user.setUnionId(unionid);
			user.setDelState(0);
			user.setAddTime(new Date());
			user.setPassword(Md5Encrypt.md5("123456").toLowerCase());//设置默认密码为123456
			user.setAge(0); // 用户年龄
			user.setUserType(0); // 用户类别，默认为0个人用户，1为管理员
			userService.save(user);
		}
		boolean isUpdate=false;
		// 处理用户头像(用户头像不存在，将微信的头像保存到用户头像)
		String photo_url = userJson.optString("avatarUrl");
		if(user.getAttachmentId() == null && StringUtils.isNotEmpty(photo_url)){
			Attachment photo = uploadPhotoFile(photo_url, user.getId());
			if(photo != null){
				user.setAttachmentId(photo.getId());
				isUpdate=true;
			}
		}
		if (isUpdate) {
			userService.updateById(user);
		}
		//更换openid
		user.setOpenId(openid);
		userService.updateById(user);

		// 生成token,格式:用户id;时间戳
		String token = JWT.sign(user.getId() + ";" + System.currentTimeMillis(), 0);
		// 将token存到redis中，有效期24小时
		redisTemplates.set(UserEnum.USER_TOKEN.getValue() + user.getId(), token, Integer.MAX_VALUE);
		HashMap<String, Object> result = new HashMap<>();
		result.put("token", token);
		result.put("phone", phone);
		result.put("session_key", session_key);
		return ApiResultUtils.success(result);
	}

	//给用户生成一个唯一的userName
	public String onlyUserName(){
		String userName = "ys_wx_";
		Random r = new Random();
		for (int i = 0; i < 7; i++) {
			userName += r.nextInt(10);
		}
		User user = userService.selectByUserName(userName);
		if(user == null){
			return userName;
		}
		return onlyUserName();
	}

	/**
	 * 获取用户
	 * @return
	 */
	@RequestMapping(value = "/savePhone", method = RequestMethod.POST)
	public ApiResult savePhone(HttpServletRequest request, @UserAnno User userLogin, @RequestBody JSONObject json) {
		System.out.println("---------------------json---------------------");
		System.out.println(json);
		System.out.println("---------------------json---------------------");
		String session_key = json.optString("session_key");
		String iv = json.optString("iv");
		String encryptedData = json.optString("encrypData");

		if (StringUtils.isBlank(session_key)) {
			return ApiResultUtils.failureWithMsg("获取失败,session_key信息错误");

		}
		if (StringUtils.isBlank(iv)) {
			return ApiResultUtils.failureWithMsg("获取失败,iv信息错误");
		}
		if (StringUtils.isBlank(encryptedData)) {
			return ApiResultUtils.failureWithMsg("获取失败,encryptedData信息错误");

		}
		byte[] dataByte = Base64.decode(encryptedData);
		// 加密秘钥
		byte[] keyByte = Base64.decode(session_key);
		// 偏移量
		byte[] ivByte = Base64.decode(iv);
		try {
			// 如果密钥不足16位，那么就补足. 这个if 中的内容很重要
			int base = 16;
			if (keyByte.length % base != 0) {
				int groups = keyByte.length / base + (keyByte.length % base != 0 ? 1 : 0);
				byte[] temp = new byte[groups * base];
				Arrays.fill(temp, (byte) 0);
				System.arraycopy(keyByte, 0, temp, 0, keyByte.length);
				keyByte = temp;
			}
			// 初始化
			Security.addProvider(new BouncyCastleProvider());
			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
			SecretKeySpec spec = new SecretKeySpec(keyByte, "AES");
			AlgorithmParameters parameters = AlgorithmParameters.getInstance("AES");
			parameters.init(new IvParameterSpec(ivByte));
			cipher.init(Cipher.DECRYPT_MODE, spec, parameters);// 初始化
			byte[] resultByte = cipher.doFinal(dataByte);
			if (null != resultByte && resultByte.length > 0) {
				String result = new String(resultByte, "UTF-8");

				JSONObject fromObject = JSONObject.fromObject(result);
				Object object = fromObject.get("purePhoneNumber");
				User user = userService.getById(userLogin.getId());
				user.setUserName(String.valueOf(object));
				user.setMobile(String.valueOf(object));
				userService.updateById(user);
				return ApiResultUtils.success();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ApiResultUtils.failureWithMsg("信息有误");
	}




	/**
	* @Description 将字符串中的emoji表情转换成可以在utf-8字符集数据库中保存的格式（表情占4个字节，需要utf8mb4字符集）
	* @param str
	* 待转换字符串
	* @return 转换后字符串
	* @throws UnsupportedEncodingException
	* exception
	*/
	public static String emojiConvert1(String str) {
		String patternString = "([\\x{10000}-\\x{10ffff}\ud800-\udfff])";
		Pattern pattern = Pattern.compile(patternString);
		Matcher matcher = pattern.matcher(str);
		StringBuffer sb = new StringBuffer();
		while (matcher.find()) {
			try {
				matcher.appendReplacement(sb, "[[" + URLEncoder.encode(matcher.group(1), "UTF-8") + "]]");
			} catch (Exception e) {
			}
		}
		matcher.appendTail(sb);
		return sb.toString();
	}


	/**
	 * 微信授权登录上传头像
	 * @param photo_url
	 * @param user_id
	 * @return
	 */
	public Attachment uploadPhotoFile(String photo_url, Long user_id){
		try {
            Attachment attachment = sysFtpService.upload("userImage", null, "JPG", photo_url);
			return attachment;
		} catch (Exception e) {
			log.error("微信授权登录上传头像失败:" + e.getMessage());
		}
		return null;
	}

}