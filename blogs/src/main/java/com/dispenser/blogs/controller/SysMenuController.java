package com.dispenser.blogs.controller;

import com.baomidou.mybatisplus.extension.api.ApiController;
import com.dispenser.blogs.annotation.LogAnno;
import com.dispenser.blogs.common.ApiResult;
import com.dispenser.blogs.common.BeanUtil;
import com.dispenser.blogs.model.dto.SysMenuDTO;
import com.dispenser.blogs.model.qo.SysMenuQO;
import com.dispenser.blogs.model.vo.SysMenuVO;
import com.dispenser.blogs.service.SysMenuService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;



/**
 * 系统菜单表
 *
 */
@RestController
@RequestMapping("/api/sysMenu")
@Api(tags = "系统菜单表")
public class SysMenuController extends ApiController {


    @Autowired
    private SysMenuService sysMenuService;

    /*
     * 分页获取系统菜单表列表
	 */
    @PostMapping("/list")
    @ApiOperation(value = "系统菜单表分页")
    public ApiResult list(@RequestBody SysMenuQO qo) {
        return sysMenuService.list(qo);
    }


    /*
     * 获取系统菜单表列表
     */
    @PostMapping("/all")
    @ApiOperation(value = "系统菜单表分页")
    public ApiResult all(@RequestBody SysMenuQO qo) {
        return sysMenuService.all(qo);
    }

    /**
   * 根据id查询系统菜单表
   */
    @GetMapping("/get/{id}")
    @ApiOperation(value = "根据id查询系统菜单表")
    public ApiResult get(@PathVariable(name = "id", required = true) Long id) {
        return sysMenuService.get(id);
    }

    /**
     * 新增系统菜单表
     */
    @LogAnno("新增系统菜单表")
    @PostMapping("/save")
    @ApiOperation(value = "新增系统菜单表")
    public ApiResult save(@RequestBody SysMenuVO vo) {
        SysMenuDTO dto = BeanUtil.copyProperties(vo, SysMenuDTO.class);
        return sysMenuService.save(dto);
    }

    /**
     * 修改系统菜单表
     */
    @LogAnno("修改系统菜单表")
    @PostMapping("/editSave")
    @ApiOperation(value = "修改系统菜单表")
    public ApiResult editSave(@RequestBody SysMenuVO vo) {
        SysMenuDTO dto = BeanUtil.copyProperties(vo, SysMenuDTO.class);
        return sysMenuService.update(dto);
    }

    /**
     * 逻辑删除系统菜单表
     */
    @LogAnno("删除系统菜单表")
    @PostMapping("/delete")
    @ApiOperation(value = "根据id删除进度计划")
    public ApiResult delete(@RequestBody SysMenuVO vo) {
        SysMenuDTO dto = BeanUtil.copyProperties(vo, SysMenuDTO.class);
        return sysMenuService.delete(dto.getId());
    }
}
