package com.dispenser.blogs.controller;

import com.baomidou.mybatisplus.extension.api.ApiController;
import com.dispenser.blogs.common.ApiResult;
import com.dispenser.blogs.common.BeanUtil;
import com.dispenser.blogs.model.dto.QuestionResultDTO;
import com.dispenser.blogs.model.qo.QuestionResultQO;
import com.dispenser.blogs.model.vo.QuestionResultVO;
import com.dispenser.blogs.service.QuestionResultService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;



/**
 * 问题答案-基本用于选择题
 *
 */
@RestController
@RequestMapping("/api/questionResult")
@Api(tags = "问题答案-基本用于选择题")
public class QuestionResultController extends ApiController {


    @Autowired
    private QuestionResultService questionResultService;

    /*
     * 分页获取问题答案-基本用于选择题列表
	 */
    @PostMapping("/list")
    @ApiOperation(value = "问题答案-基本用于选择题分页")
    public ApiResult list(@RequestBody QuestionResultQO qo) {
        return questionResultService.list(qo);
    }

    /**
   * 根据id查询问题答案-基本用于选择题
   */
    @GetMapping("/get/{id}")
    @ApiOperation(value = "根据id查询问题答案-基本用于选择题")
    public ApiResult get(@PathVariable(name = "id", required = true) Long id) {
        return questionResultService.get(id);
    }

    /**
     * 新增问题答案-基本用于选择题
     */
    @PostMapping("/save")
    @ApiOperation(value = "新增问题答案-基本用于选择题")
    public ApiResult save(@RequestBody QuestionResultVO vo) {
        QuestionResultDTO dto = BeanUtil.copyProperties(vo, QuestionResultDTO.class);
        return questionResultService.save(dto);
    }

    /**
     * 修改问题答案-基本用于选择题
     */
    @PostMapping("/editSave")
    @ApiOperation(value = "修改问题答案-基本用于选择题")
    public ApiResult editSave(@RequestBody QuestionResultVO vo) {
        QuestionResultDTO dto = BeanUtil.copyProperties(vo, QuestionResultDTO.class);
        return questionResultService.update(dto);
    }

    /**
     * 逻辑删除问题答案-基本用于选择题
     */
    @PostMapping("/delete")
    @ApiOperation(value = "根据id删除进度计划")
    public ApiResult delete(@RequestBody QuestionResultVO vo) {
        QuestionResultDTO dto = BeanUtil.copyProperties(vo, QuestionResultDTO.class);
        return questionResultService.delete(dto.getId());
    }
}
