package com.dispenser.blogs.controller;

import com.baomidou.mybatisplus.extension.api.ApiController;
import com.dispenser.blogs.common.ApiResult;
import com.dispenser.blogs.common.BeanUtil;
import com.dispenser.blogs.model.dto.ArticleTagsDTO;
import com.dispenser.blogs.model.qo.ArticleTagsQO;
import com.dispenser.blogs.model.vo.ArticleTagsVO;
import com.dispenser.blogs.service.ArticleTagsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;



/**
 * 文章与标签中间表
 *
 */
@RestController
@RequestMapping("/api/articleTags")
@Api(tags = "文章与标签中间表")
public class ArticleTagsController extends ApiController {


    @Autowired
    private ArticleTagsService articleTagsService;

    /*
     * 分页获取文章与标签中间表列表
	 */
    @PostMapping("/list")
    @ApiOperation(value = "文章与标签中间表分页")
    public ApiResult list(@RequestBody ArticleTagsQO qo) {
        return articleTagsService.list(qo);
    }

    /**
   * 根据id查询文章与标签中间表
   */
    @GetMapping("/get/{id}")
    @ApiOperation(value = "根据id查询文章与标签中间表")
    public ApiResult get(@PathVariable(name = "id", required = true) Long id) {
        return articleTagsService.get(id);
    }

    /**
     * 新增文章与标签中间表
     */
    @PostMapping("/save")
    @ApiOperation(value = "新增文章与标签中间表")
    public ApiResult save(@RequestBody ArticleTagsVO vo) {
        ArticleTagsDTO dto = BeanUtil.copyProperties(vo, ArticleTagsDTO.class);
        return articleTagsService.save(dto);
    }

    /**
     * 修改文章与标签中间表
     */
    @PostMapping("/editSave")
    @ApiOperation(value = "修改文章与标签中间表")
    public ApiResult editSave(@RequestBody ArticleTagsVO vo) {
        ArticleTagsDTO dto = BeanUtil.copyProperties(vo, ArticleTagsDTO.class);
        return articleTagsService.update(dto);
    }

    /**
     * 逻辑删除文章与标签中间表
     */
    @PostMapping("/delete")
    @ApiOperation(value = "根据id删除进度计划")
    public ApiResult delete(@RequestBody ArticleTagsVO vo) {
        ArticleTagsDTO dto = BeanUtil.copyProperties(vo, ArticleTagsDTO.class);
        return articleTagsService.delete(dto.getId());
    }
}
