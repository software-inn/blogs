package com.dispenser.blogs.controller;

import com.baomidou.mybatisplus.extension.api.ApiController;
import com.dispenser.blogs.common.ApiResult;
import com.dispenser.blogs.common.BeanUtil;
import com.dispenser.blogs.model.dto.SysLogDTO;
import com.dispenser.blogs.model.qo.SysLogQO;
import com.dispenser.blogs.model.vo.SysLogVO;
import com.dispenser.blogs.service.SysLogService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;



/**
 * 系统日志
 *
 */
@RestController
@RequestMapping("/api/sysLog")
@Api(tags = "系统日志")
public class SysLogController extends ApiController {


    @Autowired
    private SysLogService sysLogService;

    /*
     * 分页获取系统日志列表
	 */
    @PostMapping("/list")
    @ApiOperation(value = "系统日志分页")
    public ApiResult list(@RequestBody SysLogQO qo) {
        return sysLogService.list(qo);
    }

    /**
   * 根据id查询系统日志
   */
    @GetMapping("/get/{id}")
    @ApiOperation(value = "根据id查询系统日志")
    public ApiResult get(@PathVariable(name = "id", required = true) Long id) {
        return sysLogService.get(id);
    }

    /**
     * 新增系统日志
     */
    @PostMapping("/save")
    @ApiOperation(value = "新增系统日志")
    public ApiResult save(@RequestBody SysLogVO vo) {
        SysLogDTO dto = BeanUtil.copyProperties(vo, SysLogDTO.class);
        return sysLogService.save(dto);
    }

    /**
     * 修改系统日志
     */
    @PostMapping("/editSave")
    @ApiOperation(value = "修改系统日志")
    public ApiResult editSave(@RequestBody SysLogVO vo) {
        SysLogDTO dto = BeanUtil.copyProperties(vo, SysLogDTO.class);
        return sysLogService.update(dto);
    }

    /**
     * 逻辑删除系统日志
     */
    @PostMapping("/delete")
    @ApiOperation(value = "根据id删除进度计划")
    public ApiResult delete(@RequestBody SysLogVO vo) {
        SysLogDTO dto = BeanUtil.copyProperties(vo, SysLogDTO.class);
        return sysLogService.delete(dto.getId());
    }
}
