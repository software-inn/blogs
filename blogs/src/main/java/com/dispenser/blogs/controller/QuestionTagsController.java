package com.dispenser.blogs.controller;

import com.baomidou.mybatisplus.extension.api.ApiController;
import com.dispenser.blogs.common.ApiResult;
import com.dispenser.blogs.common.BeanUtil;
import com.dispenser.blogs.model.dto.QuestionTagsDTO;
import com.dispenser.blogs.model.qo.QuestionTagsQO;
import com.dispenser.blogs.model.vo.QuestionTagsVO;
import com.dispenser.blogs.service.QuestionTagsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;



/**
 * 问题与标签中间表
 *
 */
@RestController
@RequestMapping("/api/questionTags")
@Api(tags = "问题与标签中间表")
public class QuestionTagsController extends ApiController {


    @Autowired
    private QuestionTagsService questionTagsService;

    /*
     * 分页获取问题与标签中间表列表
	 */
    @PostMapping("/list")
    @ApiOperation(value = "问题与标签中间表分页")
    public ApiResult list(@RequestBody QuestionTagsQO qo) {
        return questionTagsService.list(qo);
    }

    /**
   * 根据id查询问题与标签中间表
   */
    @GetMapping("/get/{id}")
    @ApiOperation(value = "根据id查询问题与标签中间表")
    public ApiResult get(@PathVariable(name = "id", required = true) Long id) {
        return questionTagsService.get(id);
    }

    /**
     * 新增问题与标签中间表
     */
    @PostMapping("/save")
    @ApiOperation(value = "新增问题与标签中间表")
    public ApiResult save(@RequestBody QuestionTagsVO vo) {
        QuestionTagsDTO dto = BeanUtil.copyProperties(vo, QuestionTagsDTO.class);
        return questionTagsService.save(dto);
    }

    /**
     * 修改问题与标签中间表
     */
    @PostMapping("/editSave")
    @ApiOperation(value = "修改问题与标签中间表")
    public ApiResult editSave(@RequestBody QuestionTagsVO vo) {
        QuestionTagsDTO dto = BeanUtil.copyProperties(vo, QuestionTagsDTO.class);
        return questionTagsService.update(dto);
    }

    /**
     * 逻辑删除问题与标签中间表
     */
    @PostMapping("/delete")
    @ApiOperation(value = "根据id删除进度计划")
    public ApiResult delete(@RequestBody QuestionTagsVO vo) {
        QuestionTagsDTO dto = BeanUtil.copyProperties(vo, QuestionTagsDTO.class);
        return questionTagsService.delete(dto.getId());
    }
}
