package com.dispenser.blogs.common;

import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.HttpServletRequest;

public class UserContentHandler {

    public static Long getUserId(HttpServletRequest request){

        Object user_id = request.getAttribute("user_id");
        if(StringUtils.isBlank(String.valueOf(user_id))){
            return null;
        }
        return Long.valueOf(String.valueOf(user_id));
    }
}
