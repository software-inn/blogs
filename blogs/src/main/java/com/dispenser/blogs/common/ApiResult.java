package com.dispenser.blogs.common;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("接口返回对象")
public class ApiResult<T> {
    @ApiModelProperty("返回状态码")
    private int code;
    @ApiModelProperty("返回信息")
    private String msg;
    @ApiModelProperty("返回数据")
    private T data;

    public ApiResult(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

}

