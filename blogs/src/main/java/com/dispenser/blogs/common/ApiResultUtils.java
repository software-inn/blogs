package com.dispenser.blogs.common;

import com.dispenser.blogs.model.enums.ApiResultEnum;

public class ApiResultUtils<T> {
    public ApiResultUtils() {
    }



    public static ApiResult<String> success() {
        return success("");
    }

    public static <T> ApiResult<T> success(T data) {
        return new ApiResult(ApiResultEnum.SUCCESS.getCode(),ApiResultEnum.SUCCESS.getMsg(),data);
    }


    public static <T> ApiResult<T> failureWithMsg(String msg) {
        return new ApiResult(ApiResultEnum.UNKNOWN.getCode(),ApiResultEnum.UNKNOWN.getMsg(),msg);
    }

    public static <T> ApiResult<T> failure() {
        return new ApiResult(ApiResultEnum.UNKNOWN.getCode(),ApiResultEnum.UNKNOWN.getMsg());
    }
    public static <T> ApiResult<T> failure(Integer code,String msg) {
        return new ApiResult(code,msg);
    }
}
