package com.dispenser.blogs.common;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageInfo;
import com.google.common.collect.Lists;
import org.springframework.beans.BeanUtils;

import java.util.List;

public class BeanUtil {
        public BeanUtil() {
        }

        public static <T> T copyProperties(Object source, Class<T> target) {
            T targetObject = null;
            if (null == source) {
                return null;
            } else {
                try {
                    targetObject = target.newInstance();
                } catch (IllegalAccessException | InstantiationException var4) {
                    var4.printStackTrace();
                }

                copyProperties(source, targetObject);
                return targetObject;
            }
        }

        public static void copyProperties(Object source, Object target) {
            BeanUtils.copyProperties(source, target);
        }

        public static <M> List<M> transfer(List<?> list, Class<M> clazz) {
            List<M> mList = Lists.newArrayList();
            list.forEach((o) -> {
                mList.add(copyProperties(o, clazz));
            });
            return mList;
        }

        public static <M> PageInfo<M> transferPageInfo(List<?> list, Class<M> clazz) {
            if (list instanceof Page) {
                PageInfo pageInfo = new PageInfo(list);
                List<M> mList = transfer(list, clazz);
                pageInfo.setList(mList);
                return pageInfo;
            } else {
                return null;
            }
        }

        public static void transfer(Object source, List list) {
            list.forEach((o) -> {
                copyProperties(source, o);
            });
        }


}
