package com.dispenser.blogs.model.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;


/**
 * 系统菜单表传输对象
 */
@Setter
@Getter
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("系统菜单表数据传输对象")
public class SysMenuDTO {


	/**  **/
	@ApiModelProperty(value="ID")
	private Long id;


	/* 添加时间，这里为长时间格式 */
	@ApiModelProperty(value="添加时间，这里为长时间格式")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date addTime;


	/* 是否删除,默认为0未删除，-1表示删除状态 */
	@ApiModelProperty(value="是否删除,默认为0未删除，-1表示删除状态")
	private Integer delState;


	/* 菜单名称 */
	@ApiModelProperty(value="菜单名称")
	private String title;


	/* 菜单地址 */
	@ApiModelProperty(value="菜单地址")
	private String path;


	/* 英文名称 */
	@ApiModelProperty(value="英文名称")
	private String value;


	/* 父菜单 */
	@ApiModelProperty(value="父菜单")
	private Integer parentId;


	/* 排序 */
	@ApiModelProperty(value="排序")
	private Integer sequence;


	/* 菜单图标 */
	@ApiModelProperty(value="菜单图标")
	private String icon;

	/* 隐藏显示 */
	@ApiModelProperty(value="隐藏显示")
	private Integer display;



}
