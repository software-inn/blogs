package com.dispenser.blogs.model.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum UserEnum {

    USER_TOKEN("USER_TOKEN");

    private String value;
}
