package com.dispenser.blogs.model.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.util.Date;


/**
 * 图片资源FTP服务器表实体
 */
@TableName(value="`b_sys_ftp`",resultMap = "BaseResultMap")
@Setter
@Getter
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("图片资源FTP服务器表实体")
@JsonIgnoreProperties(value = { "handler"})
public class SysFtp implements Serializable  {
	private static final long serialVersionUID = 1L;

	@TableId(type = IdType.AUTO)
	@TableField(value="`id`")
	@ApiModelProperty(value = "ID")
	private Long id;



	@TableField(value="`add_time`")
	@ApiModelProperty(value = "添加时间，这里为长时间格式")
	private Date addTime;



	@TableField(value="`del_state`")
	@ApiModelProperty(value = "是否删除,默认为0未删除，-1表示删除状态")
	private Integer delState;



	@TableField(value="`ftp_addr`")
	@ApiModelProperty(value = " FTP服务器地址,优先使用服务器地址访问，如果没有域名，则使用服务器ip访问，需带有http:")
	private String ftpAddr;



	@TableField(value="`ftp_amount`")
	@ApiModelProperty(value = " ftp所拥有用户数量上限，默认100")
	private Integer ftpAmount;



	@TableField(value="`ftp_ip`")
	@ApiModelProperty(value = " FTP ip地址,用于图片上传，不可带有http:")
	private String ftpIp;



	@TableField(value="`ftp_name`")
	@ApiModelProperty(value = " FTP服务器名称")
	private String ftpName;



	@TableField(value="`ftp_password`")
	@ApiModelProperty(value = " 链接FTP用户密码")
	private String ftpPassword;



	@TableField(value="`ftp_sequence`")
	@ApiModelProperty(value = " 排序")
	private Integer ftpSequence;



	@TableField(value="`ftp_system`")
	@ApiModelProperty(value = " 是否为当前使用的系统服务器，1为是，系统可以有多个系统ftp，只能同时使用一个")
	private Integer ftpSystem;



	@TableField(value="`ftp_type`")
	@ApiModelProperty(value = " 服务器类型，0为用户服务器，1为系统服务器，系统服务器可以有多个，但同时只能使用一个")
	private Integer ftpType;



	@TableField(value="`ftp_username`")
	@ApiModelProperty(value = " 链接FTP用户名")
	private String ftpUsername;



	@TableField(value="`ftp_port`")
	@ApiModelProperty(value = "")
	private Integer ftpPort;

}
