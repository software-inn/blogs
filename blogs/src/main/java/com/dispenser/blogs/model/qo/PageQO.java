package com.dispenser.blogs.model.qo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("分页条件")
public class PageQO {
    @ApiModelProperty(
            value = "每页数量",
            example = "10"
    )
    private Integer limit;
    @ApiModelProperty(
            value = "页数",
            example = "1"
    )
    private Integer page;
    @ApiModelProperty(
            value = "排序字段",
            example = "id",
            hidden = true
    )
    private String sidx;
    @ApiModelProperty(
            value = "正序ASC/倒序DESC",
            example = "ASC",
            hidden = true
    )
    private String sord;
    @ApiModelProperty(
            value = "用来分页计算开始位置",
            hidden = true
    )
    private Integer startIdx;

    public PageQO() {
    }

    public Integer getLimit() {
        return this.limit != null && this.limit > 0 ? this.limit : 10;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getPage() {
        return this.page != null && this.page > 0 ? this.page : 1;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public String getSidx() {
        return this.sidx;
    }

    public void setSidx(String sidx) {
        this.sidx = sidx;
    }

    public String getSord() {
        return this.sord;
    }

    public void setSord(String sord) {
        this.sord = sord;
    }

    public Integer getStartIdx() {
        return (this.getPage() - 1) * this.getLimit();
    }

    public void setStartIdx(Integer startIdx) {
        this.startIdx = startIdx;
    }
}

