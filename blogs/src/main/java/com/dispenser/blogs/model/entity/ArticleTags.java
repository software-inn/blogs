package com.dispenser.blogs.model.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.util.Date;


/**
 * 文章与标签中间表实体
 */
@TableName(value="`b_article_tags`",resultMap = "BaseResultMap")
@Setter
@Getter
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("文章与标签中间表实体")
@JsonIgnoreProperties(value = { "handler"})
public class ArticleTags implements Serializable  {
	private static final long serialVersionUID = 1L;

	@TableId(type = IdType.AUTO)
	@TableField(value="`id`")
	@ApiModelProperty(value = "ID")
	private Long id;



	@TableField(value="`add_time`")
	@ApiModelProperty(value = "添加时间，这里为长时间格式")
	private Date addTime;



	@TableField(value="`del_state`")
	@ApiModelProperty(value = "是否删除,默认为0未删除，-1表示删除状态")
	private Integer delState;



	@TableField(value="`article_id`")
	@ApiModelProperty(value = "文章id")
	private Long articleId;



	@TableField(value="`tags_id`")
	@ApiModelProperty(value = "标签id")
	private Long tagsId;

}
