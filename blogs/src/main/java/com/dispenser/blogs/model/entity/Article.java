package com.dispenser.blogs.model.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.util.Date;
import java.util.List;


/**
 * 文章实体
 */
@TableName(value="`b_article`",resultMap = "BaseResultMap")
@Setter
@Getter
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("文章实体")
@JsonIgnoreProperties(value = { "handler"})
public class Article implements Serializable  {
	private static final long serialVersionUID = 1L;

	@TableId(type = IdType.AUTO)
	@TableField(value="`id`")
	@ApiModelProperty(value = "ID")
	private Long id;



	@TableField(value="`add_time`")
	@ApiModelProperty(value = "添加时间，这里为长时间格式")
	private Date addTime;



	@TableField(value="`del_state`")
	@ApiModelProperty(value = "是否删除,默认为0未删除，-1表示删除状态")
	private Integer delState;



	@TableField(value="`hot`")
	@ApiModelProperty(value = "是否热门显示,默认为0 否 ，1是")
	private Integer hot;



	@TableField(value="`title`")
	@ApiModelProperty(value = "标题")
	private String title;



	@TableField(value="`cover_image_id`")
	@ApiModelProperty(value = "封面图片")
	private Long coverImageId;



	@TableField(value="`comment`")
	@ApiModelProperty(value = "描述")
	private String comment;



	@TableField(value="`content`")
	@ApiModelProperty(value = "内容")
	private String content;



	@TableField(value="`like`")
	@ApiModelProperty(value = "赞")
	private Integer like;



	@TableField(value="`tread`")
	@ApiModelProperty(value = "踩")
	private Integer tread;



	@TableField(value="`collect`")
	@ApiModelProperty(value = "收藏")
	private Integer collect;



	@TableField(value="`class_id`")
	@ApiModelProperty(value = "分类")
	private Long classId;



	@TableField(value="`vista`")
	@ApiModelProperty(value = "访问量")
	private Integer vista;

	/* 推荐指数 */
	@TableField(value="`level`")
	@ApiModelProperty(value="推荐指数")
	private Integer level;

	/* 状态0草稿1发布 */
	@TableField(value="`status`")
	@ApiModelProperty(value="状态0草稿1发布")
	private Integer status;

	/* 图片 */
	@TableField(exist = false)
	@ApiModelProperty(value="图片")
	Attachment attachment;

	/* 分类 */
	@TableField(exist = false)
	@ApiModelProperty(value="分类")
	ArticleClass articleClass;

	/* 标签 */
	@TableField(exist = false)
	@ApiModelProperty(value="标签")
	List<Tags> tagsList;
}
