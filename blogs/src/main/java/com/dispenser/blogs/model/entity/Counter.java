package com.dispenser.blogs.model.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;



/**
 * 实体
 */
@TableName(value="`b_counter`",resultMap = "BaseResultMap")
@Setter
@Getter
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("实体")
@JsonIgnoreProperties(value = { "handler"})
public class Counter implements Serializable  {
	private static final long serialVersionUID = 1L;

	@TableId(type = IdType.AUTO)
	@TableField(value="`id`")
	@ApiModelProperty(value = "ID")
	private Long id;



	@TableField(value="`count`")
	@ApiModelProperty(value = "数字")
	private Integer count;

}
