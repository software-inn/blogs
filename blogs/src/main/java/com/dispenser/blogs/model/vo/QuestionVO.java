package com.dispenser.blogs.model.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;

import java.util.Date;


/**
 * 问题传输对象
 */
@Setter
@Getter
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@ApiModel("问题数据传输对象")
public class QuestionVO implements Serializable{
	private static final long serialVersionUID = 1L;

	/**  **/
	@ApiModelProperty(value="ID")
	private Long id;


	/* 添加时间，这里为长时间格式 */
	@ApiModelProperty(value="添加时间，这里为长时间格式")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date addTime;


	/* 是否删除,默认为0未删除，-1表示删除状态 */
	@ApiModelProperty(value="是否删除,默认为0未删除，-1表示删除状态")
	private Integer delState;


	/* 数据来源 */
	@ApiModelProperty(value="数据来源")
	private String source;


	/* 排序，从小到大 */
	@ApiModelProperty(value="排序，从小到大")
	private Integer sequence;


	/* 类型，0单选 1判断  2多选  3解答题 */
	@ApiModelProperty(value="类型，0单选 1判断  2多选  3解答题")
	private Integer type;


	/* 难度 0 1 2 3 4 等级别 */
	@ApiModelProperty(value="难度 0 1 2 3 4 等级别")
	private Integer level;


	/* 分类 */
	@ApiModelProperty(value="分类")
	private Integer classId;


	/* 标题 */
	@ApiModelProperty(value="标题")
	private String title;


	/* 问题详情 */
	@ApiModelProperty(value="问题详情")
	private String detail;


	/* 问题解析 */
	@ApiModelProperty(value="问题解析")
	private String parse;


	/* 内容 */
	@ApiModelProperty(value="内容")
	private String content;


	/* 赞 */
	@ApiModelProperty(value="赞")
	private Integer like;


	/* 踩 */
	@ApiModelProperty(value="踩")
	private Integer tread;


	/* 收藏 */
	@ApiModelProperty(value="收藏")
	private Integer collect;


	/* 访问量 */
	@ApiModelProperty(value="访问量")
	private Long vista;

}
