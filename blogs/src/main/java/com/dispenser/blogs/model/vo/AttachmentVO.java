package com.dispenser.blogs.model.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;

import java.math.BigDecimal;
import java.util.Date;


/**
 * 图片表传输对象
 */
@Setter
@Getter
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@ApiModel("图片表数据传输对象")
public class AttachmentVO implements Serializable{
	private static final long serialVersionUID = 1L;

	/**  **/
	@ApiModelProperty(value="ID")
	private Long id;


	/* 添加时间，这里为长时间格式 */
	@ApiModelProperty(value="添加时间，这里为长时间格式")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date addTime;


	/* 是否删除,默认为0未删除，-1表示删除状态 */
	@ApiModelProperty(value="是否删除,默认为0未删除，-1表示删除状态")
	private Integer delState;


	/*  扩展名，不包括. */
	@ApiModelProperty(value=" 扩展名，不包括.")
	private String ext;


	/*  高度 */
	@ApiModelProperty(value=" 高度")
	private Integer height;


	/*  附件说明 */
	@ApiModelProperty(value=" 附件说明")
	private String info;


	/*  附件名称 */
	@ApiModelProperty(value=" 附件名称")
	private String name;


	/*  存放路径 */
	@ApiModelProperty(value=" 存放路径")
	private String path;


	/*  附件大小 */
	@ApiModelProperty(value=" 附件大小")
	private BigDecimal size;


	/*  宽度 */
	@ApiModelProperty(value=" 宽度")
	private Integer width;

}
