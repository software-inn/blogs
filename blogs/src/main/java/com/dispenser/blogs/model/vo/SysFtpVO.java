package com.dispenser.blogs.model.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;

import java.util.Date;


/**
 * 图片资源FTP服务器表传输对象
 */
@Setter
@Getter
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@ApiModel("图片资源FTP服务器表数据传输对象")
public class SysFtpVO implements Serializable{
	private static final long serialVersionUID = 1L;

	/**  **/
	@ApiModelProperty(value="ID")
	private Long id;


	/* 添加时间，这里为长时间格式 */
	@ApiModelProperty(value="添加时间，这里为长时间格式")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date addTime;


	/* 是否删除,默认为0未删除，-1表示删除状态 */
	@ApiModelProperty(value="是否删除,默认为0未删除，-1表示删除状态")
	private Integer delState;


	/*  FTP服务器地址,优先使用服务器地址访问，如果没有域名，则使用服务器ip访问，需带有http: */
	@ApiModelProperty(value=" FTP服务器地址,优先使用服务器地址访问，如果没有域名，则使用服务器ip访问，需带有http:")
	private String ftpAddr;


	/*  ftp所拥有用户数量上限，默认100 */
	@ApiModelProperty(value=" ftp所拥有用户数量上限，默认100")
	private Integer ftpAmount;


	/*  FTP ip地址,用于图片上传，不可带有http: */
	@ApiModelProperty(value=" FTP ip地址,用于图片上传，不可带有http:")
	private String ftpIp;


	/*  FTP服务器名称 */
	@ApiModelProperty(value=" FTP服务器名称")
	private String ftpName;


	/*  链接FTP用户密码 */
	@ApiModelProperty(value=" 链接FTP用户密码")
	private String ftpPassword;


	/*  排序 */
	@ApiModelProperty(value=" 排序")
	private Integer ftpSequence;


	/*  是否为当前使用的系统服务器，1为是，系统可以有多个系统ftp，只能同时使用一个 */
	@ApiModelProperty(value=" 是否为当前使用的系统服务器，1为是，系统可以有多个系统ftp，只能同时使用一个")
	private Integer ftpSystem;


	/*  服务器类型，0为用户服务器，1为系统服务器，系统服务器可以有多个，但同时只能使用一个 */
	@ApiModelProperty(value=" 服务器类型，0为用户服务器，1为系统服务器，系统服务器可以有多个，但同时只能使用一个")
	private Integer ftpType;


	/*  链接FTP用户名 */
	@ApiModelProperty(value=" 链接FTP用户名")
	private String ftpUsername;


	/*  */
	@ApiModelProperty(value="")
	private Integer ftpPort;

}
