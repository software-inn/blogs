package com.dispenser.blogs.model.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.util.Date;


/**
 * 问题实体
 */
@TableName(value="`b_question`",resultMap = "BaseResultMap")
@Setter
@Getter
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("问题实体")
@JsonIgnoreProperties(value = { "handler"})
public class Question implements Serializable  {
	private static final long serialVersionUID = 1L;

	@TableId(type = IdType.AUTO)
	@TableField(value="`id`")
	@ApiModelProperty(value = "ID")
	private Long id;



	@TableField(value="`add_time`")
	@ApiModelProperty(value = "添加时间，这里为长时间格式")
	private Date addTime;



	@TableField(value="`del_state`")
	@ApiModelProperty(value = "是否删除,默认为0未删除，-1表示删除状态")
	private Integer delState;



	@TableField(value="`source`")
	@ApiModelProperty(value = "数据来源")
	private String source;



	@TableField(value="`sequence`")
	@ApiModelProperty(value = "排序，从小到大")
	private Integer sequence;



	@TableField(value="`type`")
	@ApiModelProperty(value = "类型，0单选 1判断  2多选  3解答题")
	private Integer type;



	@TableField(value="`level`")
	@ApiModelProperty(value = "难度 0 1 2 3 4 等级别")
	private Integer level;



	@TableField(value="`class_id`")
	@ApiModelProperty(value = "分类")
	private Integer classId;



	@TableField(value="`title`")
	@ApiModelProperty(value = "标题")
	private String title;



	@TableField(value="`detail`")
	@ApiModelProperty(value = "问题详情")
	private String detail;



	@TableField(value="`parse`")
	@ApiModelProperty(value = "问题解析")
	private String parse;



	@TableField(value="`content`")
	@ApiModelProperty(value = "内容")
	private String content;



	@TableField(value="`like`")
	@ApiModelProperty(value = "赞")
	private Integer like;



	@TableField(value="`tread`")
	@ApiModelProperty(value = "踩")
	private Integer tread;



	@TableField(value="`collect`")
	@ApiModelProperty(value = "收藏")
	private Integer collect;



	@TableField(value="`vista`")
	@ApiModelProperty(value = "访问量")
	private Long vista;

}
