package com.dispenser.blogs.model.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.util.Date;


/**
 * 用户表实体
 */
@TableName(value="`b_user`",resultMap = "BaseResultMap")
@Setter
@Getter
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("用户表实体")
@JsonIgnoreProperties(value = { "handler"})
public class User implements Serializable  {
	private static final long serialVersionUID = 1L;

	@TableId(type = IdType.AUTO)
	@TableField(value="`id`")
	@ApiModelProperty(value = "ID")
	private Long id;



	@TableField(value="`add_time`")
	@ApiModelProperty(value = "添加时间，这里为长时间格式")
	private Date addTime;



	@TableField(value="`del_state`")
	@ApiModelProperty(value = "是否删除,默认为0未删除，-1表示删除状态")
	private Integer delState;



	@TableField(value="`birthday`")
	@ApiModelProperty(value = " 出生日期")
	private Date birthday;



	@TableField(value="`email`")
	@ApiModelProperty(value = " 邮箱地址")
	private String email;



	@TableField(value="`last_login_date`")
	@ApiModelProperty(value = " 上次登陆时间")
	private Date lastLoginDate;



	@TableField(value="`last_login_ip`")
	@ApiModelProperty(value = " 上次登录IP")
	private String lastLoginIp;



	@TableField(value="`login_count`")
	@ApiModelProperty(value = " 登录次数")
	private Integer loginCount;



	@TableField(value="`login_date`")
	@ApiModelProperty(value = " 登陆时间")
	private Date loginDate;



	@TableField(value="`login_ip`")
	@ApiModelProperty(value = " 登陆Ip")
	private String loginIp;



	@TableField(value="`mobile`")
	@ApiModelProperty(value = " 手机支付密码，用户手机端使用预存款支付时需要输入支付密码，如果用户没有设置支付密码需要输入登录密码")
	private String mobile;



	@TableField(value="`nick_name`")
	@ApiModelProperty(value = " 昵称")
	private String nickName;



	@TableField(value="`open_id`")
	@ApiModelProperty(value = " 微信公众平台使用的openid")
	private String openId;



	@TableField(value="`password`")
	@ApiModelProperty(value = " 手机支付密码，用户手机端使用预存款支付时需要输入支付密码，如果用户没有设置支付密码需要输入登录密码")
	private String password;



	@TableField(value="`sex`")
	@ApiModelProperty(value = " 性别 1为男、0为女、-1为保密")
	private Integer sex;



	@TableField(value="`user_name`")
	@ApiModelProperty(value = " 用户名")
	private String userName;



	@TableField(value="`user_type`")
	@ApiModelProperty(value = " 用户类别，默认为0个人用户，1管理员")
	private Integer userType;



	@TableField(value="`union_id`")
	@ApiModelProperty(value = " 微信公众平台使用unionId")
	private String unionId;



	@TableField(value="`age`")
	@ApiModelProperty(value = " 用户年龄")
	private Integer age;



	@TableField(value="`signature`")
	@ApiModelProperty(value = "个性签名")
	private String signature;



	@TableField(value="`integral`")
	@ApiModelProperty(value = "用户积分")
	private Long integral;



	@TableField(value="`attachment_id`")
	@ApiModelProperty(value = "用户头像")
	private Long attachmentId;


	/* 用户签到次数 */
	@TableField(value="`clock`")
	@ApiModelProperty(value="用户签到次数")
	private Integer clock;

	/* 用户头像 */
	@TableField(exist = false)
	@ApiModelProperty(value="用户头像")
	Attachment attachment;
}
