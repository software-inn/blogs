package com.dispenser.blogs.model.enums;

public enum ApiResultEnum {
        SUCCESS(20000, "成功"),
        FAILURE(50000, "系统异常"),
        UNKNOWN(50001, "未知异常"),
        PASSWORD_ERROR(10010001, "密码错误");

        private Integer code;
        private String msg;

        private ApiResultEnum(Integer code, String msg) {
                this.code = code;
                this.msg = msg;
        }

        public Integer getCode() {
                return this.code;
        }

        public void setCode(Integer code) {
                this.code = code;
        }

        public String getMsg() {
                return this.msg;
        }

        public void setMsg(String msg) {
                this.msg = msg;
        }
}
