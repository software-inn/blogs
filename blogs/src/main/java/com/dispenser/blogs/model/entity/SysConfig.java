package com.dispenser.blogs.model.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.util.Date;


/**
 * 系统配置表实体
 */
@TableName(value="`b_sys_config`",resultMap = "BaseResultMap")
@Setter
@Getter
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("系统配置表实体")
@JsonIgnoreProperties(value = { "handler"})
public class SysConfig implements Serializable  {
	private static final long serialVersionUID = 1L;

	@TableId(type = IdType.AUTO)
	@TableField(value="`id`")
	@ApiModelProperty(value = "ID")
	private Long id;



	@TableField(value="`add_time`")
	@ApiModelProperty(value = "添加时间，这里为长时间格式")
	private Date addTime;



	@TableField(value="`del_state`")
	@ApiModelProperty(value = "是否删除,默认为0未删除，-1表示删除状态")
	private Integer delState;



	@TableField(value="`email_enable`")
	@ApiModelProperty(value = " 邮件是否开启")
	private Boolean emailEnable;



	@TableField(value="`email_host`")
	@ApiModelProperty(value = " stmp服务器")
	private String emailHost;



	@TableField(value="`email_port`")
	@ApiModelProperty(value = " stmp端口")
	private Integer emailPort;



	@TableField(value="`email_pwss`")
	@ApiModelProperty(value = " 邮箱密码")
	private String emailPwss;



	@TableField(value="`email_user`")
	@ApiModelProperty(value = " 邮箱用户名")
	private String emailUser;



	@TableField(value="`every_answer_integrate`")
	@ApiModelProperty(value = " 答题(赠送积分)")
	private Integer everyAnswerIntegrate;



	@TableField(value="`image_file_size`")
	@ApiModelProperty(value = " 允许图片上传的最大值")
	private Integer imageFileSize;



	@TableField(value="`login_integrate`")
	@ApiModelProperty(value = " 会员每日登陆(赠送积分)")
	private Integer loginIntegrate;



	@TableField(value="`upload_file_path`")
	@ApiModelProperty(value = " 用户上传文件路径")
	private String uploadFilePath;



	@TableField(value="`upload_test_bank_path`")
	@ApiModelProperty(value = " 试题文件路径")
	private String uploadTestBankPath;



	@TableField(value="`weixin_appId`")
	@ApiModelProperty(value = " 微信App Id，申请开发者成功后微信生成的AppId")
	private String weixinAppid;



	@TableField(value="`weixin_appSecret`")
	@ApiModelProperty(value = " 微信AppSecret，申请开发者成功后微信生成的AppSecret")
	private String weixinAppsecret;



	@TableField(value="`Website_ico_id`")
	@ApiModelProperty(value = " 网站ICO图片")
	private Long websiteIcoId;



	@TableField(value="`admin_left_logo_id`")
	@ApiModelProperty(value = "后台管理左上角Logo")
	private Long adminLeftLogoId;



	@TableField(value="`user_icon_id`")
	@ApiModelProperty(value = " 默认用户图标")
	private Long userIconId;



	@TableField(value="`website_logo_id`")
	@ApiModelProperty(value = " 网站logo")
	private Long websiteLogoId;



	@TableField(value="`admin_url`")
	@ApiModelProperty(value = "管理url")
	private String adminUrl;



	@TableField(value="`index_url`")
	@ApiModelProperty(value = "首页url")
	private String indexUrl;



	@TableField(value="`sharding_strategy`")
	@ApiModelProperty(value = "分库分表策略,也就是按照多大的数据量分一个库表，填写数字，最优是一百万：1000000，默认一百万分一个库表。")
	private Long shardingStrategy;



	@TableField(value="`static_version`")
	@ApiModelProperty(value = "静态资源版本")
	private String staticVersion;

	/* 图片服务器 Nginx 代理的地址*/
	@TableField(value="`image_server_address`")
	@ApiModelProperty(value="图片服务器 Nginx 代理的地址")
	private String imageServerAddress;

}
