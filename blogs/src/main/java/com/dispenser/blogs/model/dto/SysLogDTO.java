package com.dispenser.blogs.model.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;


/**
 * 系统日志传输对象
 */
@Setter
@Getter
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("系统日志数据传输对象")
public class SysLogDTO {


	/**  **/
	@ApiModelProperty(value="ID")
	private Long id;


	/* 用户名 */
	@ApiModelProperty(value="用户名")
	private String username;


	/* 用户操作 */
	@ApiModelProperty(value="用户操作")
	private String operation;


	/* 请求方法 */
	@ApiModelProperty(value="请求方法")
	private String method;


	/* 请求参数 */
	@ApiModelProperty(value="请求参数")
	private String params;


	/* 执行时长(毫秒) */
	@ApiModelProperty(value="执行时长(毫秒)")
	private Long time;


	/* IP地址 */
	@ApiModelProperty(value="IP地址")
	private String ip;


	/* 创建时间 */
	@ApiModelProperty(value="创建时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date addTime;


	/* 删除状态 0 否 1 是 */
	@ApiModelProperty(value="删除状态 0 否 1 是")
	private Integer delState;

}
