package com.dispenser.blogs.model.dto;

import com.dispenser.blogs.model.entity.ArticleClass;
import com.dispenser.blogs.model.entity.Attachment;
import com.dispenser.blogs.model.entity.Tags;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;


/**
 * 文章传输对象
 */
@Setter
@Getter
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("文章数据传输对象")
public class ArticleDTO {


	/**  **/
	@ApiModelProperty(value="ID")
	private Long id;


	/* 添加时间，这里为长时间格式 */
	@ApiModelProperty(value="添加时间，这里为长时间格式")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date addTime;


	/* 是否删除,默认为0未删除，-1表示删除状态 */
	@ApiModelProperty(value="是否删除,默认为0未删除，-1表示删除状态")
	private Integer delState;


	/* 是否热门显示,默认为0 否 ，1是 */
	@ApiModelProperty(value="是否热门显示,默认为0 否 ，1是")
	private Integer hot;


	/* 标题 */
	@ApiModelProperty(value="标题")
	private String title;


	/* 封面图片 */
	@ApiModelProperty(value="封面图片")
	private Long coverImageId;


	/* 描述 */
	@ApiModelProperty(value="描述")
	private String comment;


	/* 内容 */
	@ApiModelProperty(value="内容")
	private String content;


	/* 赞 */
	@ApiModelProperty(value="赞")
	private Integer like;


	/* 踩 */
	@ApiModelProperty(value="踩")
	private Integer tread;


	/* 收藏 */
	@ApiModelProperty(value="收藏")
	private Integer collect;


	/* 分类 */
	@ApiModelProperty(value="分类")
	private Long classId;


	/* 访问量 */
	@ApiModelProperty(value="访问量")
	private Integer vista;

	/* 推荐指数 */
	@ApiModelProperty(value="推荐指数")
	private Integer level;

	/* 状态0草稿1发布 */
	@ApiModelProperty(value="状态0草稿1发布")
	private Integer status;

	/* 图片 */
	@ApiModelProperty(value="图片")
	Attachment attachment;

	/* 分类 */
	@ApiModelProperty(value="分类")
	ArticleClass articleClass;

	/* 标签 */
	@ApiModelProperty(value="标签")
	List<Tags> tagsList;
}
