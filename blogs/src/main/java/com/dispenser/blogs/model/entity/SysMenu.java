package com.dispenser.blogs.model.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.util.Date;


/**
 * 系统菜单表实体
 */
@TableName(value="`b_sys_menu`",resultMap = "BaseResultMap")
@Setter
@Getter
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("系统菜单表实体")
@JsonIgnoreProperties(value = { "handler"})
public class SysMenu implements Serializable  {
	private static final long serialVersionUID = 1L;

	@TableId(type = IdType.AUTO)
	@TableField(value="`id`")
	@ApiModelProperty(value = "ID")
	private Long id;



	@TableField(value="`add_time`")
	@ApiModelProperty(value = "添加时间，这里为长时间格式")
	private Date addTime;



	@TableField(value="`del_state`")
	@ApiModelProperty(value = "是否删除,默认为0未删除，-1表示删除状态")
	private Integer delState;



	@TableField(value="`title`")
	@ApiModelProperty(value = "菜单名称")
	private String title;



	@TableField(value="`path`")
	@ApiModelProperty(value = "菜单地址")
	private String path;



	@TableField(value="`value`")
	@ApiModelProperty(value = "英文名称")
	private String value;



	@TableField(value="`parent_id`")
	@ApiModelProperty(value = "父菜单")
	private Integer parentId;



	@TableField(value="`sequence`")
	@ApiModelProperty(value = "排序")
	private Integer sequence;



	@TableField(value="`icon`")
	@ApiModelProperty(value = "菜单图标")
	private String icon;

	/* 隐藏显示 */
	@TableField(value="`display`")
	@ApiModelProperty(value="隐藏显示")
	private Integer display;

}
