package com.dispenser.blogs.model.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.dispenser.blogs.model.entity.Attachment;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;

import java.util.Date;


/**
 * 用户表传输对象
 */
@Setter
@Getter
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@ApiModel("用户表数据传输对象")
public class UserVO implements Serializable{
	private static final long serialVersionUID = 1L;

	/**  **/
	@ApiModelProperty(value="ID")
	private Long id;


	/* 添加时间，这里为长时间格式 */
	@ApiModelProperty(value="添加时间，这里为长时间格式")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date addTime;


	/* 是否删除,默认为0未删除，-1表示删除状态 */
	@ApiModelProperty(value="是否删除,默认为0未删除，-1表示删除状态")
	private Integer delState;


	/*  出生日期 */
	@ApiModelProperty(value=" 出生日期")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date birthday;


	/*  邮箱地址 */
	@ApiModelProperty(value=" 邮箱地址")
	private String email;


	/*  上次登陆时间 */
	@ApiModelProperty(value=" 上次登陆时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date lastLoginDate;


	/*  上次登录IP */
	@ApiModelProperty(value=" 上次登录IP")
	private String lastLoginIp;


	/*  登录次数 */
	@ApiModelProperty(value=" 登录次数")
	private Integer loginCount;


	/*  登陆时间 */
	@ApiModelProperty(value=" 登陆时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date loginDate;


	/*  登陆Ip */
	@ApiModelProperty(value=" 登陆Ip")
	private String loginIp;


	/*  手机支付密码，用户手机端使用预存款支付时需要输入支付密码，如果用户没有设置支付密码需要输入登录密码 */
	@ApiModelProperty(value=" 手机支付密码，用户手机端使用预存款支付时需要输入支付密码，如果用户没有设置支付密码需要输入登录密码")
	private String mobile;


	/*  昵称 */
	@ApiModelProperty(value=" 昵称")
	private String nickName;


	/*  微信公众平台使用的openid */
	@ApiModelProperty(value=" 微信公众平台使用的openid")
	private String openId;


	/*  手机支付密码，用户手机端使用预存款支付时需要输入支付密码，如果用户没有设置支付密码需要输入登录密码 */
	@ApiModelProperty(value=" 手机支付密码，用户手机端使用预存款支付时需要输入支付密码，如果用户没有设置支付密码需要输入登录密码")
	private String password;


	/*  性别 1为男、0为女、-1为保密 */
	@ApiModelProperty(value=" 性别 1为男、0为女、-1为保密")
	private Integer sex;


	/*  用户名 */
	@ApiModelProperty(value=" 用户名")
	private String userName;


	/*  用户类别，默认为0个人用户，1管理员 */
	@ApiModelProperty(value=" 用户类别，默认为0个人用户，1管理员")
	private Integer userType;


	/*  微信公众平台使用unionId */
	@ApiModelProperty(value=" 微信公众平台使用unionId")
	private String unionId;


	/*  用户年龄 */
	@ApiModelProperty(value=" 用户年龄")
	private Integer age;


	/* 个性签名 */
	@ApiModelProperty(value="个性签名")
	private String signature;


	/* 用户积分 */
	@ApiModelProperty(value="用户积分")
	private Long integral;


	/* 用户头像 */
	@ApiModelProperty(value="用户头像")
	private Long attachmentId;

	/* 用户签到次数 */
	@ApiModelProperty(value="用户签到次数")
	private Integer clock;

	/* 用户头像 */
	@ApiModelProperty(value="用户头像")
	Attachment attachment;


}
