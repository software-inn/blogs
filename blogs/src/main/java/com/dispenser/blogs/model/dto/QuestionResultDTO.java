package com.dispenser.blogs.model.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;


/**
 * 问题答案-基本用于选择题传输对象
 */
@Setter
@Getter
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("问题答案-基本用于选择题数据传输对象")
public class QuestionResultDTO {


	/**  **/
	@ApiModelProperty(value="ID")
	private Long id;


	/* 添加时间，这里为长时间格式 */
	@ApiModelProperty(value="添加时间，这里为长时间格式")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date addTime;


	/* 是否删除,默认为0未删除，-1表示删除状态 */
	@ApiModelProperty(value="是否删除,默认为0未删除，-1表示删除状态")
	private Integer delState;


	/* 问题id */
	@ApiModelProperty(value="问题id")
	private Long questionId;


	/* 答案顺序 */
	@ApiModelProperty(value="答案顺序")
	private String sequence;


	/* 答案内容 */
	@ApiModelProperty(value="答案内容")
	private String content;


	/* 0 否 1 正确 */
	@ApiModelProperty(value="0 否 1 正确")
	private Integer right;

}
