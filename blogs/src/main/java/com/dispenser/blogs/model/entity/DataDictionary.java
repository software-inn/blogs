package com.dispenser.blogs.model.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.util.Date;


/**
 * 数据字典表实体
 */
@TableName(value="`b_data_dictionary`",resultMap = "BaseResultMap")
@Setter
@Getter
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("数据字典表实体")
@JsonIgnoreProperties(value = { "handler"})
public class DataDictionary implements Serializable  {
	private static final long serialVersionUID = 1L;

	@TableId(type = IdType.AUTO)
	@TableField(value="`id`")
	@ApiModelProperty(value = "ID")
	private Long id;



	@TableField(value="`parent_id`")
	@ApiModelProperty(value = "父级")
	private Long parentId;



	@TableField(value="`numbers`")
	@ApiModelProperty(value = "数据编码")
	private String numbers;



	@TableField(value="`label`")
	@ApiModelProperty(value = "标签")
	private String label;



	@TableField(value="`name`")
	@ApiModelProperty(value = "名称")
	private String name;



	@TableField(value="`value`")
	@ApiModelProperty(value = "内容")
	private String value;



	@TableField(value="`sort`")
	@ApiModelProperty(value = "排序")
	private Integer sort;



	@TableField(value="`created_time`")
	@ApiModelProperty(value = "创建日期")
	private Date createdTime;



	@TableField(value="`created_id`")
	@ApiModelProperty(value = "创建人id")
	private Long createdId;



	@TableField(value="`created_name`")
	@ApiModelProperty(value = "创建人名称")
	private String createdName;



	@TableField(value="`updated_time`")
	@ApiModelProperty(value = "修改日期")
	private Date updatedTime;



	@TableField(value="`updated_id`")
	@ApiModelProperty(value = "修改人id")
	private Long updatedId;



	@TableField(value="`updated_name`")
	@ApiModelProperty(value = "修改人名称")
	private String updatedName;



	@TableField(value="`del_state`")
	@ApiModelProperty(value = "是否删除,默认为0未删除，1表示删除状态")
	private Integer delState;

}
