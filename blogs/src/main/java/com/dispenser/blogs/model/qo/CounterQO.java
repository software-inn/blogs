package com.dispenser.blogs.model.qo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;


/**
 * 传输对象
 */
@Setter
@Getter
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@ApiModel("数据传输对象")
public class CounterQO extends PageQO {


	/**  **/
	@ApiModelProperty(value="ID")
	private Long id;


	/* 数字 */
	@ApiModelProperty(value="数字")
	private Integer count;

}
