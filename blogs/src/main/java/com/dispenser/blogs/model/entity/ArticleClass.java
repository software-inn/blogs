package com.dispenser.blogs.model.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.util.Date;


/**
 * 分类实体
 */
@TableName(value="`b_article_class`",resultMap = "BaseResultMap")
@Setter
@Getter
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("分类实体")
@JsonIgnoreProperties(value = { "handler"})
public class ArticleClass implements Serializable  {
	private static final long serialVersionUID = 1L;

	@TableId(type = IdType.AUTO)
	@TableField(value="`id`")
	@ApiModelProperty(value = "ID")
	private Long id;



	@TableField(value="`add_time`")
	@ApiModelProperty(value = "添加时间，这里为长时间格式")
	private Date addTime;



	@TableField(value="`del_state`")
	@ApiModelProperty(value = "是否删除,默认为0未删除，-1表示删除状态")
	private Integer delState;



	@TableField(value="`display`")
	@ApiModelProperty(value = "是否显示,默认为0 显示 ，1隐藏")
	private Integer display;



	@TableField(value="`icon_id`")
	@ApiModelProperty(value = "图标id")
	private Long iconId;



	@TableField(value="`comment`")
	@ApiModelProperty(value = "描述")
	private String comment;



	@TableField(value="`sequence`")
	@ApiModelProperty(value = "排序，从小到大")
	private Integer sequence;



	@TableField(value="`class_name`")
	@ApiModelProperty(value = "分类名称")
	private String className;



	@TableField(value="`class_type`")
	@ApiModelProperty(value = "类型，用于扩展")
	private Integer classType;



	@TableField(value="`parent_id`")
	@ApiModelProperty(value = "父级id")
	private Long parentId;

	/* 父级 */
	@TableField(exist = false)
	@ApiModelProperty(value="父级")
	ArticleClass articleClass;

	/* 图片 */
	@TableField(exist = false)
	@ApiModelProperty(value="图片")
	Attachment attachment;

}
