package com.dispenser.blogs.model.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.util.Date;


/**
 * 问题答案-基本用于选择题实体
 */
@TableName(value="`b_question_result`",resultMap = "BaseResultMap")
@Setter
@Getter
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("问题答案-基本用于选择题实体")
@JsonIgnoreProperties(value = { "handler"})
public class QuestionResult implements Serializable  {
	private static final long serialVersionUID = 1L;

	@TableId(type = IdType.AUTO)
	@TableField(value="`id`")
	@ApiModelProperty(value = "ID")
	private Long id;



	@TableField(value="`add_time`")
	@ApiModelProperty(value = "添加时间，这里为长时间格式")
	private Date addTime;



	@TableField(value="`del_state`")
	@ApiModelProperty(value = "是否删除,默认为0未删除，-1表示删除状态")
	private Integer delState;



	@TableField(value="`question_id`")
	@ApiModelProperty(value = "问题id")
	private Long questionId;



	@TableField(value="`sequence`")
	@ApiModelProperty(value = "答案顺序")
	private String sequence;



	@TableField(value="`content`")
	@ApiModelProperty(value = "答案内容")
	private String content;



	@TableField(value="`right`")
	@ApiModelProperty(value = "0 否 1 正确")
	private Integer right;

}
