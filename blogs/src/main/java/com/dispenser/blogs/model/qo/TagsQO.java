package com.dispenser.blogs.model.qo;


import com.dispenser.blogs.model.entity.Attachment;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * 标签传输对象
 */
@Setter
@Getter
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@ApiModel("标签数据传输对象")
public class TagsQO extends PageQO {


	/**  **/
	@ApiModelProperty(value="ID")
	private Long id;


	/* 添加时间，这里为长时间格式 */
	@ApiModelProperty(value="添加时间，这里为长时间格式")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date addTime;


	/* 是否删除,默认为0未删除，-1表示删除状态 */
	@ApiModelProperty(value="是否删除,默认为0未删除，-1表示删除状态")
	private Integer delState;


	/* 是否显示,默认为0 显示 ，1隐藏 */
	@ApiModelProperty(value="是否显示,默认为0 显示 ，1隐藏")
	private Integer display;


	/* 图标id */
	@ApiModelProperty(value="图标id")
	private Long iconId;


	/* 描述 */
	@ApiModelProperty(value="描述")
	private String comment;


	/* 排序，从小到大 */
	@ApiModelProperty(value="排序，从小到大")
	private Integer sequence;


	/* 标签名称 */
	@ApiModelProperty(value="标签名称")
	private String tagsName;


	/* 类型，用于扩展 */
	@ApiModelProperty(value="类型，用于扩展")
	private Integer tagsType;

	/* 图片 */
	@ApiModelProperty(value="图片")
	Attachment attachment;
}
