package com.dispenser.blogs.model.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.util.Date;


/**
 * 系统日志实体
 */
@TableName(value="`b_sys_log`",resultMap = "BaseResultMap")
@Setter
@Getter
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("系统日志实体")
@JsonIgnoreProperties(value = { "handler"})
public class SysLog implements Serializable  {
	private static final long serialVersionUID = 1L;

	@TableId(type = IdType.AUTO)
	@TableField(value="`id`")
	@ApiModelProperty(value = "ID")
	private Long id;



	@TableField(value="`username`")
	@ApiModelProperty(value = "用户名")
	private String username;



	@TableField(value="`operation`")
	@ApiModelProperty(value = "用户操作")
	private String operation;



	@TableField(value="`method`")
	@ApiModelProperty(value = "请求方法")
	private String method;



	@TableField(value="`params`")
	@ApiModelProperty(value = "请求参数")
	private String params;



	@TableField(value="`time`")
	@ApiModelProperty(value = "执行时长(毫秒)")
	private Long time;



	@TableField(value="`ip`")
	@ApiModelProperty(value = "IP地址")
	private String ip;



	@TableField(value="`add_time`")
	@ApiModelProperty(value = "创建时间")
	private Date addTime;



	@TableField(value="`del_state`")
	@ApiModelProperty(value = "删除状态 0 否 1 是")
	private Integer delState;

}
