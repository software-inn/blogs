package com.dispenser.blogs.model.qo;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * 文章与标签中间表传输对象
 */
@Setter
@Getter
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@ApiModel("文章与标签中间表数据传输对象")
public class ArticleTagsQO extends PageQO {


	/**  **/
	@ApiModelProperty(value="ID")
	private Long id;


	/* 添加时间，这里为长时间格式 */
	@ApiModelProperty(value="添加时间，这里为长时间格式")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date addTime;


	/* 是否删除,默认为0未删除，-1表示删除状态 */
	@ApiModelProperty(value="是否删除,默认为0未删除，-1表示删除状态")
	private Integer delState;


	/* 文章id */
	@ApiModelProperty(value="文章id")
	private Long articleId;


	/* 标签id */
	@ApiModelProperty(value="标签id")
	private Long tagsId;

}
