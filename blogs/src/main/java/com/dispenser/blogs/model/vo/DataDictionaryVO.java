package com.dispenser.blogs.model.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;

import java.util.Date;


/**
 * 数据字典表传输对象
 */
@Setter
@Getter
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@ApiModel("数据字典表数据传输对象")
public class DataDictionaryVO implements Serializable{
	private static final long serialVersionUID = 1L;

	/**  **/
	@ApiModelProperty(value="ID")
	private Long id;


	/* 父级 */
	@ApiModelProperty(value="父级")
	private Long parentId;


	/* 数据编码 */
	@ApiModelProperty(value="数据编码")
	private String numbers;


	/* 标签 */
	@ApiModelProperty(value="标签")
	private String label;


	/* 名称 */
	@ApiModelProperty(value="名称")
	private String name;


	/* 内容 */
	@ApiModelProperty(value="内容")
	private String value;


	/* 排序 */
	@ApiModelProperty(value="排序")
	private Integer sort;


	/* 创建日期 */
	@ApiModelProperty(value="创建日期")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date createdTime;


	/* 创建人id */
	@ApiModelProperty(value="创建人id")
	private Long createdId;


	/* 创建人名称 */
	@ApiModelProperty(value="创建人名称")
	private String createdName;


	/* 修改日期 */
	@ApiModelProperty(value="修改日期")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date updatedTime;


	/* 修改人id */
	@ApiModelProperty(value="修改人id")
	private Long updatedId;


	/* 修改人名称 */
	@ApiModelProperty(value="修改人名称")
	private String updatedName;


	/* 是否删除,默认为0未删除，1表示删除状态 */
	@ApiModelProperty(value="是否删除,默认为0未删除，1表示删除状态")
	private Integer delState;

}
