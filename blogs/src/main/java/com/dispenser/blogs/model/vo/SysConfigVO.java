package com.dispenser.blogs.model.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;

import java.util.Date;


/**
 * 系统配置表传输对象
 */
@Setter
@Getter
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@ApiModel("系统配置表数据传输对象")
public class SysConfigVO implements Serializable{
	private static final long serialVersionUID = 1L;

	/**  **/
	@ApiModelProperty(value="ID")
	private Long id;


	/* 添加时间，这里为长时间格式 */
	@ApiModelProperty(value="添加时间，这里为长时间格式")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date addTime;


	/* 是否删除,默认为0未删除，-1表示删除状态 */
	@ApiModelProperty(value="是否删除,默认为0未删除，-1表示删除状态")
	private Integer delState;


	/*  邮件是否开启 */
	@ApiModelProperty(value=" 邮件是否开启")
	private Boolean emailEnable;


	/*  stmp服务器 */
	@ApiModelProperty(value=" stmp服务器")
	private String emailHost;


	/*  stmp端口 */
	@ApiModelProperty(value=" stmp端口")
	private Integer emailPort;


	/*  邮箱密码 */
	@ApiModelProperty(value=" 邮箱密码")
	private String emailPwss;


	/*  邮箱用户名 */
	@ApiModelProperty(value=" 邮箱用户名")
	private String emailUser;


	/*  答题(赠送积分) */
	@ApiModelProperty(value=" 答题(赠送积分)")
	private Integer everyAnswerIntegrate;


	/*  允许图片上传的最大值 */
	@ApiModelProperty(value=" 允许图片上传的最大值")
	private Integer imageFileSize;


	/*  会员每日登陆(赠送积分) */
	@ApiModelProperty(value=" 会员每日登陆(赠送积分)")
	private Integer loginIntegrate;


	/*  用户上传文件路径 */
	@ApiModelProperty(value=" 用户上传文件路径")
	private String uploadFilePath;


	/*  试题文件路径 */
	@ApiModelProperty(value=" 试题文件路径")
	private String uploadTestBankPath;


	/*  微信App Id，申请开发者成功后微信生成的AppId */
	@ApiModelProperty(value=" 微信App Id，申请开发者成功后微信生成的AppId")
	private String weixinAppid;


	/*  微信AppSecret，申请开发者成功后微信生成的AppSecret */
	@ApiModelProperty(value=" 微信AppSecret，申请开发者成功后微信生成的AppSecret")
	private String weixinAppsecret;


	/*  网站ICO图片 */
	@ApiModelProperty(value=" 网站ICO图片")
	private Long websiteIcoId;


	/* 后台管理左上角Logo */
	@ApiModelProperty(value="后台管理左上角Logo")
	private Long adminLeftLogoId;


	/*  默认用户图标 */
	@ApiModelProperty(value=" 默认用户图标")
	private Long userIconId;


	/*  网站logo */
	@ApiModelProperty(value=" 网站logo")
	private Long websiteLogoId;


	/* 管理url */
	@ApiModelProperty(value="管理url")
	private String adminUrl;


	/* 首页url */
	@ApiModelProperty(value="首页url")
	private String indexUrl;


	/* 分库分表策略,也就是按照多大的数据量分一个库表，填写数字，最优是一百万：1000000，默认一百万分一个库表。 */
	@ApiModelProperty(value="分库分表策略,也就是按照多大的数据量分一个库表，填写数字，最优是一百万：1000000，默认一百万分一个库表。")
	private Long shardingStrategy;


	/* 静态资源版本 */
	@ApiModelProperty(value="静态资源版本")
	private String staticVersion;

	/* 图片服务器 Nginx 代理的地址*/
	@ApiModelProperty(value="图片服务器 Nginx 代理的地址")
	private String imageServerAddress;
}
