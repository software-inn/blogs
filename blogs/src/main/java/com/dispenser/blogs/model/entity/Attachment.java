package com.dispenser.blogs.model.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.math.BigDecimal;
import java.util.Date;


/**
 * 图片表实体
 */
@TableName(value="`b_attachment`",resultMap = "BaseResultMap")
@Setter
@Getter
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("图片表实体")
@JsonIgnoreProperties(value = { "handler"})
public class Attachment implements Serializable  {
	private static final long serialVersionUID = 1L;

	@TableId(type = IdType.AUTO)
	@TableField(value="`id`")
	@ApiModelProperty(value = "ID")
	private Long id;



	@TableField(value="`add_time`")
	@ApiModelProperty(value = "添加时间，这里为长时间格式")
	private Date addTime;



	@TableField(value="`del_state`")
	@ApiModelProperty(value = "是否删除,默认为0未删除，-1表示删除状态")
	private Integer delState;



	@TableField(value="`ext`")
	@ApiModelProperty(value = " 扩展名，不包括.")
	private String ext;



	@TableField(value="`height`")
	@ApiModelProperty(value = " 高度")
	private Integer height;



	@TableField(value="`info`")
	@ApiModelProperty(value = " 附件说明")
	private String info;



	@TableField(value="`name`")
	@ApiModelProperty(value = " 附件名称")
	private String name;



	@TableField(value="`path`")
	@ApiModelProperty(value = " 存放路径")
	private String path;



	@TableField(value="`size`")
	@ApiModelProperty(value = " 附件大小")
	private BigDecimal size;



	@TableField(value="`width`")
	@ApiModelProperty(value = " 宽度")
	private Integer width;

}
