package com.dispenser.blogs.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import java.util.List;
import com.dispenser.blogs.model.dto.CounterDTO;
import com.dispenser.blogs.model.entity.Counter;
import com.dispenser.blogs.model.vo.CounterVO;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * 
 *
 * @author l
 * @email 9543482@qq.com
 * @date 2020-12-09 00:20:43
 */
@Mapper
@Component
public interface CounterDao extends BaseMapper<Counter> {

    /**
     * 新增数据
     *
     * @param counter 实体对象
     * @return 新增结果
     */
    int saveEntity(Counter counter);

    /**
     * 修改数据
     *
     * @param counter 查询实体
     * @return 返回结果
     */
    int updateById(Counter counter);

    /**
     * 根据id查询数据
     *
     * @param id
     * @return
     */
        Counter queryById(@RequestParam("id") Long id);

    /**
     * 根据条件查询数据
     *
     * @param counterDTO
     * @return
     */
    List<Counter> queryAll(CounterDTO counterDTO);

    /**
     * 根据id删除数据信息
     *
     * @param id
     * @return
     */
    int deleteById(@RequestParam("id") Long id);

    /**
     * 批量删除（逻辑删除）
     *
     * @param idList
     */
    int deleteAll(List<Long> idList);

}
