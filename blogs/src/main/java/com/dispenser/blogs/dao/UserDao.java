package com.dispenser.blogs.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import java.util.List;
import com.dispenser.blogs.model.dto.UserDTO;
import com.dispenser.blogs.model.entity.User;
import com.dispenser.blogs.model.vo.UserVO;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * 用户表
 *
 * @author l
 * @email 9543482@qq.com
 * @date 2020-12-10 12:41:02
 */
@Mapper
@Component
public interface UserDao extends BaseMapper<User> {

    /**
     * 新增数据
     *
     * @param user 实体对象
     * @return 新增结果
     */
    int saveEntity(User user);

    /**
     * 修改数据
     *
     * @param user 查询实体
     * @return 返回结果
     */
    int updateById(User user);

    /**
     * 根据id查询数据
     *
     * @param id
     * @return
     */
        User queryById(@RequestParam("id") Long id);

    /**
     * 根据条件查询数据
     *
     * @param userDTO
     * @return
     */
    List<User> queryAll(UserDTO userDTO);

    /**
     * 根据id删除数据信息
     *
     * @param id
     * @return
     */
    int deleteById(@RequestParam("id") Long id);

    /**
     * 批量删除（逻辑删除）
     *
     * @param idList
     */
    int deleteAll(List<Long> idList);


    /**
     * 根据unionid获取用户
     * @param unionId
     * @return
     */
    User selectByUnionid(@RequestParam("unionId")String unionId);

    /**
     * 根据userName获取用户
     * @param userName
     * @return
     */
    User selectByUserName(@RequestParam("userName")String userName);
}
