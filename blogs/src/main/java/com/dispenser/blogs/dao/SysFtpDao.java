package com.dispenser.blogs.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import java.util.List;
import com.dispenser.blogs.model.dto.SysFtpDTO;
import com.dispenser.blogs.model.entity.SysFtp;
import com.dispenser.blogs.model.vo.SysFtpVO;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * 图片资源FTP服务器表
 *
 * @author l
 * @email 9543482@qq.com
 * @date 2020-12-10 12:41:04
 */
@Mapper
@Component
public interface SysFtpDao extends BaseMapper<SysFtp> {

    /**
     * 新增数据
     *
     * @param sysFtp 实体对象
     * @return 新增结果
     */
    int saveEntity(SysFtp sysFtp);

    /**
     * 修改数据
     *
     * @param sysFtp 查询实体
     * @return 返回结果
     */
    int updateById(SysFtp sysFtp);

    /**
     * 根据id查询数据
     *
     * @param id
     * @return
     */
        SysFtp queryById(@RequestParam("id") Long id);

    /**
     * 根据条件查询数据
     *
     * @param sysFtpDTO
     * @return
     */
    List<SysFtp> queryAll(SysFtpDTO sysFtpDTO);

    /**
     * 根据id删除数据信息
     *
     * @param id
     * @return
     */
    int deleteById(@RequestParam("id") Long id);

    /**
     * 批量删除（逻辑删除）
     *
     * @param idList
     */
    int deleteAll(List<Long> idList);

}
