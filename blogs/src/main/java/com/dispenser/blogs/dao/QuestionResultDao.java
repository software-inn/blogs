package com.dispenser.blogs.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import java.util.List;
import com.dispenser.blogs.model.dto.QuestionResultDTO;
import com.dispenser.blogs.model.entity.QuestionResult;
import com.dispenser.blogs.model.vo.QuestionResultVO;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * 问题答案-基本用于选择题
 *
 * @author l
 * @email 9543482@qq.com
 * @date 2020-12-13 20:36:31
 */
@Mapper
@Component
public interface QuestionResultDao extends BaseMapper<QuestionResult> {

    /**
     * 新增数据
     *
     * @param questionResult 实体对象
     * @return 新增结果
     */
    int saveEntity(QuestionResult questionResult);

    /**
     * 修改数据
     *
     * @param questionResult 查询实体
     * @return 返回结果
     */
    int updateById(QuestionResult questionResult);

    /**
     * 根据id查询数据
     *
     * @param id
     * @return
     */
        QuestionResult queryById(@RequestParam("id") Long id);

    /**
     * 根据条件查询数据
     *
     * @param questionResultDTO
     * @return
     */
    List<QuestionResult> queryAll(QuestionResultDTO questionResultDTO);

    /**
     * 根据id删除数据信息
     *
     * @param id
     * @return
     */
    int deleteById(@RequestParam("id") Long id);

    /**
     * 批量删除（逻辑删除）
     *
     * @param idList
     */
    int deleteAll(List<Long> idList);

}
