package com.dispenser.blogs.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import java.util.List;
import com.dispenser.blogs.model.dto.TagsDTO;
import com.dispenser.blogs.model.entity.Tags;
import com.dispenser.blogs.model.vo.TagsVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * 标签
 *
 * @author l
 * @email 9543482@qq.com
 * @date 2020-12-13 20:36:31
 */
@Mapper
@Component
public interface TagsDao extends BaseMapper<Tags> {

    /**
     * 新增数据
     *
     * @param tags 实体对象
     * @return 新增结果
     */
    int saveEntity(Tags tags);

    /**
     * 修改数据
     *
     * @param tags 查询实体
     * @return 返回结果
     */
    int updateById(Tags tags);

    /**
     * 根据id查询数据
     *
     * @param id
     * @return
     */
        Tags queryById(@RequestParam("id") Long id);

    /**
     * 根据条件查询数据
     *
     * @param tagsDTO
     * @return
     */
    List<Tags> queryAll(TagsDTO tagsDTO);

    /**
     * 根据id删除数据信息
     *
     * @param id
     * @return
     */
    int deleteById(@RequestParam("id") Long id);

    /**
     * 批量删除（逻辑删除）
     *
     * @param idList
     */
    int deleteAll(List<Long> idList);


    /**
     * 获取文章的所有标签
     * @param id
     * @return
     */
    List<Tags> queryByArticleId(@Param("id") Long id);
}
