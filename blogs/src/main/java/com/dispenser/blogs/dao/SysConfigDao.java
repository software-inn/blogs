package com.dispenser.blogs.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import java.util.List;
import com.dispenser.blogs.model.dto.SysConfigDTO;
import com.dispenser.blogs.model.entity.SysConfig;
import com.dispenser.blogs.model.vo.SysConfigVO;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * 系统配置表
 *
 * @author l
 * @email 9543482@qq.com
 * @date 2020-12-10 12:41:04
 */
@Mapper
@Component
public interface SysConfigDao extends BaseMapper<SysConfig> {

    /**
     * 新增数据
     *
     * @param sysConfig 实体对象
     * @return 新增结果
     */
    int saveEntity(SysConfig sysConfig);

    /**
     * 修改数据
     *
     * @param sysConfig 查询实体
     * @return 返回结果
     */
    int updateById(SysConfig sysConfig);

    /**
     * 根据id查询数据
     *
     * @param id
     * @return
     */
        SysConfig queryById(@RequestParam("id") Long id);

    /**
     * 根据条件查询数据
     *
     * @param sysConfigDTO
     * @return
     */
    List<SysConfig> queryAll(SysConfigDTO sysConfigDTO);

    /**
     * 根据id删除数据信息
     *
     * @param id
     * @return
     */
    int deleteById(@RequestParam("id") Long id);

    /**
     * 批量删除（逻辑删除）
     *
     * @param idList
     */
    int deleteAll(List<Long> idList);

}
