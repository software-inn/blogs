package com.dispenser.blogs.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import java.util.List;
import com.dispenser.blogs.model.dto.ArticleTagsDTO;
import com.dispenser.blogs.model.entity.ArticleTags;
import com.dispenser.blogs.model.vo.ArticleTagsVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * 文章与标签中间表
 *
 * @author l
 * @email 9543482@qq.com
 * @date 2020-12-13 20:36:31
 */
@Mapper
@Component
public interface ArticleTagsDao extends BaseMapper<ArticleTags> {

    /**
     * 新增数据
     *
     * @param articleTags 实体对象
     * @return 新增结果
     */
    int saveEntity(ArticleTags articleTags);

    /**
     * 修改数据
     *
     * @param articleTags 查询实体
     * @return 返回结果
     */
    int updateById(ArticleTags articleTags);

    /**
     * 根据id查询数据
     *
     * @param id
     * @return
     */
        ArticleTags queryById(@RequestParam("id") Long id);

    /**
     * 根据条件查询数据
     *
     * @param articleTagsDTO
     * @return
     */
    List<ArticleTags> queryAll(ArticleTagsDTO articleTagsDTO);

    /**
     * 根据id删除数据信息
     *
     * @param id
     * @return
     */
    int deleteById(@RequestParam("id") Long id);

    /**
     * 批量删除（逻辑删除）
     *
     * @param idList
     */
    int deleteAll(List<Long> idList);

    /**
     * 批量插入
     * @param list
     * @return
     */
    int insertBatch(List<ArticleTags> list);


    /**
     * 删除文章标签
     * @param id
     * @return
     */
    int deleteByArticleId(@Param("id")Long id);

}
