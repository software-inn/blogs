package com.dispenser.blogs.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import java.util.List;
import com.dispenser.blogs.model.dto.ArticleClassDTO;
import com.dispenser.blogs.model.entity.ArticleClass;
import com.dispenser.blogs.model.vo.ArticleClassVO;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * 分类
 *
 * @author l
 * @email 9543482@qq.com
 * @date 2020-12-13 20:49:03
 */
@Mapper
@Component
public interface ArticleClassDao extends BaseMapper<ArticleClass> {

    /**
     * 新增数据
     *
     * @param articleClass 实体对象
     * @return 新增结果
     */
    int saveEntity(ArticleClass articleClass);

    /**
     * 修改数据
     *
     * @param articleClass 查询实体
     * @return 返回结果
     */
    int updateById(ArticleClass articleClass);

    /**
     * 根据id查询数据
     *
     * @param id
     * @return
     */
        ArticleClass queryById(@RequestParam("id") Long id);

    /**
     * 根据条件查询数据
     *
     * @param articleClassDTO
     * @return
     */
    List<ArticleClass> queryAll(ArticleClassDTO articleClassDTO);

    /**
     * 根据id删除数据信息
     *
     * @param id
     * @return
     */
    int deleteById(@RequestParam("id") Long id);

    /**
     * 批量删除（逻辑删除）
     *
     * @param idList
     */
    int deleteAll(List<Long> idList);

}
