package com.dispenser.blogs.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import java.util.List;
import com.dispenser.blogs.model.dto.SysMenuDTO;
import com.dispenser.blogs.model.entity.SysMenu;
import com.dispenser.blogs.model.vo.SysMenuVO;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * 系统菜单表
 *
 * @author l
 * @email 9543482@qq.com
 * @date 2020-12-10 12:41:04
 */
@Mapper
@Component
public interface SysMenuDao extends BaseMapper<SysMenu> {

    /**
     * 新增数据
     *
     * @param sysMenu 实体对象
     * @return 新增结果
     */
    int saveEntity(SysMenu sysMenu);

    /**
     * 修改数据
     *
     * @param sysMenu 查询实体
     * @return 返回结果
     */
    int updateById(SysMenu sysMenu);

    /**
     * 根据id查询数据
     *
     * @param id
     * @return
     */
        SysMenu queryById(@RequestParam("id") Long id);

    /**
     * 根据条件查询数据
     *
     * @param sysMenuDTO
     * @return
     */
    List<SysMenu> queryAll(SysMenuDTO sysMenuDTO);

    /**
     * 根据id删除数据信息
     *
     * @param id
     * @return
     */
    int deleteById(@RequestParam("id") Long id);

    /**
     * 批量删除（逻辑删除）
     *
     * @param idList
     */
    int deleteAll(List<Long> idList);

}
