package com.dispenser.blogs.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import java.util.List;
import com.dispenser.blogs.model.dto.QuestionTagsDTO;
import com.dispenser.blogs.model.entity.QuestionTags;
import com.dispenser.blogs.model.vo.QuestionTagsVO;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * 问题与标签中间表
 *
 * @author l
 * @email 9543482@qq.com
 * @date 2020-12-13 20:36:31
 */
@Mapper
@Component
public interface QuestionTagsDao extends BaseMapper<QuestionTags> {

    /**
     * 新增数据
     *
     * @param questionTags 实体对象
     * @return 新增结果
     */
    int saveEntity(QuestionTags questionTags);

    /**
     * 修改数据
     *
     * @param questionTags 查询实体
     * @return 返回结果
     */
    int updateById(QuestionTags questionTags);

    /**
     * 根据id查询数据
     *
     * @param id
     * @return
     */
        QuestionTags queryById(@RequestParam("id") Long id);

    /**
     * 根据条件查询数据
     *
     * @param questionTagsDTO
     * @return
     */
    List<QuestionTags> queryAll(QuestionTagsDTO questionTagsDTO);

    /**
     * 根据id删除数据信息
     *
     * @param id
     * @return
     */
    int deleteById(@RequestParam("id") Long id);

    /**
     * 批量删除（逻辑删除）
     *
     * @param idList
     */
    int deleteAll(List<Long> idList);

}
