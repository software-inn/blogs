package com.dispenser.blogs.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import java.util.List;
import com.dispenser.blogs.model.dto.DataDictionaryDTO;
import com.dispenser.blogs.model.entity.DataDictionary;
import com.dispenser.blogs.model.vo.DataDictionaryVO;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * 数据字典表
 *
 * @author l
 * @email 9543482@qq.com
 * @date 2020-12-13 20:36:30
 */
@Mapper
@Component
public interface DataDictionaryDao extends BaseMapper<DataDictionary> {

    /**
     * 新增数据
     *
     * @param dataDictionary 实体对象
     * @return 新增结果
     */
    int saveEntity(DataDictionary dataDictionary);

    /**
     * 修改数据
     *
     * @param dataDictionary 查询实体
     * @return 返回结果
     */
    int updateById(DataDictionary dataDictionary);

    /**
     * 根据id查询数据
     *
     * @param id
     * @return
     */
        DataDictionary queryById(@RequestParam("id") Long id);

    /**
     * 根据条件查询数据
     *
     * @param dataDictionaryDTO
     * @return
     */
    List<DataDictionary> queryAll(DataDictionaryDTO dataDictionaryDTO);

    /**
     * 根据id删除数据信息
     *
     * @param id
     * @return
     */
    int deleteById(@RequestParam("id") Long id);

    /**
     * 批量删除（逻辑删除）
     *
     * @param idList
     */
    int deleteAll(List<Long> idList);

}
