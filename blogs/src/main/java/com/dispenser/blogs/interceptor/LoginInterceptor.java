package com.dispenser.blogs.interceptor;

import com.alibaba.fastjson.JSONObject;
import com.dispenser.blogs.common.ApiResultUtils;
import com.dispenser.blogs.common.JWT;
import com.dispenser.blogs.common.RedisTemplates;
import com.dispenser.blogs.common.SpringContextHolder;
import com.dispenser.blogs.model.enums.UserEnum;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.http.HttpMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;

@Slf4j
public class LoginInterceptor implements HandlerInterceptor {

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {

		if(HttpMethod.OPTIONS.toString().equals(request.getMethod())) {
			log.info("OPTIONS请求，放行");
			return true;
		}
		response.setCharacterEncoding("utf-8");
		// 从header中获取token
		String tokenStr = request.getHeader("B-Token");
		// TODO token为空直接通过，后期需要确定那些服务登录后才能访问， 在这里拦截
		if(StringUtils.isEmpty(tokenStr)) {
			PrintWriter writer = response.getWriter();
			responseMessage(response, writer, ApiResultUtils.failure(50012,"token不能为空"));
        	return false;
		}
		
		// 解密token
		String token = JWT.unsign(tokenStr, String.class);
		
		// 验证token的合法性
		if(StringUtils.isEmpty(token) || token.indexOf(";") == -1){
			PrintWriter writer = response.getWriter();
			responseMessage(response, writer, ApiResultUtils.failure(50008,"非法token"));
        	return false;
		}
		
		// 从token获取用户id
		String [] tokens = token.split(";");
		Long userId = null2Long(tokens[0]);
		if(null == userId){
			PrintWriter writer = response.getWriter();
        	responseMessage(response, writer, ApiResultUtils.failure(50008,"非法token"));
        	return false;
		}

		RedisTemplates redisTemplates = SpringContextHolder.getBean(RedisTemplates.class);
		// 从redis中取出用户token
		String redisToken = String.valueOf(redisTemplates.get(UserEnum.USER_TOKEN.getValue()+ userId));
		
		// redis中token为空  || redis中的token和header中的token不相等，提示：token已过期，请重新登录
		if(StringUtils.isEmpty(redisToken) || !StringUtils.equals(tokenStr, redisToken)){
			PrintWriter writer = response.getWriter();
        	responseMessage(response, writer, ApiResultUtils.failure(50014,"token已过期，请重新登录"));
        	return false;
		}
		
		// 将用户id绑定到request中
		request.setAttribute("user_id", userId);
		
		return true;
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
	}

	private void responseMessage(HttpServletResponse response, PrintWriter out, Object responseVO) {
		response.setContentType("application/json; charset=utf-8");  
        response.addHeader("Access-Control-Allow-Origin", "*");
        response.addHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
        response.addHeader("Access-Control-Allow-Headers",
                       "Content-Type,X-Requested-With,accept,Origin,Access-Control-Request-Method,Access-Control-Request-Headers,token");
        out.print(JSONObject.toJSON(responseVO).toString());
        out.flush();
        out.close();
    }
	
	public Long null2Long(String s) {
		Long v = null;
		if (s != null) {
			try {
				v = Long.parseLong(s);
			} catch (Exception localException) {
			}
		}
		return v;
	}

}
