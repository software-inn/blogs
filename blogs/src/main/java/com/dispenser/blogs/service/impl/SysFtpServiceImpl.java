package com.dispenser.blogs.service.impl;

import com.alibaba.druid.util.Base64;
import com.alibaba.druid.util.StringUtils;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dispenser.blogs.common.ApiResult;
import com.dispenser.blogs.common.BeanUtil;
import com.dispenser.blogs.common.SFtpUtil;
import com.dispenser.blogs.controller.CounterController;
import com.dispenser.blogs.dao.AttachmentDao;
import com.dispenser.blogs.dao.SysConfigDao;
import com.dispenser.blogs.dao.SysFtpDao;
import com.dispenser.blogs.model.dto.SysFtpDTO;
import com.dispenser.blogs.model.entity.Attachment;
import com.dispenser.blogs.model.entity.SysConfig;
import com.dispenser.blogs.model.entity.SysFtp;
import com.dispenser.blogs.model.qo.SysFtpQO;
import com.dispenser.blogs.model.vo.SysFtpVO;
import com.dispenser.blogs.service.SysFtpService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import javax.net.ssl.HttpsURLConnection;
import java.awt.image.BufferedImage;
import java.io.*;
import java.math.BigDecimal;
import java.net.URL;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static com.dispenser.blogs.common.ApiResultUtils.failureWithMsg;
import static com.dispenser.blogs.common.ApiResultUtils.success;

/**
 * 单据编码规则service实现类
 */
@Slf4j
@Service("sysFtpService")
@Transactional
public class SysFtpServiceImpl extends ServiceImpl<SysFtpDao, SysFtp> implements SysFtpService {

    private final Logger logger = LoggerFactory.getLogger(CounterController.class);

    private static final String MENU_NAME = "图片资源FTP服务器表";

	@Autowired
	private AttachmentDao attachmentDao;
	@Autowired
	private SysFtpDao sysFtpDao;
	@Autowired
	private SysConfigDao sysConfigDao;
	@Value("${file-upload-path}")
	private String fileUploadPath;

	/**
	 * 分页获取图片资源FTP服务器表列表
	 */
	@Override
	public ApiResult<PageInfo<SysFtpVO>> list(SysFtpQO qo) {

        logger.info("分页获取请求参数--SysFtpQO：" + qo);

		PageHelper.startPage(qo.getPage(), qo.getLimit());

		SysFtpDTO dto=BeanUtil.copyProperties(qo, SysFtpDTO.class);
		List<SysFtp> list = sysFtpDao.queryAll(dto);

		PageInfo<SysFtpVO> pageInfo = BeanUtil.transferPageInfo(list, SysFtpVO.class);
        logger.info("返回结果：" + pageInfo);
		return success(pageInfo);
	}

	/**
	 * 根据id查询图片资源FTP服务器表
	 */
	@Override
	public ApiResult<SysFtpVO> get(Long id) {
        logger.info("根据id查询请求参数--id：" + id);

		SysFtp entity = sysFtpDao.queryById(id);
		if(entity==null){
            logger.info("当前图片资源FTP服务器表不存在");
			return failureWithMsg("当前图片资源FTP服务器表不存在");
		}
		SysFtpVO vo = BeanUtil.copyProperties(entity, SysFtpVO.class);

        logger.info("返回结果：" + vo);
		return success(vo);
	}

	/**
	 * 新增图片资源FTP服务器表
	 */
	@Override
	public ApiResult save(SysFtpDTO dto) {
        logger.info("新增请求参数--SysFtpDTO：" + dto);

		SysFtp entity = BeanUtil.copyProperties(dto, SysFtp.class);
		if(entity.getId() == null){
			if (sysFtpDao.saveEntity(entity)>0) {
				logger.info("返回结果：" + entity);
				return success(entity);
			}
		}else{
			if (sysFtpDao.updateById(entity)>0) {
				logger.info("返回结果：" + entity);
				return success(entity);
			}
		}

        logger.info("保存失败");
		return failureWithMsg("保存失败");
	}

	/**
	 * 修改图片资源FTP服务器表
	 */
	@Override
	public ApiResult update(SysFtpDTO dto) {
        logger.info("修改请求参数--SysFtpDTO：" + dto);

		SysFtp oldEntity = sysFtpDao.queryById(dto.getId());
		if(oldEntity==null){
            logger.info("当前图片资源FTP服务器表不存在");
			return failureWithMsg("当前图片资源FTP服务器表不存在");
		}

		SysFtp entity = BeanUtil.copyProperties(dto, SysFtp.class);

		if (sysFtpDao.updateById(entity)>0) {

            logger.info("返回结果："+entity);
			return success(entity);
		}
        logger.info("保存失败");
		return failureWithMsg("修改失败");
	}

	/**
	 * 逻辑删除图片资源FTP服务器表
	 */
	@Override
	public ApiResult delete(Long id) {
        logger.info("逻辑删除请求参数--id：" + id);

		SysFtp oldEntity = sysFtpDao.queryById(id);
		if(oldEntity==null){
            logger.info("当前图片资源FTP服务器表不存在");
			return failureWithMsg("当前图片资源FTP服务器表不存在");
		}

		if (sysFtpDao.deleteById(id)>0) {
            logger.info("删除成功");
			return success("删除成功");
		}

        logger.info("删除失败");
		return failureWithMsg("删除失败");
	}

	/**
	 * 批量删除（逻辑删除）
	 *
	 * @param idList
 	*/
	@Override
	public ApiResult deleteAll(List<Long> idList) {
        logger.info("批量删除（逻辑删除）请求参数--idList：" + idList);
		if (idList != null) {
			int isOk = 0;
			isOk = sysFtpDao.deleteAll(idList);   //删除数据
			if (isOk > 0) {
                logger.info("删除成功");
				return success("删除成功");
			}
		}

        logger.info("删除失败");
		return failureWithMsg("删除失败");
	}

	/**
	 * Base64 上传文件
	 * @param params
	 * @return
	 */
	public Boolean uploadByBase64(Map<String, Object> params) {
		String base64 = (String) params.get("base64");
		String folder = (String) params.get("folder");
		String fileName = (String) params.get("fileName");

		byte[] bytes1 = Base64.base64ToByteArray(base64);
		ByteArrayInputStream stream = new ByteArrayInputStream(bytes1);

		// FTP服务器配置
		SysFtp sysFtp = sysFtpDao.queryById(1L);
		if (sysFtp == null) {
			return false;
		}

		// 上传文件到SFTP
		Boolean flag = SFtpUtil.connect(sysFtp.getFtpIp(), sysFtp.getFtpPort(), sysFtp.getFtpUsername(),
				sysFtp.getFtpPassword());
		if (!flag) {
			return false;
		}
		flag = SFtpUtil.upload(stream, folder, fileName);
		if (!flag) {
			return false;
		}
		return flag;
	}





	/**
	 * sft网络文件上传
	 *
	 * @param folder
	 *            文件夹 如：account账户、activity:活动，多层目录用/隔开，如：user/image用户头像
	 * @param file_url
	 *            网络流地址
	 * @param oldFileId
	 *            原文件id
	 * @param extend
	 *            文件后缀
	 * @return
	 */
	public Attachment upload(String folder, Long oldFileId, String extend,String file_url) {
		if (StringUtils.isEmpty(folder)) {
			log.info("sft网络文件上传,路径为空！");
			return null;
		}

		// 处理文件夹和文件名称，所有的文件都存储在upload目录下
		Attachment attachment = null;
		String fileName = "";
		if (StringUtils.isEmpty(folder)) {
			folder = "attachments";
		}
		if (oldFileId != null) {
			attachment = attachmentDao.queryById(oldFileId);
			folder = (attachment != null) ? attachment.getPath() : folder;
			fileName = (attachment != null) ? attachment.getName() : "";
		}
		if (StringUtils.isEmpty(fileName)) { // 文件名字
			fileName = UUID.randomUUID().toString() + "." + extend;
		}

		URL url = null;
		HttpsURLConnection conn = null;
		InputStream in = null;
		OutputStream os = null;
		String filePath = ""; // 上传文件本地目录全路径+文件名
		try {
			url = new URL(file_url);
			conn = (HttpsURLConnection) url.openConnection();
			in = conn.getInputStream();

			// FTP文件上传路径
			String uploadFilePath = sysConfigDao.queryById(1L).getUploadFilePath();
			folder = uploadFilePath + "/" + folder;

			// 本地昨时文件夹上传目录
			String path = fileUploadPath + folder;
			File fileDir = new File(path);
			if (!fileDir.exists()) {
				fileDir.mkdirs();
			}
			// 设置数据缓冲
			byte[] bs = new byte[1024 * 2];
			// 读取到的数据长度
			int len = 0;
			// 输出的文件流保存图片至本地
			filePath = fileDir + "/" + fileName;
			os = new FileOutputStream(filePath);
			while ((len = in.read(bs)) != -1) {
				os.write(bs, 0, len);
			}
			os.flush();
			in = new FileInputStream(new File(filePath));
		} catch (Exception e) {
			log.info("sft网络文件上传,存储本地文件失败");
			return null;
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (IOException e) {
				}
			}
			if (conn != null) {
				conn.disconnect();
			}
			if (os != null) {
				try {
					os.close();
				} catch (IOException e) {
				}
			}
		}

		InputStream inStream = null;
		try {
			inStream = new FileInputStream(new File(filePath));
			Integer size = inStream.available();
			/*
			 * if (size > 10 * 1024 * 1024) { // 上传文件不能超过10MB
			 * log.info("sft网络文件上传,大于10MB"); return null; }
			 */

			// 图片长和宽,此代码要放sftp之前，ftp上传完成后会关闭流
			int width = 0, height = 0;
			try {
				BufferedImage bis = ImageIO.read(inStream);
				width = bis.getWidth();
				height = bis.getHeight();
				bis.flush();
			} catch (Exception e) {
			}

			// FTP服务器配置
			SysFtp sysFtp = sysFtpDao.queryById(1L);
			if (sysFtp == null ) {
				log.info("sft网络文件上传,FTP服务器未配置");
				return null;
			}
			// 上传文件到SFTP
			Boolean flag = SFtpUtil.connect(sysFtp.getFtpIp(), sysFtp.getFtpPort(), sysFtp.getFtpUsername(),
					sysFtp.getFtpPassword());
			if (!flag) {
				log.info("sft网络文件上传,FTP连接失败！");
				return null;
			}
			flag = SFtpUtil.upload(folder, filePath);
			if (!flag) {
				log.info("sft网络文件上传,FTP上传失败！");
				return null;
			}
			// 记录到数据库
			if (attachment == null) {
				attachment = new Attachment();
			}
			attachment.setName(fileName);
			attachment.setExt(extend);
			attachment.setSize(new BigDecimal(size));
			attachment.setPath(folder);
			attachment.setWidth(Integer.valueOf(width));
			attachment.setHeight(Integer.valueOf(height));
			if (attachment.getId() != null) {
				attachmentDao.updateById(attachment);
			} else {
				attachment.setAddTime(new Date());
				attachment.setDelState(0);
				attachmentDao.saveEntity(attachment);
			}
			return attachment;
		} catch (Exception e) {
			log.info("sft网络文件上传异常！");
			return null;
		} finally {
			if (inStream != null) {
				try {
					inStream.close();
				} catch (IOException e) {
				}
			}
			if (StringUtils.isEmpty(filePath)) {
				File file = new File(filePath);
				if (file.exists()) {
					file.delete();
				}
			}
		}
	}

	/**
	 * sft文件上传
	 *
	 * @param file
	 *
	 * @param oldFileId
	 *            原文件id
	 * @return
	 */
	@Override
	public ApiResult uploadFile(MultipartFile file,String oldFileId,String folder) {

		if (file == null) {
			return failureWithMsg("上传文件为空");
		}
		SysConfig sysConfig = sysConfigDao.queryById(1L);
		if (file.getSize() > sysConfig.getImageFileSize()) { // 上传文件不能超过系统设置
			return failureWithMsg("上传文件不能超过" + sysConfig.getImageFileSize());
		}
		if(folder == null){
			folder = "attachments";// 文件路径
		}
		String uploadFilePath = sysConfigDao.queryById(1L).getUploadFilePath();

		// 处理文件夹和文件名称，所有的文件都存储在upload目录下
		Attachment attachment = null;
		String fileName = "";
		folder = uploadFilePath + "/" + folder;
		if (!StringUtils.isEmpty(oldFileId)) {
			attachment = attachmentDao.queryById(Long.valueOf(oldFileId));
			folder = (attachment != null) ? attachment.getPath() : folder;
			fileName = (attachment != null) ? attachment.getName() : "";
		}

		try {
			// 文件后缀
			String extend = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf(".") + 1)
					.toLowerCase();
			if (StringUtils.isEmpty(fileName)) { // 文件名字
				fileName = UUID.randomUUID().toString() + "." + extend;
			}

			// 图片长和宽,此代码要放sftp之前，ftp上传完成后会关闭流
			int width = 0, height = 0;
			try {
				BufferedImage bis = ImageIO.read(file.getInputStream());
				width = bis.getWidth();
				height = bis.getHeight();
				bis.flush();
			} catch (Exception e) {
			}

			// FTP服务器配置
			SysFtp sysFtp = sysFtpDao.queryById(1L);
			if (sysFtp == null) {
				return failureWithMsg("系统未配置FTP服务器");
			}
			// 上传文件到SFTP
			Boolean flag = SFtpUtil.connect(sysFtp.getFtpIp(), sysFtp.getFtpPort(), sysFtp.getFtpUsername(),
					sysFtp.getFtpPassword());
			if (!flag) {
				return failureWithMsg("FTP服务器链接失败");
			}
			flag = SFtpUtil.upload(file.getInputStream(), folder, fileName);
			if (!flag) {
				return failureWithMsg("文件上传失败");
			}

			// 记录到数据库
			if (attachment == null) {
				attachment = new Attachment();
			}
			attachment.setName(fileName);
			attachment.setExt(extend);
			attachment.setSize(BigDecimal.valueOf(file.getSize()));
			attachment.setPath(folder);
			attachment.setWidth(Integer.valueOf(width));
			attachment.setHeight(Integer.valueOf(height));
			if (attachment.getId() != null) {
				attachmentDao.updateById(attachment);
			} else {
				attachment.setAddTime(new Date());
				attachment.setDelState(0);
				attachmentDao.saveEntity(attachment);
			}
			return success(attachment);

		} catch (Exception e) {
			return failureWithMsg("文件上传失败:" + e.getMessage());
		}
	}


}
