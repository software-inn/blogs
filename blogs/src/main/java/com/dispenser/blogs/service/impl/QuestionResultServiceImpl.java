package com.dispenser.blogs.service.impl;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dispenser.blogs.dao.QuestionResultDao;
import com.dispenser.blogs.model.dto.QuestionResultDTO;
import com.dispenser.blogs.model.entity.QuestionResult;
import com.dispenser.blogs.model.qo.QuestionResultQO;
import com.dispenser.blogs.model.vo.QuestionResultVO;
import com.dispenser.blogs.service.QuestionResultService;
import com.dispenser.blogs.common.ApiResult;
import com.dispenser.blogs.common.BeanUtil;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static com.dispenser.blogs.common.ApiResultUtils.failureWithMsg;
import static com.dispenser.blogs.common.ApiResultUtils.success;

/**
 * 单据编码规则service实现类
 */
@Slf4j
@Service("questionResultService")
@Transactional
public class QuestionResultServiceImpl extends ServiceImpl<QuestionResultDao, QuestionResult> implements QuestionResultService {

    private final Logger logger = LoggerFactory.getLogger(QuestionResultServiceImpl.class);

    private static final String MENU_NAME = "问题答案-基本用于选择题";

	@Autowired
	QuestionResultDao questionResultDao;

	/**
	 * 分页获取问题答案-基本用于选择题列表
	 */
	@Override
	public ApiResult<PageInfo<QuestionResultVO>> list(QuestionResultQO qo) {

        logger.info("分页获取请求参数--QuestionResultQO：" + qo);

		PageHelper.startPage(qo.getPage(), qo.getLimit());

		QuestionResultDTO dto=BeanUtil.copyProperties(qo, QuestionResultDTO.class);
		List<QuestionResult> list = questionResultDao.queryAll(dto);

		PageInfo<QuestionResultVO> pageInfo = BeanUtil.transferPageInfo(list, QuestionResultVO.class);
        logger.info("返回结果：" + pageInfo);
		return success(pageInfo);
	}

	/**
	 * 根据id查询问题答案-基本用于选择题
	 */
	@Override
	public ApiResult<QuestionResultVO> get(Long id) {
        logger.info("根据id查询请求参数--id：" + id);

		QuestionResult entity = questionResultDao.queryById(id);
		if(entity==null){
            logger.info("当前问题答案-基本用于选择题不存在");
			return failureWithMsg("当前问题答案-基本用于选择题不存在");
		}
		QuestionResultVO vo = BeanUtil.copyProperties(entity, QuestionResultVO.class);

        logger.info("返回结果：" + vo);
		return success(vo);
	}

	/**
	 * 新增问题答案-基本用于选择题
	 */
	@Override
	public ApiResult save(QuestionResultDTO dto) {
        logger.info("新增请求参数--QuestionResultDTO：" + dto);

		QuestionResult entity = BeanUtil.copyProperties(dto, QuestionResult.class);

		if (questionResultDao.saveEntity(entity)>0) {
            logger.info("返回结果：" + entity);
			return success(entity);
		}
        logger.info("保存失败");
		return failureWithMsg("保存失败");
	}

	/**
	 * 修改问题答案-基本用于选择题
	 */
	@Override
	public ApiResult update(QuestionResultDTO dto) {
        logger.info("修改请求参数--QuestionResultDTO：" + dto);

		QuestionResult oldEntity = questionResultDao.queryById(dto.getId());
		if(oldEntity==null){
            logger.info("当前问题答案-基本用于选择题不存在");
			return failureWithMsg("当前问题答案-基本用于选择题不存在");
		}

		QuestionResult entity = BeanUtil.copyProperties(dto, QuestionResult.class);

		if (questionResultDao.updateById(entity)>0) {

            logger.info("返回结果："+entity);
			return success(entity);
		}
        logger.info("保存失败");
		return failureWithMsg("修改失败");
	}

	/**
	 * 逻辑删除问题答案-基本用于选择题
	 */
	@Override
	public ApiResult delete(Long id) {
        logger.info("逻辑删除请求参数--id：" + id);

		QuestionResult oldEntity = questionResultDao.queryById(id);
		if(oldEntity==null){
            logger.info("当前问题答案-基本用于选择题不存在");
			return failureWithMsg("当前问题答案-基本用于选择题不存在");
		}

		if (questionResultDao.deleteById(id)>0) {
            logger.info("删除成功");
			return success("删除成功");
		}

        logger.info("删除失败");
		return failureWithMsg("删除失败");
	}

	/**
	 * 批量删除（逻辑删除）
	 *
	 * @param idList
 	*/
	@Override
	public ApiResult deleteAll(List<Long> idList) {
        logger.info("批量删除（逻辑删除）请求参数--idList：" + idList);
		if (idList != null) {
			int isOk = 0;
			isOk = questionResultDao.deleteAll(idList);   //删除数据
			if (isOk > 0) {
                logger.info("删除成功");
				return success("删除成功");
			}
		}

        logger.info("删除失败");
		return failureWithMsg("删除失败");
	}

}
