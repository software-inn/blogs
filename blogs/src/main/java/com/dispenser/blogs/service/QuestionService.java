package com.dispenser.blogs.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.dispenser.blogs.model.dto.QuestionDTO;
import com.dispenser.blogs.model.entity.Question;
import com.dispenser.blogs.model.qo.QuestionQO;
import com.dispenser.blogs.model.vo.QuestionVO;
import com.dispenser.blogs.common.ApiResult;
import com.github.pagehelper.PageInfo;
import java.util.List;
/**
 * 问题service
 */
public interface QuestionService extends IService<Question> {

	/**
     * 分页获取问题列表
     */
	ApiResult<PageInfo<QuestionVO>> list(QuestionQO qo);

	/**
     * 根据id查询问题
     */
	ApiResult<QuestionVO> get(Long id);

	/**
     * 新增问题
     */
	ApiResult save(QuestionDTO dto);

	/**
     * 修改问题
     */
	ApiResult update(QuestionDTO dto);

	/**
     * 逻辑删除问题
     */
	ApiResult delete(Long id);

	/**
	* 批量删除（逻辑删除）
	*
	* @param idList
	*/
	ApiResult deleteAll(List<Long> idList);
}

