package com.dispenser.blogs.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.dispenser.blogs.model.dto.QuestionResultDTO;
import com.dispenser.blogs.model.entity.QuestionResult;
import com.dispenser.blogs.model.qo.QuestionResultQO;
import com.dispenser.blogs.model.vo.QuestionResultVO;
import com.dispenser.blogs.common.ApiResult;
import com.github.pagehelper.PageInfo;
import java.util.List;
/**
 * 问题答案-基本用于选择题service
 */
public interface QuestionResultService extends IService<QuestionResult> {

	/**
     * 分页获取问题答案-基本用于选择题列表
     */
	ApiResult<PageInfo<QuestionResultVO>> list(QuestionResultQO qo);

	/**
     * 根据id查询问题答案-基本用于选择题
     */
	ApiResult<QuestionResultVO> get(Long id);

	/**
     * 新增问题答案-基本用于选择题
     */
	ApiResult save(QuestionResultDTO dto);

	/**
     * 修改问题答案-基本用于选择题
     */
	ApiResult update(QuestionResultDTO dto);

	/**
     * 逻辑删除问题答案-基本用于选择题
     */
	ApiResult delete(Long id);

	/**
	* 批量删除（逻辑删除）
	*
	* @param idList
	*/
	ApiResult deleteAll(List<Long> idList);
}

