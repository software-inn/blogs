package com.dispenser.blogs.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.dispenser.blogs.common.ApiResult;
import com.dispenser.blogs.model.dto.CounterDTO;
import com.dispenser.blogs.model.entity.Counter;
import com.dispenser.blogs.model.qo.CounterQO;
import com.dispenser.blogs.model.vo.CounterVO;
import com.github.pagehelper.PageInfo;

import java.util.List;
/**
 * service
 */
public interface CounterService extends IService<Counter> {

	/**
     * 分页获取列表
     */
	ApiResult<PageInfo<CounterVO>> list(CounterQO qo);

	/**
     * 根据id查询
     */
	ApiResult<CounterVO> get(Long id);

	/**
     * 新增
     */
	ApiResult save(CounterDTO dto);

	/**
     * 修改
     */
	ApiResult update(CounterDTO dto);

	/**
     * 逻辑删除
     */
	ApiResult delete(Long id);

	/**
	* 批量删除（逻辑删除）
	*
	* @param idList
	*/
	ApiResult deleteAll(List<Long> idList);
}

