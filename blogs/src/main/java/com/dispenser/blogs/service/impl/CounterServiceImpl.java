package com.dispenser.blogs.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dispenser.blogs.common.ApiResult;
import com.dispenser.blogs.common.BeanUtil;
import com.dispenser.blogs.controller.CounterController;
import com.dispenser.blogs.dao.CounterDao;
import com.dispenser.blogs.model.dto.CounterDTO;
import com.dispenser.blogs.model.entity.Counter;
import com.dispenser.blogs.model.qo.CounterQO;
import com.dispenser.blogs.model.vo.CounterVO;
import com.dispenser.blogs.service.CounterService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static com.dispenser.blogs.common.ApiResultUtils.failureWithMsg;
import static com.dispenser.blogs.common.ApiResultUtils.success;

/**
 * 单据编码规则service实现类
 */
@Slf4j
@Service("counterService")
@Transactional
public class CounterServiceImpl extends ServiceImpl<CounterDao, Counter> implements CounterService {

    private final Logger logger = LoggerFactory.getLogger(CounterController.class);

    private static final String MENU_NAME = "";

	@Autowired
	CounterDao counterDao;

	/**
	 * 分页获取列表
	 */
	@Override
	public ApiResult<PageInfo<CounterVO>> list(CounterQO qo) {

       logger.info("请求参数--CounterQO：" + qo);

		PageHelper.startPage(qo.getPage(), qo.getLimit());

		CounterDTO dto=BeanUtil.copyProperties(qo, CounterDTO.class);
		List<Counter> list = counterDao.queryAll(dto);

		PageInfo<CounterVO> pageInfo = BeanUtil.transferPageInfo(list, CounterVO.class);
        logger.info("返回结果：" + pageInfo);
		return success(pageInfo);
	}

	/**
	 * 根据id查询
	 */
	@Override
	public ApiResult<CounterVO> get(Long id) {
       logger.info("请求参数--id：" + id);

		Counter entity = counterDao.queryById(id);
		if(entity==null){
			logger.info("当前不存在");
			return failureWithMsg("当前不存在");
		}
		CounterVO vo = BeanUtil.copyProperties(entity, CounterVO.class);

       logger.info("返回结果：" + vo);
		return success(vo);
	}

	/**
	 * 新增
	 */
	@Override
	public ApiResult save(CounterDTO dto) {
       logger.info("请求参数--CounterDTO：" + dto);

		Counter entity = BeanUtil.copyProperties(dto, Counter.class);

		if (counterDao.saveEntity(entity)>0) {
           logger.info("返回结果：" + entity);
			return success(entity);
		}
       logger.info("保存失败");
		return failureWithMsg("保存失败");
	}

	/**
	 * 修改
	 */
	@Override
	public ApiResult update(CounterDTO dto) {
       logger.info("请求参数--CounterDTO：" + dto);

		Counter oldEntity = counterDao.queryById(dto.getId());
		if(oldEntity==null){
           logger.info("当前不存在");
			return failureWithMsg("当前不存在");
		}

		Counter entity = BeanUtil.copyProperties(dto, Counter.class);

		if (counterDao.updateById(entity)>0) {

           logger.info("返回结果："+entity);
			return success(entity);
		}
        logger.info("保存失败");
		return failureWithMsg("修改失败");
	}

	/**
	 * 逻辑删除
	 */
	@Override
	public ApiResult delete(Long id) {
       logger.info("请求参数--id：" + id);

		Counter oldEntity = counterDao.queryById(id);
		if(oldEntity==null){
           logger.info("当前不存在");
			return failureWithMsg("当前不存在");
		}

		if (counterDao.deleteById(id)>0) {
           logger.info("删除成功");
			return success("删除成功");
		}

       logger.info("删除失败");
		return failureWithMsg("删除失败");
	}

	/**
	 * 批量删除（逻辑删除）
	 *
	 * @param idList
 	*/
	@Override
	public ApiResult deleteAll(List<Long> idList) {
		logger.info("请求参数--List<Long>：" + idList);

		if (idList != null) {
			int isOk = 0;
			isOk = counterDao.deleteAll(idList);   //删除数据
			if (isOk > 0) {
               logger.info("删除成功");
				return success("删除成功");
			}
		}

       logger.info("删除失败");
		return failureWithMsg("删除失败");
	}

}
