package com.dispenser.blogs.service.impl;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dispenser.blogs.dao.QuestionDao;
import com.dispenser.blogs.model.dto.QuestionDTO;
import com.dispenser.blogs.model.entity.Question;
import com.dispenser.blogs.model.qo.QuestionQO;
import com.dispenser.blogs.model.vo.QuestionVO;
import com.dispenser.blogs.service.QuestionService;
import com.dispenser.blogs.common.ApiResult;
import com.dispenser.blogs.common.BeanUtil;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static com.dispenser.blogs.common.ApiResultUtils.failureWithMsg;
import static com.dispenser.blogs.common.ApiResultUtils.success;

/**
 * 单据编码规则service实现类
 */
@Slf4j
@Service("questionService")
@Transactional
public class QuestionServiceImpl extends ServiceImpl<QuestionDao, Question> implements QuestionService {

    private final Logger logger = LoggerFactory.getLogger(QuestionServiceImpl.class);

    private static final String MENU_NAME = "问题";

	@Autowired
	QuestionDao questionDao;

	/**
	 * 分页获取问题列表
	 */
	@Override
	public ApiResult<PageInfo<QuestionVO>> list(QuestionQO qo) {

        logger.info("分页获取请求参数--QuestionQO：" + qo);

		PageHelper.startPage(qo.getPage(), qo.getLimit());

		QuestionDTO dto=BeanUtil.copyProperties(qo, QuestionDTO.class);
		List<Question> list = questionDao.queryAll(dto);

		PageInfo<QuestionVO> pageInfo = BeanUtil.transferPageInfo(list, QuestionVO.class);
        logger.info("返回结果：" + pageInfo);
		return success(pageInfo);
	}

	/**
	 * 根据id查询问题
	 */
	@Override
	public ApiResult<QuestionVO> get(Long id) {
        logger.info("根据id查询请求参数--id：" + id);

		Question entity = questionDao.queryById(id);
		if(entity==null){
            logger.info("当前问题不存在");
			return failureWithMsg("当前问题不存在");
		}
		QuestionVO vo = BeanUtil.copyProperties(entity, QuestionVO.class);

        logger.info("返回结果：" + vo);
		return success(vo);
	}

	/**
	 * 新增问题
	 */
	@Override
	public ApiResult save(QuestionDTO dto) {
        logger.info("新增请求参数--QuestionDTO：" + dto);

		Question entity = BeanUtil.copyProperties(dto, Question.class);

		if (questionDao.saveEntity(entity)>0) {
            logger.info("返回结果：" + entity);
			return success(entity);
		}
        logger.info("保存失败");
		return failureWithMsg("保存失败");
	}

	/**
	 * 修改问题
	 */
	@Override
	public ApiResult update(QuestionDTO dto) {
        logger.info("修改请求参数--QuestionDTO：" + dto);

		Question oldEntity = questionDao.queryById(dto.getId());
		if(oldEntity==null){
            logger.info("当前问题不存在");
			return failureWithMsg("当前问题不存在");
		}

		Question entity = BeanUtil.copyProperties(dto, Question.class);

		if (questionDao.updateById(entity)>0) {

            logger.info("返回结果："+entity);
			return success(entity);
		}
        logger.info("保存失败");
		return failureWithMsg("修改失败");
	}

	/**
	 * 逻辑删除问题
	 */
	@Override
	public ApiResult delete(Long id) {
        logger.info("逻辑删除请求参数--id：" + id);

		Question oldEntity = questionDao.queryById(id);
		if(oldEntity==null){
            logger.info("当前问题不存在");
			return failureWithMsg("当前问题不存在");
		}

		if (questionDao.deleteById(id)>0) {
            logger.info("删除成功");
			return success("删除成功");
		}

        logger.info("删除失败");
		return failureWithMsg("删除失败");
	}

	/**
	 * 批量删除（逻辑删除）
	 *
	 * @param idList
 	*/
	@Override
	public ApiResult deleteAll(List<Long> idList) {
        logger.info("批量删除（逻辑删除）请求参数--idList：" + idList);
		if (idList != null) {
			int isOk = 0;
			isOk = questionDao.deleteAll(idList);   //删除数据
			if (isOk > 0) {
                logger.info("删除成功");
				return success("删除成功");
			}
		}

        logger.info("删除失败");
		return failureWithMsg("删除失败");
	}

}
