package com.dispenser.blogs.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.dispenser.blogs.model.dto.QuestionTagsDTO;
import com.dispenser.blogs.model.entity.QuestionTags;
import com.dispenser.blogs.model.qo.QuestionTagsQO;
import com.dispenser.blogs.model.vo.QuestionTagsVO;
import com.dispenser.blogs.common.ApiResult;
import com.github.pagehelper.PageInfo;
import java.util.List;
/**
 * 问题与标签中间表service
 */
public interface QuestionTagsService extends IService<QuestionTags> {

	/**
     * 分页获取问题与标签中间表列表
     */
	ApiResult<PageInfo<QuestionTagsVO>> list(QuestionTagsQO qo);

	/**
     * 根据id查询问题与标签中间表
     */
	ApiResult<QuestionTagsVO> get(Long id);

	/**
     * 新增问题与标签中间表
     */
	ApiResult save(QuestionTagsDTO dto);

	/**
     * 修改问题与标签中间表
     */
	ApiResult update(QuestionTagsDTO dto);

	/**
     * 逻辑删除问题与标签中间表
     */
	ApiResult delete(Long id);

	/**
	* 批量删除（逻辑删除）
	*
	* @param idList
	*/
	ApiResult deleteAll(List<Long> idList);
}

