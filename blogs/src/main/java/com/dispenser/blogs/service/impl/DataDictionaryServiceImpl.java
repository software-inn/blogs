package com.dispenser.blogs.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dispenser.blogs.common.ApiResult;
import com.dispenser.blogs.common.BeanUtil;
import com.dispenser.blogs.dao.DataDictionaryDao;
import com.dispenser.blogs.model.dto.DataDictionaryDTO;
import com.dispenser.blogs.model.entity.DataDictionary;
import com.dispenser.blogs.model.qo.DataDictionaryQO;
import com.dispenser.blogs.model.vo.DataDictionaryVO;
import com.dispenser.blogs.service.DataDictionaryService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static com.dispenser.blogs.common.ApiResultUtils.failureWithMsg;
import static com.dispenser.blogs.common.ApiResultUtils.success;

/**
 * 单据编码规则service实现类
 */
@Slf4j
@Service("dataDictionaryService")
@Transactional
public class DataDictionaryServiceImpl extends ServiceImpl<DataDictionaryDao, DataDictionary> implements DataDictionaryService {

    private final Logger logger = LoggerFactory.getLogger(DataDictionaryServiceImpl.class);

    private static final String MENU_NAME = "数据字典表";

	@Autowired
	DataDictionaryDao dataDictionaryDao;

	/**
	 * 分页获取数据字典表列表
	 */
	@Override
	public ApiResult<PageInfo<DataDictionaryVO>> list(DataDictionaryQO qo) {

        logger.info("分页获取请求参数--DataDictionaryQO：" + qo);

		PageHelper.startPage(qo.getPage(), qo.getLimit());

		DataDictionaryDTO dto=BeanUtil.copyProperties(qo, DataDictionaryDTO.class);
		List<DataDictionary> list = dataDictionaryDao.queryAll(dto);

		PageInfo<DataDictionaryVO> pageInfo = BeanUtil.transferPageInfo(list, DataDictionaryVO.class);
        logger.info("返回结果：" + pageInfo);
		return success(pageInfo);
	}

	/**
	 * 根据id查询数据字典表
	 */
	@Override
	public ApiResult<DataDictionaryVO> get(Long id) {
        logger.info("根据id查询请求参数--id：" + id);

		DataDictionary entity = dataDictionaryDao.queryById(id);
		if(entity==null){
            logger.info("当前数据字典表不存在");
			return failureWithMsg("当前数据字典表不存在");
		}
		DataDictionaryVO vo = BeanUtil.copyProperties(entity, DataDictionaryVO.class);

        logger.info("返回结果：" + vo);
		return success(vo);
	}

	/**
	 * 新增数据字典表
	 */
	@Override
	public ApiResult save(DataDictionaryDTO dto) {
        logger.info("新增请求参数--DataDictionaryDTO：" + dto);

		DataDictionary entity = BeanUtil.copyProperties(dto, DataDictionary.class);

		if (dataDictionaryDao.saveEntity(entity)>0) {
            logger.info("返回结果：" + entity);
			return success(entity);
		}
        logger.info("保存失败");
		return failureWithMsg("保存失败");
	}

	/**
	 * 修改数据字典表
	 */
	@Override
	public ApiResult update(DataDictionaryDTO dto) {
        logger.info("修改请求参数--DataDictionaryDTO：" + dto);

		DataDictionary oldEntity = dataDictionaryDao.queryById(dto.getId());
		if(oldEntity==null){
            logger.info("当前数据字典表不存在");
			return failureWithMsg("当前数据字典表不存在");
		}

		DataDictionary entity = BeanUtil.copyProperties(dto, DataDictionary.class);

		if (dataDictionaryDao.updateById(entity)>0) {

            logger.info("返回结果："+entity);
			return success(entity);
		}
        logger.info("保存失败");
		return failureWithMsg("修改失败");
	}

	/**
	 * 逻辑删除数据字典表
	 */
	@Override
	public ApiResult delete(Long id) {
        logger.info("逻辑删除请求参数--id：" + id);

		DataDictionary oldEntity = dataDictionaryDao.queryById(id);
		if(oldEntity==null){
            logger.info("当前数据字典表不存在");
			return failureWithMsg("当前数据字典表不存在");
		}

		if (dataDictionaryDao.deleteById(id)>0) {
            logger.info("删除成功");
			return success("删除成功");
		}

        logger.info("删除失败");
		return failureWithMsg("删除失败");
	}

	/**
	 * 批量删除（逻辑删除）
	 *
	 * @param idList
 	*/
	@Override
	public ApiResult deleteAll(List<Long> idList) {
        logger.info("批量删除（逻辑删除）请求参数--idList：" + idList);
		if (idList != null) {
			int isOk = 0;
			isOk = dataDictionaryDao.deleteAll(idList);   //删除数据
			if (isOk > 0) {
                logger.info("删除成功");
				return success("删除成功");
			}
		}

        logger.info("删除失败");
		return failureWithMsg("删除失败");
	}

}
