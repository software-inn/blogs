package com.dispenser.blogs.service.impl;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dispenser.blogs.dao.QuestionTagsDao;
import com.dispenser.blogs.model.dto.QuestionTagsDTO;
import com.dispenser.blogs.model.entity.QuestionTags;
import com.dispenser.blogs.model.qo.QuestionTagsQO;
import com.dispenser.blogs.model.vo.QuestionTagsVO;
import com.dispenser.blogs.service.QuestionTagsService;
import com.dispenser.blogs.common.ApiResult;
import com.dispenser.blogs.common.BeanUtil;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static com.dispenser.blogs.common.ApiResultUtils.failureWithMsg;
import static com.dispenser.blogs.common.ApiResultUtils.success;

/**
 * 单据编码规则service实现类
 */
@Slf4j
@Service("questionTagsService")
@Transactional
public class QuestionTagsServiceImpl extends ServiceImpl<QuestionTagsDao, QuestionTags> implements QuestionTagsService {

    private final Logger logger = LoggerFactory.getLogger(QuestionTagsServiceImpl.class);

    private static final String MENU_NAME = "问题与标签中间表";

	@Autowired
	QuestionTagsDao questionTagsDao;

	/**
	 * 分页获取问题与标签中间表列表
	 */
	@Override
	public ApiResult<PageInfo<QuestionTagsVO>> list(QuestionTagsQO qo) {

        logger.info("分页获取请求参数--QuestionTagsQO：" + qo);

		PageHelper.startPage(qo.getPage(), qo.getLimit());

		QuestionTagsDTO dto=BeanUtil.copyProperties(qo, QuestionTagsDTO.class);
		List<QuestionTags> list = questionTagsDao.queryAll(dto);

		PageInfo<QuestionTagsVO> pageInfo = BeanUtil.transferPageInfo(list, QuestionTagsVO.class);
        logger.info("返回结果：" + pageInfo);
		return success(pageInfo);
	}

	/**
	 * 根据id查询问题与标签中间表
	 */
	@Override
	public ApiResult<QuestionTagsVO> get(Long id) {
        logger.info("根据id查询请求参数--id：" + id);

		QuestionTags entity = questionTagsDao.queryById(id);
		if(entity==null){
            logger.info("当前问题与标签中间表不存在");
			return failureWithMsg("当前问题与标签中间表不存在");
		}
		QuestionTagsVO vo = BeanUtil.copyProperties(entity, QuestionTagsVO.class);

        logger.info("返回结果：" + vo);
		return success(vo);
	}

	/**
	 * 新增问题与标签中间表
	 */
	@Override
	public ApiResult save(QuestionTagsDTO dto) {
        logger.info("新增请求参数--QuestionTagsDTO：" + dto);

		QuestionTags entity = BeanUtil.copyProperties(dto, QuestionTags.class);

		if (questionTagsDao.saveEntity(entity)>0) {
            logger.info("返回结果：" + entity);
			return success(entity);
		}
        logger.info("保存失败");
		return failureWithMsg("保存失败");
	}

	/**
	 * 修改问题与标签中间表
	 */
	@Override
	public ApiResult update(QuestionTagsDTO dto) {
        logger.info("修改请求参数--QuestionTagsDTO：" + dto);

		QuestionTags oldEntity = questionTagsDao.queryById(dto.getId());
		if(oldEntity==null){
            logger.info("当前问题与标签中间表不存在");
			return failureWithMsg("当前问题与标签中间表不存在");
		}

		QuestionTags entity = BeanUtil.copyProperties(dto, QuestionTags.class);

		if (questionTagsDao.updateById(entity)>0) {

            logger.info("返回结果："+entity);
			return success(entity);
		}
        logger.info("保存失败");
		return failureWithMsg("修改失败");
	}

	/**
	 * 逻辑删除问题与标签中间表
	 */
	@Override
	public ApiResult delete(Long id) {
        logger.info("逻辑删除请求参数--id：" + id);

		QuestionTags oldEntity = questionTagsDao.queryById(id);
		if(oldEntity==null){
            logger.info("当前问题与标签中间表不存在");
			return failureWithMsg("当前问题与标签中间表不存在");
		}

		if (questionTagsDao.deleteById(id)>0) {
            logger.info("删除成功");
			return success("删除成功");
		}

        logger.info("删除失败");
		return failureWithMsg("删除失败");
	}

	/**
	 * 批量删除（逻辑删除）
	 *
	 * @param idList
 	*/
	@Override
	public ApiResult deleteAll(List<Long> idList) {
        logger.info("批量删除（逻辑删除）请求参数--idList：" + idList);
		if (idList != null) {
			int isOk = 0;
			isOk = questionTagsDao.deleteAll(idList);   //删除数据
			if (isOk > 0) {
                logger.info("删除成功");
				return success("删除成功");
			}
		}

        logger.info("删除失败");
		return failureWithMsg("删除失败");
	}

}
