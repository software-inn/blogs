package com.dispenser.blogs.service.impl;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dispenser.blogs.dao.TagsDao;
import com.dispenser.blogs.model.dto.TagsDTO;
import com.dispenser.blogs.model.entity.Tags;
import com.dispenser.blogs.model.qo.TagsQO;
import com.dispenser.blogs.model.vo.TagsVO;
import com.dispenser.blogs.service.TagsService;
import com.dispenser.blogs.common.ApiResult;
import com.dispenser.blogs.common.BeanUtil;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static com.dispenser.blogs.common.ApiResultUtils.failureWithMsg;
import static com.dispenser.blogs.common.ApiResultUtils.success;

/**
 * 单据编码规则service实现类
 */
@Slf4j
@Service("tagsService")
@Transactional
public class TagsServiceImpl extends ServiceImpl<TagsDao, Tags> implements TagsService {

    private final Logger logger = LoggerFactory.getLogger(TagsServiceImpl.class);

    private static final String MENU_NAME = "标签";

	@Autowired
	TagsDao tagsDao;

	/**
	 * 分页获取标签列表
	 */
	@Override
	public ApiResult<PageInfo<TagsVO>> list(TagsQO qo) {

        logger.info("分页获取请求参数--TagsQO：" + qo);

		PageHelper.startPage(qo.getPage(), qo.getLimit());

		TagsDTO dto=BeanUtil.copyProperties(qo, TagsDTO.class);
		List<Tags> list = tagsDao.queryAll(dto);

		PageInfo<TagsVO> pageInfo = BeanUtil.transferPageInfo(list, TagsVO.class);
        logger.info("返回结果：" + pageInfo);
		return success(pageInfo);
	}

	/**
	 * 根据id查询标签
	 */
	@Override
	public ApiResult<TagsVO> get(Long id) {
        logger.info("根据id查询请求参数--id：" + id);

		Tags entity = tagsDao.queryById(id);
		if(entity==null){
            logger.info("当前标签不存在");
			return failureWithMsg("当前标签不存在");
		}
		TagsVO vo = BeanUtil.copyProperties(entity, TagsVO.class);

        logger.info("返回结果：" + vo);
		return success(vo);
	}

	/**
	 * 新增标签
	 */
	@Override
	public ApiResult save(TagsDTO dto) {
        logger.info("新增请求参数--TagsDTO：" + dto);

		Tags entity = BeanUtil.copyProperties(dto, Tags.class);
		entity.setIconId(dto.getAttachment() != null ? dto.getAttachment().getId():null);
		if (tagsDao.saveEntity(entity)>0) {
            logger.info("返回结果：" + entity);
			return success(entity);
		}
        logger.info("保存失败");
		return failureWithMsg("保存失败");
	}

	/**
	 * 修改标签
	 */
	@Override
	public ApiResult update(TagsDTO dto) {
        logger.info("修改请求参数--TagsDTO：" + dto);

		Tags oldEntity = tagsDao.queryById(dto.getId());
		if(oldEntity==null){
            logger.info("当前标签不存在");
			return failureWithMsg("当前标签不存在");
		}

		Tags entity = BeanUtil.copyProperties(dto, Tags.class);
		entity.setIconId(dto.getAttachment() != null ? dto.getAttachment().getId():null);
		if (tagsDao.updateById(entity)>0) {

            logger.info("返回结果："+entity);
			return success(entity);
		}
        logger.info("保存失败");
		return failureWithMsg("修改失败");
	}

	/**
	 * 逻辑删除标签
	 */
	@Override
	public ApiResult delete(Long id) {
        logger.info("逻辑删除请求参数--id：" + id);

		Tags oldEntity = tagsDao.queryById(id);
		if(oldEntity==null){
            logger.info("当前标签不存在");
			return failureWithMsg("当前标签不存在");
		}

		if (tagsDao.deleteById(id)>0) {
            logger.info("删除成功");
			return success("删除成功");
		}

        logger.info("删除失败");
		return failureWithMsg("删除失败");
	}

	/**
	 * 批量删除（逻辑删除）
	 *
	 * @param idList
 	*/
	@Override
	public ApiResult deleteAll(List<Long> idList) {
        logger.info("批量删除（逻辑删除）请求参数--idList：" + idList);
		if (idList != null) {
			int isOk = 0;
			isOk = tagsDao.deleteAll(idList);   //删除数据
			if (isOk > 0) {
                logger.info("删除成功");
				return success("删除成功");
			}
		}

        logger.info("删除失败");
		return failureWithMsg("删除失败");
	}

	@Override
	public ApiResult all(TagsQO qo) {

		logger.info("获取请求参数--TagsQO：" + qo);

		TagsDTO dto=BeanUtil.copyProperties(qo, TagsDTO.class);
		List<Tags> list = tagsDao.queryAll(dto);
		logger.info("返回结果：" + list);
		return success(list);
	}

}
