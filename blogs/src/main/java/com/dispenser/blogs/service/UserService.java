package com.dispenser.blogs.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.dispenser.blogs.common.ApiResult;
import com.dispenser.blogs.model.dto.UserDTO;
import com.dispenser.blogs.model.entity.User;
import com.dispenser.blogs.model.qo.UserQO;
import com.dispenser.blogs.model.vo.UserVO;
import com.github.pagehelper.PageInfo;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
/**
 * 用户表service
 */
public interface UserService extends IService<User> {

	/**
     * 分页获取用户表列表
     */
	ApiResult<PageInfo<UserVO>> list(UserQO qo);

	/**
     * 根据id查询用户表
     */
	ApiResult<UserVO> get(Long id);

	/**
     * 新增用户表
     */
	ApiResult save(UserDTO dto);

	/**
     * 修改用户表
     */
	ApiResult update(UserDTO dto);

	/**
     * 逻辑删除用户表
     */
	ApiResult delete(Long id);

	/**
	* 批量删除（逻辑删除）
	*
	* @param idList
	*/
	ApiResult deleteAll(List<Long> idList);


	/**
	 * 根据unionid 用户
	 * @param unionid
	 * @return
	 */
	User selectByUnionid(String unionid);


	/**
	 * 根据userName 用户
	 * @param userName
	 * @return
	 */
	User selectByUserName(String userName);


	/**
	 * 用户登录
	 * @param qo
	 * @return
	 */
    ApiResult login(HttpServletRequest request, UserQO qo);

	/**
	 * 用户退出
	 * @param id
	 * @return
	 */
    ApiResult logout(Long id);
}

