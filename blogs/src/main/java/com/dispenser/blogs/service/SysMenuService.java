package com.dispenser.blogs.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.dispenser.blogs.common.ApiResult;
import com.dispenser.blogs.model.dto.SysMenuDTO;
import com.dispenser.blogs.model.entity.SysMenu;
import com.dispenser.blogs.model.qo.SysMenuQO;
import com.dispenser.blogs.model.vo.SysMenuVO;
import com.github.pagehelper.PageInfo;

import java.util.List;
/**
 * 系统菜单表service
 */
public interface SysMenuService extends IService<SysMenu> {

	/**
     * 分页获取系统菜单表列表
     */
	ApiResult list(SysMenuQO qo);

	/**
     * 根据id查询系统菜单表
     */
	ApiResult<SysMenuVO> get(Long id);

	/**
     * 新增系统菜单表
     */
	ApiResult save(SysMenuDTO dto);

	/**
     * 修改系统菜单表
     */
	ApiResult update(SysMenuDTO dto);

	/**
     * 逻辑删除系统菜单表
     */
	ApiResult delete(Long id);

	/**
	* 批量删除（逻辑删除）
	*
	* @param idList
	*/
	ApiResult deleteAll(List<Long> idList);

	/**
	 * 分页数据
	 * @param qo
	 * @return
	 */
	ApiResult all(SysMenuQO qo);
}

