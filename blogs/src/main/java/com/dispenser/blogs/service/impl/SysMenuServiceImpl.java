package com.dispenser.blogs.service.impl;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.dispenser.blogs.controller.CounterController;
import com.dispenser.blogs.model.dto.CounterDTO;
import com.dispenser.blogs.model.vo.CounterVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dispenser.blogs.dao.SysMenuDao;
import com.dispenser.blogs.model.dto.SysMenuDTO;
import com.dispenser.blogs.model.entity.SysMenu;
import com.dispenser.blogs.model.entity.SysMenu;
import com.dispenser.blogs.model.qo.SysMenuQO;
import com.dispenser.blogs.model.vo.SysMenuVO;
import com.dispenser.blogs.service.SysMenuService;
import com.dispenser.blogs.common.ApiResult;
import com.dispenser.blogs.common.BeanUtil;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static com.dispenser.blogs.common.ApiResultUtils.failureWithMsg;
import static com.dispenser.blogs.common.ApiResultUtils.success;

/**
 * 单据编码规则service实现类
 */
@Slf4j
@Service("sysMenuService")
@Transactional
public class SysMenuServiceImpl extends ServiceImpl<SysMenuDao, SysMenu> implements SysMenuService {

    private final Logger logger = LoggerFactory.getLogger(CounterController.class);

    private static final String MENU_NAME = "系统菜单表";

	@Autowired
	SysMenuDao sysMenuDao;

	/**
	 * 分页获取系统菜单表列表
	 */
	@Override
	public ApiResult all(SysMenuQO qo) {

        logger.info("请求参数--SysMenuQO：" + qo);

		SysMenuDTO dto=BeanUtil.copyProperties(qo, SysMenuDTO.class);
		List<SysMenu> list = sysMenuDao.queryAll(dto);

        logger.info("返回结果：" + list);
		return success(list);
	}

	/**
	 * 分页获取系统菜单表列表
	 */
	@Override
	public ApiResult list(SysMenuQO qo) {

		logger.info("请求参数--SysMenuQO：" + qo);
		PageHelper.startPage(qo.getPage(), qo.getLimit());
		SysMenuDTO dto=BeanUtil.copyProperties(qo, SysMenuDTO.class);
		List<SysMenu> list = sysMenuDao.queryAll(dto);
		PageInfo<SysMenuVO> pageInfo = BeanUtil.transferPageInfo(list, SysMenuVO.class);
		logger.info("返回结果：" + pageInfo);
		return success(pageInfo);
	}

	/**
	 * 根据id查询系统菜单表
	 */
	@Override
	public ApiResult<SysMenuVO> get(Long id) {
        logger.info("根据id查询请求参数--id：" + id);

		SysMenu entity = sysMenuDao.queryById(id);
		if(entity==null){
            logger.info("当前系统菜单表不存在");
			return failureWithMsg("当前系统菜单表不存在");
		}
		SysMenuVO vo = BeanUtil.copyProperties(entity, SysMenuVO.class);

        logger.info("返回结果：" + vo);
		return success(vo);
	}

	/**
	 * 新增系统菜单表
	 */
	@Override
	public ApiResult save(SysMenuDTO dto) {
        logger.info("新增请求参数--SysMenuDTO：" + dto);

		SysMenu entity = BeanUtil.copyProperties(dto, SysMenu.class);

		if (sysMenuDao.saveEntity(entity)>0) {
            logger.info("返回结果：" + entity);
			return success(entity);
		}
        logger.info("保存失败");
		return failureWithMsg("保存失败");
	}

	/**
	 * 修改系统菜单表
	 */
	@Override
	public ApiResult update(SysMenuDTO dto) {
        logger.info("修改请求参数--SysMenuDTO：" + dto);

		SysMenu oldEntity = sysMenuDao.queryById(dto.getId());
		if(oldEntity==null){
            logger.info("当前系统菜单表不存在");
			return failureWithMsg("当前系统菜单表不存在");
		}

		SysMenu entity = BeanUtil.copyProperties(dto, SysMenu.class);

		if (sysMenuDao.updateById(entity)>0) {

            logger.info("返回结果："+entity);
			return success(entity);
		}
        logger.info("保存失败");
		return failureWithMsg("修改失败");
	}

	/**
	 * 逻辑删除系统菜单表
	 */
	@Override
	public ApiResult delete(Long id) {
        logger.info("逻辑删除请求参数--id：" + id);

		SysMenu oldEntity = sysMenuDao.queryById(id);
		if(oldEntity==null){
            logger.info("当前系统菜单表不存在");
			return failureWithMsg("当前系统菜单表不存在");
		}

		if (sysMenuDao.deleteById(id)>0) {
            logger.info("删除成功");
			return success("删除成功");
		}

        logger.info("删除失败");
		return failureWithMsg("删除失败");
	}

	/**
	 * 批量删除（逻辑删除）
	 *
	 * @param idList
 	*/
	@Override
	public ApiResult deleteAll(List<Long> idList) {
        logger.info("批量删除（逻辑删除）请求参数--idList：" + idList);
		if (idList != null) {
			int isOk = 0;
			isOk = sysMenuDao.deleteAll(idList);   //删除数据
			if (isOk > 0) {
                logger.info("删除成功");
				return success("删除成功");
			}
		}

        logger.info("删除失败");
		return failureWithMsg("删除失败");
	}

}
