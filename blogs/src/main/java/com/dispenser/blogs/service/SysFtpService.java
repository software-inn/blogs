package com.dispenser.blogs.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.dispenser.blogs.common.ApiResult;
import com.dispenser.blogs.model.dto.SysFtpDTO;
import com.dispenser.blogs.model.entity.Attachment;
import com.dispenser.blogs.model.entity.SysFtp;
import com.dispenser.blogs.model.qo.SysFtpQO;
import com.dispenser.blogs.model.vo.SysFtpVO;
import com.github.pagehelper.PageInfo;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

/**
 * 图片资源FTP服务器表service
 */
public interface SysFtpService extends IService<SysFtp> {

	/**
     * 分页获取图片资源FTP服务器表列表
     */
	ApiResult<PageInfo<SysFtpVO>> list(SysFtpQO qo);

	/**
     * 根据id查询图片资源FTP服务器表
     */
	ApiResult<SysFtpVO> get(Long id);

	/**
     * 新增图片资源FTP服务器表
     */
	ApiResult save(SysFtpDTO dto);

	/**
     * 修改图片资源FTP服务器表
     */
	ApiResult update(SysFtpDTO dto);

	/**
     * 逻辑删除图片资源FTP服务器表
     */
	ApiResult delete(Long id);

	/**
	* 批量删除（逻辑删除）
	*
	* @param idList
	*/
	ApiResult deleteAll(List<Long> idList);

	/**
	 * Base64 上传文件
	 * @param params
	 * @return
	 */
	 Boolean uploadByBase64(Map<String, Object> params);

	/**
	 * 上传文件
	 * @param flie
	 * @param oldFileId 历史文件id
	 * @return
	 */
	ApiResult uploadFile(MultipartFile flie, String oldFileId ,String folder);

	/**
	 * sft网络文件上传
	 *
	 * @param folder
	 *            文件夹 如：account账户、activity:活动，多层目录用/隔开，如：user/image用户头像
	 * @param file_url
	 *            网络流地址
	 * @param oldFileId
	 *            原文件id
	 * @param extend
	 *            文件后缀
	 * @return
	 */
	 Attachment upload(String folder, Long oldFileId, String extend, String file_url);
}

