package com.dispenser.blogs.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.dispenser.blogs.common.ApiResult;
import com.dispenser.blogs.model.dto.SysLogDTO;
import com.dispenser.blogs.model.entity.SysLog;
import com.dispenser.blogs.model.qo.SysLogQO;
import com.dispenser.blogs.model.vo.SysLogVO;
import com.github.pagehelper.PageInfo;

import java.util.List;
/**
 * 系统日志service
 */
public interface SysLogService extends IService<SysLog> {

	/**
     * 分页获取系统日志列表
     */
	ApiResult<PageInfo<SysLogVO>> list(SysLogQO qo);

	/**
     * 根据id查询系统日志
     */
	ApiResult<SysLogVO> get(Long id);

	/**
     * 新增系统日志
     */
	ApiResult save(SysLogDTO dto);

	/**
     * 修改系统日志
     */
	ApiResult update(SysLogDTO dto);

	/**
     * 逻辑删除系统日志
     */
	ApiResult delete(Long id);

	/**
	* 批量删除（逻辑删除）
	*
	* @param idList
	*/
	ApiResult deleteAll(List<Long> idList);
}

