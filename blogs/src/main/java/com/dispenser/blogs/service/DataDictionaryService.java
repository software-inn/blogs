package com.dispenser.blogs.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.dispenser.blogs.model.dto.DataDictionaryDTO;
import com.dispenser.blogs.model.entity.DataDictionary;
import com.dispenser.blogs.model.qo.DataDictionaryQO;
import com.dispenser.blogs.model.vo.DataDictionaryVO;
import com.dispenser.blogs.common.ApiResult;
import com.github.pagehelper.PageInfo;
import java.util.List;
/**
 * 数据字典表service
 */
public interface DataDictionaryService extends IService<DataDictionary> {

	/**
     * 分页获取数据字典表列表
     */
	ApiResult<PageInfo<DataDictionaryVO>> list(DataDictionaryQO qo);

	/**
     * 根据id查询数据字典表
     */
	ApiResult<DataDictionaryVO> get(Long id);

	/**
     * 新增数据字典表
     */
	ApiResult save(DataDictionaryDTO dto);

	/**
     * 修改数据字典表
     */
	ApiResult update(DataDictionaryDTO dto);

	/**
     * 逻辑删除数据字典表
     */
	ApiResult delete(Long id);

	/**
	* 批量删除（逻辑删除）
	*
	* @param idList
	*/
	ApiResult deleteAll(List<Long> idList);
}

