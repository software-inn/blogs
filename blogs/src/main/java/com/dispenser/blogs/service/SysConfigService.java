package com.dispenser.blogs.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.dispenser.blogs.common.ApiResult;
import com.dispenser.blogs.model.dto.SysConfigDTO;
import com.dispenser.blogs.model.entity.SysConfig;
import com.dispenser.blogs.model.qo.SysConfigQO;
import com.dispenser.blogs.model.vo.SysConfigVO;
import com.github.pagehelper.PageInfo;

import java.util.List;
/**
 * 系统配置表service
 */
public interface SysConfigService extends IService<SysConfig> {

	/**
     * 分页获取系统配置表列表
     */
	ApiResult<PageInfo<SysConfigVO>> list(SysConfigQO qo);

	/**
     * 根据id查询系统配置表
     */
	ApiResult<SysConfigVO> get(Long id);

	/**
     * 新增系统配置表
     */
	ApiResult save(SysConfigDTO dto);

	/**
     * 修改系统配置表
     */
	ApiResult update(SysConfigDTO dto);

	/**
     * 逻辑删除系统配置表
     */
	ApiResult delete(Long id);

	/**
	* 批量删除（逻辑删除）
	*
	* @param idList
	*/
	ApiResult deleteAll(List<Long> idList);
}

