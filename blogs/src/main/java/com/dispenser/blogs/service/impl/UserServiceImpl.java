package com.dispenser.blogs.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dispenser.blogs.common.*;
import com.dispenser.blogs.controller.CounterController;
import com.dispenser.blogs.dao.AttachmentDao;
import com.dispenser.blogs.dao.UserDao;
import com.dispenser.blogs.model.dto.UserDTO;
import com.dispenser.blogs.model.entity.User;
import com.dispenser.blogs.model.enums.UserEnum;
import com.dispenser.blogs.model.qo.UserQO;
import com.dispenser.blogs.model.vo.UserVO;
import com.dispenser.blogs.service.UserService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;

import static com.dispenser.blogs.common.ApiResultUtils.failureWithMsg;
import static com.dispenser.blogs.common.ApiResultUtils.success;

/**
 * 单据编码规则service实现类
 */
@Slf4j
@Service("userService")
@Transactional
public class UserServiceImpl extends ServiceImpl<UserDao, User> implements UserService {

    private final Logger logger = LoggerFactory.getLogger(CounterController.class);

    private static final String MENU_NAME = "用户表";

	@Autowired
	UserDao userDao;
	@Autowired
	AttachmentDao attachmentDao;
	@Autowired
	RedisTemplates redisTemplates;

	/**
	 * 分页获取用户表列表
	 */
	@Override
	public ApiResult<PageInfo<UserVO>> list(UserQO qo) {

        logger.info("分页获取请求参数--UserQO：" + qo);

		PageHelper.startPage(qo.getPage(), qo.getLimit());

		UserDTO dto=BeanUtil.copyProperties(qo, UserDTO.class);
		List<User> list = userDao.queryAll(dto);

		PageInfo<UserVO> pageInfo = BeanUtil.transferPageInfo(list, UserVO.class);
        logger.info("返回结果：" + pageInfo);
		return success(pageInfo);
	}

	/**
	 * 根据id查询用户表
	 */
	@Override
	public ApiResult<UserVO> get(Long id) {
        logger.info("根据id查询请求参数--id：" + id);

		User entity = userDao.queryById(id);
		if(entity==null){
            logger.info("当前用户表不存在");
			return failureWithMsg("当前用户表不存在");
		}
		if(entity.getAttachmentId() != null){
			entity.setAttachment(attachmentDao.queryById(entity.getAttachmentId()));
		}
		UserVO vo = BeanUtil.copyProperties(entity, UserVO.class);

        logger.info("返回结果：" + vo);
		return success(vo);
	}

	/**
	 * 新增用户表
	 */
	@Override
	public ApiResult save(UserDTO dto) {
        logger.info("新增请求参数--UserDTO：" + dto);

		User entity = BeanUtil.copyProperties(dto, User.class);

		if (userDao.saveEntity(entity)>0) {
            logger.info("返回结果：" + entity);
			return success(entity);
		}
        logger.info("保存失败");
		return failureWithMsg("保存失败");
	}

	/**
	 * 修改用户表
	 */
	@Override
	public ApiResult update(UserDTO dto) {
        logger.info("修改请求参数--UserDTO：" + dto);

		User oldEntity = userDao.queryById(dto.getId());
		if(oldEntity==null){
            logger.info("当前用户表不存在");
			return failureWithMsg("当前用户表不存在");
		}

		User entity = BeanUtil.copyProperties(dto, User.class);

		if (userDao.updateById(entity)>0) {

            logger.info("返回结果："+entity);
			return success(entity);
		}
        logger.info("保存失败");
		return failureWithMsg("修改失败");
	}

	/**
	 * 逻辑删除用户表
	 */
	@Override
	public ApiResult delete(Long id) {
        logger.info("逻辑删除请求参数--id：" + id);

		User oldEntity = userDao.queryById(id);
		if(oldEntity==null){
            logger.info("当前用户表不存在");
			return failureWithMsg("当前用户表不存在");
		}

		if (userDao.deleteById(id)>0) {
            logger.info("删除成功");
			return success("删除成功");
		}

        logger.info("删除失败");
		return failureWithMsg("删除失败");
	}

	/**
	 * 批量删除（逻辑删除）
	 *
	 * @param idList
 	*/
	@Override
	public ApiResult deleteAll(List<Long> idList) {
        logger.info("批量删除（逻辑删除）请求参数--idList：" + idList);
		if (idList != null) {
			int isOk = 0;
			isOk = userDao.deleteAll(idList);   //删除数据
			if (isOk > 0) {
                logger.info("删除成功");
				return success("删除成功");
			}
		}

        logger.info("删除失败");
		return failureWithMsg("删除失败");
	}

	@Override
	public User selectByUnionid(String unionid) {
		return userDao.selectByUnionid(unionid);
	}

	@Override
	public User selectByUserName(String userName) {

		return userDao.selectByUserName(userName);
	}

	@Override
	public ApiResult login(HttpServletRequest request,UserQO qo) {
		logger.info("新增请求参数--UserQO：" + qo);

		User user = userDao.selectByUserName(qo.getUserName());

		if (user == null) {
			logger.info("用户不存在");
			return failureWithMsg("用户不存在");
		}
		if(!Md5Encrypt.md5(qo.getPassword()).equals(user.getPassword())){
			logger.info("密码错误");
			return failureWithMsg("密码错误");
		}
		user.setLoginIp(IPUtils.getIpAddr(request));
		user.setLoginDate(new Date());
		userDao.updateById(user);
		// 生成token,格式:用户id;时间戳
		String token = JWT.sign(user.getId() + ";" + System.currentTimeMillis(), 0);
		// 将token存到redis中，有效期24小时
		redisTemplates.set(UserEnum.USER_TOKEN.getValue() + user.getId(), token, Integer.MAX_VALUE);
		logger.info("返回--token：" + token);
		return success(token);
	}

	@Override
	public ApiResult logout(Long id) {
		if(id == null){
			logger.info("当前用户已离线");
			return failureWithMsg("当前用户已离线");
		}
		redisTemplates.del(UserEnum.USER_TOKEN.getValue() + id);
		return success("退出成功");
	}

}
