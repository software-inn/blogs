package com.dispenser.blogs.service.impl;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.dispenser.blogs.controller.CounterController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dispenser.blogs.dao.SysLogDao;
import com.dispenser.blogs.model.dto.SysLogDTO;
import com.dispenser.blogs.model.entity.SysLog;
import com.dispenser.blogs.model.entity.SysLog;
import com.dispenser.blogs.model.qo.SysLogQO;
import com.dispenser.blogs.model.vo.SysLogVO;
import com.dispenser.blogs.service.SysLogService;
import com.dispenser.blogs.common.ApiResult;
import com.dispenser.blogs.common.BeanUtil;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static com.dispenser.blogs.common.ApiResultUtils.failureWithMsg;
import static com.dispenser.blogs.common.ApiResultUtils.success;

/**
 * 单据编码规则service实现类
 */
@Slf4j
@Service("sysLogService")
@Transactional
public class SysLogServiceImpl extends ServiceImpl<SysLogDao, SysLog> implements SysLogService {

    private final Logger logger = LoggerFactory.getLogger(CounterController.class);

    private static final String MENU_NAME = "系统日志";

	@Autowired
	SysLogDao sysLogDao;

	/**
	 * 分页获取系统日志列表
	 */
	@Override
	public ApiResult<PageInfo<SysLogVO>> list(SysLogQO qo) {

        logger.info("分页获取请求参数--SysLogQO：" + qo);

		PageHelper.startPage(qo.getPage(), qo.getLimit());

		SysLogDTO dto=BeanUtil.copyProperties(qo, SysLogDTO.class);
		List<SysLog> list = sysLogDao.queryAll(dto);

		PageInfo<SysLogVO> pageInfo = BeanUtil.transferPageInfo(list, SysLogVO.class);
        logger.info("返回结果：" + pageInfo);
		return success(pageInfo);
	}

	/**
	 * 根据id查询系统日志
	 */
	@Override
	public ApiResult<SysLogVO> get(Long id) {
        logger.info("根据id查询请求参数--id：" + id);

		SysLog entity = sysLogDao.queryById(id);
		if(entity==null){
            logger.info("当前系统日志不存在");
			return failureWithMsg("当前系统日志不存在");
		}
		SysLogVO vo = BeanUtil.copyProperties(entity, SysLogVO.class);

        logger.info("返回结果：" + vo);
		return success(vo);
	}

	/**
	 * 新增系统日志
	 */
	@Override
	public ApiResult save(SysLogDTO dto) {
        logger.info("新增请求参数--SysLogDTO：" + dto);

		SysLog entity = BeanUtil.copyProperties(dto, SysLog.class);

		if (sysLogDao.saveEntity(entity)>0) {
            logger.info("返回结果：" + entity);
			return success(entity);
		}
        logger.info("保存失败");
		return failureWithMsg("保存失败");
	}

	/**
	 * 修改系统日志
	 */
	@Override
	public ApiResult update(SysLogDTO dto) {
        logger.info("修改请求参数--SysLogDTO：" + dto);

		SysLog oldEntity = sysLogDao.queryById(dto.getId());
		if(oldEntity==null){
            logger.info("当前系统日志不存在");
			return failureWithMsg("当前系统日志不存在");
		}

		SysLog entity = BeanUtil.copyProperties(dto, SysLog.class);

		if (sysLogDao.updateById(entity)>0) {

            logger.info("返回结果："+entity);
			return success(entity);
		}
        logger.info("保存失败");
		return failureWithMsg("修改失败");
	}

	/**
	 * 逻辑删除系统日志
	 */
	@Override
	public ApiResult delete(Long id) {
        logger.info("逻辑删除请求参数--id：" + id);

		SysLog oldEntity = sysLogDao.queryById(id);
		if(oldEntity==null){
            logger.info("当前系统日志不存在");
			return failureWithMsg("当前系统日志不存在");
		}

		if (sysLogDao.deleteById(id)>0) {
            logger.info("删除成功");
			return success("删除成功");
		}

        logger.info("删除失败");
		return failureWithMsg("删除失败");
	}

	/**
	 * 批量删除（逻辑删除）
	 *
	 * @param idList
 	*/
	@Override
	public ApiResult deleteAll(List<Long> idList) {
        logger.info("批量删除（逻辑删除）请求参数--idList：" + idList);
		if (idList != null) {
			int isOk = 0;
			isOk = sysLogDao.deleteAll(idList);   //删除数据
			if (isOk > 0) {
                logger.info("删除成功");
				return success("删除成功");
			}
		}

        logger.info("删除失败");
		return failureWithMsg("删除失败");
	}

}
