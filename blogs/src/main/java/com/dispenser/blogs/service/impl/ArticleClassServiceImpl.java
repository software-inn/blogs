package com.dispenser.blogs.service.impl;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.dispenser.blogs.controller.CounterController;
import com.dispenser.blogs.dao.AttachmentDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dispenser.blogs.dao.ArticleClassDao;
import com.dispenser.blogs.model.dto.ArticleClassDTO;
import com.dispenser.blogs.model.entity.ArticleClass;
import com.dispenser.blogs.model.qo.ArticleClassQO;
import com.dispenser.blogs.model.vo.ArticleClassVO;
import com.dispenser.blogs.service.ArticleClassService;
import com.dispenser.blogs.common.ApiResult;
import com.dispenser.blogs.common.BeanUtil;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static com.dispenser.blogs.common.ApiResultUtils.failureWithMsg;
import static com.dispenser.blogs.common.ApiResultUtils.success;

/**
 * 单据编码规则service实现类
 */
@Slf4j
@Service("articleClassService")
@Transactional
public class ArticleClassServiceImpl extends ServiceImpl<ArticleClassDao, ArticleClass> implements ArticleClassService {

    private final Logger logger = LoggerFactory.getLogger(CounterController.class);

    private static final String MENU_NAME = "分类";

	@Autowired
	ArticleClassDao articleClassDao;
	@Autowired
	AttachmentDao attachmentDao;


	/**
	 * 分页获取分类列表
	 */
	@Override
	public ApiResult<PageInfo<ArticleClassVO>> list(ArticleClassQO qo) {

        logger.info("分页获取请求参数--ArticleClassQO：" + qo);

		PageHelper.startPage(qo.getPage(), qo.getLimit());

		ArticleClassDTO dto=BeanUtil.copyProperties(qo, ArticleClassDTO.class);
		List<ArticleClass> list = articleClassDao.queryAll(dto);

		PageInfo<ArticleClassVO> pageInfo = BeanUtil.transferPageInfo(list, ArticleClassVO.class);
        logger.info("返回结果：" + pageInfo);
		return success(pageInfo);
	}

	/**
	 * 根据id查询分类
	 */
	@Override
	public ApiResult<ArticleClassVO> get(Long id) {
        logger.info("根据id查询请求参数--id：" + id);

		ArticleClass entity = articleClassDao.queryById(id);
		if(entity==null){
            logger.info("当前分类不存在");
			return failureWithMsg("当前分类不存在");
		}
		entity.setArticleClass(articleClassDao.queryById(entity.getParentId()));
		entity.setAttachment(attachmentDao.queryById(entity.getIconId()));
		ArticleClassVO vo = BeanUtil.copyProperties(entity, ArticleClassVO.class);

        logger.info("返回结果：" + vo);
		return success(vo);
	}

	/**
	 * 新增分类
	 */
	@Override
	public ApiResult save(ArticleClassDTO dto) {
        logger.info("新增请求参数--ArticleClassDTO：" + dto);

		ArticleClass entity = BeanUtil.copyProperties(dto, ArticleClass.class);
		entity.setIconId(dto.getAttachment()==null ? null:dto.getAttachment().getId());
		if (articleClassDao.saveEntity(entity)>0) {
            logger.info("返回结果：" + entity);
			return success(entity);
		}
        logger.info("保存失败");
		return failureWithMsg("保存失败");
	}

	/**
	 * 修改分类
	 */
	@Override
	public ApiResult update(ArticleClassDTO dto) {
        logger.info("修改请求参数--ArticleClassDTO：" + dto);

		ArticleClass oldEntity = articleClassDao.queryById(dto.getId());
		if(oldEntity==null){
            logger.info("当前分类不存在");
			return failureWithMsg("当前分类不存在");
		}

		ArticleClass entity = BeanUtil.copyProperties(dto, ArticleClass.class);
		entity.setIconId(dto.getAttachment()==null ? null:dto.getAttachment().getId());
		if (articleClassDao.updateById(entity)>0) {

            logger.info("返回结果："+entity);
			return success(entity);
		}
        logger.info("保存失败");
		return failureWithMsg("修改失败");
	}

	/**
	 * 逻辑删除分类
	 */
	@Override
	public ApiResult delete(Long id) {
        logger.info("逻辑删除请求参数--id：" + id);

		ArticleClass oldEntity = articleClassDao.queryById(id);
		if(oldEntity==null){
            logger.info("当前分类不存在");
			return failureWithMsg("当前分类不存在");
		}

		if (articleClassDao.deleteById(id)>0) {
            logger.info("删除成功");
			return success("删除成功");
		}

        logger.info("删除失败");
		return failureWithMsg("删除失败");
	}

	/**
	 * 批量删除（逻辑删除）
	 *
	 * @param idList
 	*/
	@Override
	public ApiResult deleteAll(List<Long> idList) {
        logger.info("批量删除（逻辑删除）请求参数--idList：" + idList);
		if (idList != null) {
			int isOk = 0;
			isOk = articleClassDao.deleteAll(idList);   //删除数据
			if (isOk > 0) {
                logger.info("删除成功");
				return success("删除成功");
			}
		}

        logger.info("删除失败");
		return failureWithMsg("删除失败");
	}

	/**
	 * 获取分类所有数据
	 */
	@Override
	public ApiResult getAll(ArticleClassQO qo) {
		logger.info("获取所有请求参数--ArticleClassQO：" + qo);
		ArticleClassDTO dto=BeanUtil.copyProperties(qo, ArticleClassDTO.class);
		List<ArticleClass> list = articleClassDao.queryAll(dto);
		logger.info("返回结果：" + list);
		return success(list);
	}

}
