package com.dispenser.blogs.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.dispenser.blogs.model.dto.ArticleDTO;
import com.dispenser.blogs.model.entity.Article;
import com.dispenser.blogs.model.qo.ArticleQO;
import com.dispenser.blogs.model.vo.ArticleVO;
import com.dispenser.blogs.common.ApiResult;
import com.github.pagehelper.PageInfo;
import java.util.List;
/**
 * 文章service
 */
public interface ArticleService extends IService<Article> {

	/**
     * 分页获取文章列表
     */
	ApiResult<PageInfo<ArticleVO>> list(ArticleQO qo);

	/**
     * 根据id查询文章
     */
	ApiResult<ArticleVO> get(Long id);

	/**
     * 新增文章
     */
	ApiResult save(ArticleDTO dto);

	/**
     * 修改文章
     */
	ApiResult update(ArticleDTO dto);

	/**
     * 逻辑删除文章
     */
	ApiResult delete(Long id);

	/**
	* 批量删除（逻辑删除）
	*
	* @param idList
	*/
	ApiResult deleteAll(List<Long> idList);
}

