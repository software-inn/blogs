package com.dispenser.blogs.service.impl;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dispenser.blogs.dao.ArticleTagsDao;
import com.dispenser.blogs.model.dto.ArticleTagsDTO;
import com.dispenser.blogs.model.entity.ArticleTags;
import com.dispenser.blogs.model.qo.ArticleTagsQO;
import com.dispenser.blogs.model.vo.ArticleTagsVO;
import com.dispenser.blogs.service.ArticleTagsService;
import com.dispenser.blogs.common.ApiResult;
import com.dispenser.blogs.common.BeanUtil;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static com.dispenser.blogs.common.ApiResultUtils.failureWithMsg;
import static com.dispenser.blogs.common.ApiResultUtils.success;

/**
 * 单据编码规则service实现类
 */
@Slf4j
@Service("articleTagsService")
@Transactional
public class ArticleTagsServiceImpl extends ServiceImpl<ArticleTagsDao, ArticleTags> implements ArticleTagsService {

    private final Logger logger = LoggerFactory.getLogger(ArticleTagsServiceImpl.class);

    private static final String MENU_NAME = "文章与标签中间表";

	@Autowired
	ArticleTagsDao articleTagsDao;

	/**
	 * 分页获取文章与标签中间表列表
	 */
	@Override
	public ApiResult<PageInfo<ArticleTagsVO>> list(ArticleTagsQO qo) {

        logger.info("分页获取请求参数--ArticleTagsQO：" + qo);

		PageHelper.startPage(qo.getPage(), qo.getLimit());

		ArticleTagsDTO dto=BeanUtil.copyProperties(qo, ArticleTagsDTO.class);
		List<ArticleTags> list = articleTagsDao.queryAll(dto);

		PageInfo<ArticleTagsVO> pageInfo = BeanUtil.transferPageInfo(list, ArticleTagsVO.class);
        logger.info("返回结果：" + pageInfo);
		return success(pageInfo);
	}

	/**
	 * 根据id查询文章与标签中间表
	 */
	@Override
	public ApiResult<ArticleTagsVO> get(Long id) {
        logger.info("根据id查询请求参数--id：" + id);

		ArticleTags entity = articleTagsDao.queryById(id);
		if(entity==null){
            logger.info("当前文章与标签中间表不存在");
			return failureWithMsg("当前文章与标签中间表不存在");
		}
		ArticleTagsVO vo = BeanUtil.copyProperties(entity, ArticleTagsVO.class);

        logger.info("返回结果：" + vo);
		return success(vo);
	}

	/**
	 * 新增文章与标签中间表
	 */
	@Override
	public ApiResult save(ArticleTagsDTO dto) {
        logger.info("新增请求参数--ArticleTagsDTO：" + dto);

		ArticleTags entity = BeanUtil.copyProperties(dto, ArticleTags.class);

		if (articleTagsDao.saveEntity(entity)>0) {
            logger.info("返回结果：" + entity);
			return success(entity);
		}
        logger.info("保存失败");
		return failureWithMsg("保存失败");
	}

	/**
	 * 修改文章与标签中间表
	 */
	@Override
	public ApiResult update(ArticleTagsDTO dto) {
        logger.info("修改请求参数--ArticleTagsDTO：" + dto);

		ArticleTags oldEntity = articleTagsDao.queryById(dto.getId());
		if(oldEntity==null){
            logger.info("当前文章与标签中间表不存在");
			return failureWithMsg("当前文章与标签中间表不存在");
		}

		ArticleTags entity = BeanUtil.copyProperties(dto, ArticleTags.class);

		if (articleTagsDao.updateById(entity)>0) {

            logger.info("返回结果："+entity);
			return success(entity);
		}
        logger.info("保存失败");
		return failureWithMsg("修改失败");
	}

	/**
	 * 逻辑删除文章与标签中间表
	 */
	@Override
	public ApiResult delete(Long id) {
        logger.info("逻辑删除请求参数--id：" + id);

		ArticleTags oldEntity = articleTagsDao.queryById(id);
		if(oldEntity==null){
            logger.info("当前文章与标签中间表不存在");
			return failureWithMsg("当前文章与标签中间表不存在");
		}

		if (articleTagsDao.deleteById(id)>0) {
            logger.info("删除成功");
			return success("删除成功");
		}

        logger.info("删除失败");
		return failureWithMsg("删除失败");
	}

	/**
	 * 批量删除（逻辑删除）
	 *
	 * @param idList
 	*/
	@Override
	public ApiResult deleteAll(List<Long> idList) {
        logger.info("批量删除（逻辑删除）请求参数--idList：" + idList);
		if (idList != null) {
			int isOk = 0;
			isOk = articleTagsDao.deleteAll(idList);   //删除数据
			if (isOk > 0) {
                logger.info("删除成功");
				return success("删除成功");
			}
		}

        logger.info("删除失败");
		return failureWithMsg("删除失败");
	}

}
