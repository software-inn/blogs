package com.dispenser.blogs.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.dispenser.blogs.model.dto.TagsDTO;
import com.dispenser.blogs.model.entity.Tags;
import com.dispenser.blogs.model.qo.TagsQO;
import com.dispenser.blogs.model.vo.TagsVO;
import com.dispenser.blogs.common.ApiResult;
import com.github.pagehelper.PageInfo;
import java.util.List;
/**
 * 标签service
 */
public interface TagsService extends IService<Tags> {

	/**
     * 分页获取标签列表
     */
	ApiResult<PageInfo<TagsVO>> list(TagsQO qo);

	/**
     * 根据id查询标签
     */
	ApiResult<TagsVO> get(Long id);

	/**
     * 新增标签
     */
	ApiResult save(TagsDTO dto);

	/**
     * 修改标签
     */
	ApiResult update(TagsDTO dto);

	/**
     * 逻辑删除标签
     */
	ApiResult delete(Long id);

	/**
	* 批量删除（逻辑删除）
	*
	* @param idList
	*/
	ApiResult deleteAll(List<Long> idList);


	/**
	 * 索取所有数据
	 * @param qo
	 * @return
	 */
    ApiResult all(TagsQO qo);
}

