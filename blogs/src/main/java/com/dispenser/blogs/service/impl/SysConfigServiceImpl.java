package com.dispenser.blogs.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dispenser.blogs.common.ApiResult;
import com.dispenser.blogs.common.BeanUtil;
import com.dispenser.blogs.controller.CounterController;
import com.dispenser.blogs.dao.SysConfigDao;
import com.dispenser.blogs.model.dto.SysConfigDTO;
import com.dispenser.blogs.model.entity.SysConfig;
import com.dispenser.blogs.model.qo.SysConfigQO;
import com.dispenser.blogs.model.vo.SysConfigVO;
import com.dispenser.blogs.service.SysConfigService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static com.dispenser.blogs.common.ApiResultUtils.failureWithMsg;
import static com.dispenser.blogs.common.ApiResultUtils.success;

/**
 * 单据编码规则service实现类
 */
@Slf4j
@Service("sysConfigService")
@Transactional
public class SysConfigServiceImpl extends ServiceImpl<SysConfigDao, SysConfig> implements SysConfigService {

    private final Logger logger = LoggerFactory.getLogger(CounterController.class);

    private static final String MENU_NAME = "系统配置表";

	@Autowired
	SysConfigDao sysConfigDao;

	/**
	 * 分页获取系统配置表列表
	 */
	@Override
	public ApiResult<PageInfo<SysConfigVO>> list(SysConfigQO qo) {

        logger.info("分页获取请求参数--SysConfigQO：" + qo);

		PageHelper.startPage(qo.getPage(), qo.getLimit());

		SysConfigDTO dto=BeanUtil.copyProperties(qo, SysConfigDTO.class);
		List<SysConfig> list = sysConfigDao.queryAll(dto);

		PageInfo<SysConfigVO> pageInfo = BeanUtil.transferPageInfo(list, SysConfigVO.class);
        logger.info("返回结果：" + pageInfo);
		return success(pageInfo);
	}

	/**
	 * 根据id查询系统配置表
	 */
	@Override
	public ApiResult<SysConfigVO> get(Long id) {
        logger.info("根据id查询请求参数--id：" + id);

		SysConfig entity = sysConfigDao.queryById(id);
		if(entity==null){
            logger.info("当前系统配置表不存在");
			return failureWithMsg("当前系统配置表不存在");
		}
		SysConfigVO vo = BeanUtil.copyProperties(entity, SysConfigVO.class);

        logger.info("返回结果：" + vo);
		return success(vo);
	}

	/**
	 * 新增系统配置表
	 */
	@Override
	public ApiResult save(SysConfigDTO dto) {
        logger.info("新增请求参数--SysConfigDTO：" + dto);

		SysConfig entity = BeanUtil.copyProperties(dto, SysConfig.class);
		if(entity.getId() == null){
			if (sysConfigDao.saveEntity(entity)>0) {
				logger.info("返回结果：" + entity);
				return success(entity);
			}
		}else{
			if (sysConfigDao.updateById(entity)>0) {
				logger.info("返回结果：" + entity);
				return success(entity);
			}
		}
        logger.info("保存失败");
		return failureWithMsg("保存失败");
	}

	/**
	 * 修改系统配置表
	 */
	@Override
	public ApiResult update(SysConfigDTO dto) {
        logger.info("修改请求参数--SysConfigDTO：" + dto);

		SysConfig oldEntity = sysConfigDao.queryById(dto.getId());
		if(oldEntity==null){
            logger.info("当前系统配置表不存在");
			return failureWithMsg("当前系统配置表不存在");
		}

		SysConfig entity = BeanUtil.copyProperties(dto, SysConfig.class);

		if (sysConfigDao.updateById(entity)>0) {

            logger.info("返回结果："+entity);
			return success(entity);
		}
        logger.info("保存失败");
		return failureWithMsg("修改失败");
	}

	/**
	 * 逻辑删除系统配置表
	 */
	@Override
	public ApiResult delete(Long id) {
        logger.info("逻辑删除请求参数--id：" + id);

		SysConfig oldEntity = sysConfigDao.queryById(id);
		if(oldEntity==null){
            logger.info("当前系统配置表不存在");
			return failureWithMsg("当前系统配置表不存在");
		}

		if (sysConfigDao.deleteById(id)>0) {
            logger.info("删除成功");
			return success("删除成功");
		}

        logger.info("删除失败");
		return failureWithMsg("删除失败");
	}

	/**
	 * 批量删除（逻辑删除）
	 *
	 * @param idList
 	*/
	@Override
	public ApiResult deleteAll(List<Long> idList) {
        logger.info("批量删除（逻辑删除）请求参数--idList：" + idList);
		if (idList != null) {
			int isOk = 0;
			isOk = sysConfigDao.deleteAll(idList);   //删除数据
			if (isOk > 0) {
                logger.info("删除成功");
				return success("删除成功");
			}
		}

        logger.info("删除失败");
		return failureWithMsg("删除失败");
	}

}
