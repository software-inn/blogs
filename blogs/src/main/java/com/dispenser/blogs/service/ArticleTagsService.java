package com.dispenser.blogs.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.dispenser.blogs.model.dto.ArticleTagsDTO;
import com.dispenser.blogs.model.entity.ArticleTags;
import com.dispenser.blogs.model.qo.ArticleTagsQO;
import com.dispenser.blogs.model.vo.ArticleTagsVO;
import com.dispenser.blogs.common.ApiResult;
import com.github.pagehelper.PageInfo;
import java.util.List;
/**
 * 文章与标签中间表service
 */
public interface ArticleTagsService extends IService<ArticleTags> {

	/**
     * 分页获取文章与标签中间表列表
     */
	ApiResult<PageInfo<ArticleTagsVO>> list(ArticleTagsQO qo);

	/**
     * 根据id查询文章与标签中间表
     */
	ApiResult<ArticleTagsVO> get(Long id);

	/**
     * 新增文章与标签中间表
     */
	ApiResult save(ArticleTagsDTO dto);

	/**
     * 修改文章与标签中间表
     */
	ApiResult update(ArticleTagsDTO dto);

	/**
     * 逻辑删除文章与标签中间表
     */
	ApiResult delete(Long id);

	/**
	* 批量删除（逻辑删除）
	*
	* @param idList
	*/
	ApiResult deleteAll(List<Long> idList);
}

