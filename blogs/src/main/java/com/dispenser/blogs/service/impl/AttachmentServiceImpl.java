package com.dispenser.blogs.service.impl;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.dispenser.blogs.controller.CounterController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dispenser.blogs.dao.AttachmentDao;
import com.dispenser.blogs.model.dto.AttachmentDTO;
import com.dispenser.blogs.model.entity.Attachment;
import com.dispenser.blogs.model.entity.Attachment;
import com.dispenser.blogs.model.qo.AttachmentQO;
import com.dispenser.blogs.model.vo.AttachmentVO;
import com.dispenser.blogs.service.AttachmentService;
import com.dispenser.blogs.common.ApiResult;
import com.dispenser.blogs.common.BeanUtil;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static com.dispenser.blogs.common.ApiResultUtils.failureWithMsg;
import static com.dispenser.blogs.common.ApiResultUtils.success;

/**
 * 单据编码规则service实现类
 */
@Slf4j
@Service("attachmentService")
@Transactional
public class AttachmentServiceImpl extends ServiceImpl<AttachmentDao, Attachment> implements AttachmentService {

    private final Logger logger = LoggerFactory.getLogger(CounterController.class);

    private static final String MENU_NAME = "图片表";

	@Autowired
	AttachmentDao attachmentDao;

	/**
	 * 分页获取图片表列表
	 */
	@Override
	public ApiResult<PageInfo<AttachmentVO>> list(AttachmentQO qo) {

        logger.info("分页获取请求参数--AttachmentQO：" + qo);

		PageHelper.startPage(qo.getPage(), qo.getLimit());

		AttachmentDTO dto=BeanUtil.copyProperties(qo, AttachmentDTO.class);
		List<Attachment> list = attachmentDao.queryAll(dto);

		PageInfo<AttachmentVO> pageInfo = BeanUtil.transferPageInfo(list, AttachmentVO.class);
        logger.info("返回结果：" + pageInfo);
		return success(pageInfo);
	}

	/**
	 * 根据id查询图片表
	 */
	@Override
	public ApiResult<AttachmentVO> get(Long id) {
        logger.info("根据id查询请求参数--id：" + id);

		Attachment entity = attachmentDao.queryById(id);
		if(entity==null){
            logger.info("当前图片表不存在");
			return failureWithMsg("当前图片表不存在");
		}
		AttachmentVO vo = BeanUtil.copyProperties(entity, AttachmentVO.class);

        logger.info("返回结果：" + vo);
		return success(vo);
	}

	/**
	 * 新增图片表
	 */
	@Override
	public ApiResult save(AttachmentDTO dto) {
        logger.info("新增请求参数--AttachmentDTO：" + dto);

		Attachment entity = BeanUtil.copyProperties(dto, Attachment.class);

		if (attachmentDao.saveEntity(entity)>0) {
            logger.info("返回结果：" + entity);
			return success(entity);
		}
        logger.info("保存失败");
		return failureWithMsg("保存失败");
	}

	/**
	 * 修改图片表
	 */
	@Override
	public ApiResult update(AttachmentDTO dto) {
        logger.info("修改请求参数--AttachmentDTO：" + dto);

		Attachment oldEntity = attachmentDao.queryById(dto.getId());
		if(oldEntity==null){
            logger.info("当前图片表不存在");
			return failureWithMsg("当前图片表不存在");
		}

		Attachment entity = BeanUtil.copyProperties(dto, Attachment.class);

		if (attachmentDao.updateById(entity)>0) {

            logger.info("返回结果："+entity);
			return success(entity);
		}
        logger.info("保存失败");
		return failureWithMsg("修改失败");
	}

	/**
	 * 逻辑删除图片表
	 */
	@Override
	public ApiResult delete(Long id) {
        logger.info("逻辑删除请求参数--id：" + id);

		Attachment oldEntity = attachmentDao.queryById(id);
		if(oldEntity==null){
            logger.info("当前图片表不存在");
			return failureWithMsg("当前图片表不存在");
		}

		if (attachmentDao.deleteById(id)>0) {
            logger.info("删除成功");
			return success("删除成功");
		}

        logger.info("删除失败");
		return failureWithMsg("删除失败");
	}

	/**
	 * 批量删除（逻辑删除）
	 *
	 * @param idList
 	*/
	@Override
	public ApiResult deleteAll(List<Long> idList) {
        logger.info("批量删除（逻辑删除）请求参数--idList：" + idList);
		if (idList != null) {
			int isOk = 0;
			isOk = attachmentDao.deleteAll(idList);   //删除数据
			if (isOk > 0) {
                logger.info("删除成功");
				return success("删除成功");
			}
		}

        logger.info("删除失败");
		return failureWithMsg("删除失败");
	}

}
