package com.dispenser.blogs.service.impl;

import java.util.*;
import java.util.stream.Collectors;

import com.dispenser.blogs.controller.CounterController;
import com.dispenser.blogs.dao.ArticleClassDao;
import com.dispenser.blogs.dao.ArticleTagsDao;
import com.dispenser.blogs.dao.TagsDao;
import com.dispenser.blogs.model.entity.ArticleTags;
import com.dispenser.blogs.model.entity.Tags;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dispenser.blogs.dao.ArticleDao;
import com.dispenser.blogs.model.dto.ArticleDTO;
import com.dispenser.blogs.model.entity.Article;
import com.dispenser.blogs.model.qo.ArticleQO;
import com.dispenser.blogs.model.vo.ArticleVO;
import com.dispenser.blogs.service.ArticleService;
import com.dispenser.blogs.common.ApiResult;
import com.dispenser.blogs.common.BeanUtil;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static com.dispenser.blogs.common.ApiResultUtils.failureWithMsg;
import static com.dispenser.blogs.common.ApiResultUtils.success;

/**
 * 单据编码规则service实现类
 */
@Slf4j
@Service("articleService")
@Transactional
public class ArticleServiceImpl extends ServiceImpl<ArticleDao, Article> implements ArticleService {

    private final Logger logger = LoggerFactory.getLogger(CounterController.class);

    private static final String MENU_NAME = "文章";

	@Autowired
	ArticleDao articleDao;
	@Autowired
	TagsDao tagsDao;
	@Autowired
	ArticleClassDao articleClassDao;
	@Autowired
	ArticleTagsDao articleTagsDao;

	/**
	 * 分页获取文章列表
	 */
	@Override
	public ApiResult<PageInfo<ArticleVO>> list(ArticleQO qo) {

        logger.info("分页获取请求参数--ArticleQO：" + qo);

		PageHelper.startPage(qo.getPage(), qo.getLimit());

		ArticleDTO dto=BeanUtil.copyProperties(qo, ArticleDTO.class);
		List<Article> list = articleDao.queryAll(dto);

		PageInfo<ArticleVO> pageInfo = BeanUtil.transferPageInfo(list, ArticleVO.class);
        logger.info("返回结果：" + pageInfo);
		return success(pageInfo);
	}

	/**
	 * 根据id查询文章
	 */
	@Override
	public ApiResult<ArticleVO> get(Long id) {
        logger.info("根据id查询请求参数--id：" + id);

		Article entity = articleDao.queryById(id);
		if(entity==null){
            logger.info("当前文章不存在");
			return failureWithMsg("当前文章不存在");
		}
		entity.setArticleClass(articleClassDao.queryById(entity.getClassId()));
		entity.setTagsList(tagsDao.queryByArticleId(entity.getId()));
		ArticleVO vo = BeanUtil.copyProperties(entity, ArticleVO.class);

        logger.info("返回结果：" + vo);
		return success(vo);
	}

	/**
	 * 新增文章
	 */
	@Override
	public ApiResult save(ArticleDTO dto) {
        logger.info("新增请求参数--ArticleDTO：" + dto);

		Article entity = BeanUtil.copyProperties(dto, Article.class);
		entity.setCoverImageId(dto.getAttachment()==null?null:dto.getAttachment().getId());
		if (articleDao.saveEntity(entity)<=0) {
			logger.info("保存失败");
			return failureWithMsg("保存失败");
		}
		List<Tags> tagsList = entity.getTagsList();
		if(tagsList == null || tagsList.size() <= 0){
			logger.info("返回结果：" + entity);
			return success(entity);
		}
		Date date = new Date();
		List<ArticleTags> articleTagsList = new ArrayList<>();
		tagsList.forEach(e->{
			ArticleTags articleTags = new ArticleTags();
			articleTags.setDelState(0);
			articleTags.setAddTime(date);
			articleTags.setArticleId(entity.getId());
			articleTags.setTagsId(e.getId());
			articleTagsList.add(articleTags);
		});
		articleTagsDao.insertBatch(articleTagsList);
		logger.info("返回结果：" + entity);
		return success(entity);
	}

	/**
	 * 修改文章
	 */
	@Override
	public ApiResult update(ArticleDTO dto) {
        logger.info("修改请求参数--ArticleDTO：" + dto);

		Article oldEntity = articleDao.queryById(dto.getId());
		if(oldEntity==null){
            logger.info("当前文章不存在");
			return failureWithMsg("当前文章不存在");
		}

		Article entity = BeanUtil.copyProperties(dto, Article.class);
		entity.setCoverImageId(dto.getAttachment()==null?null:dto.getAttachment().getId());
		if (articleDao.updateById(entity)<=0) {
			logger.info("修改失败");
			return failureWithMsg("保存失败");
		}
		articleTagsDao.deleteByArticleId(entity.getId());
		List<Tags> tagsList = entity.getTagsList();
		if(tagsList == null || tagsList.size() <= 0){
			logger.info("返回结果：" + entity);
			return success(entity);
		}
		Date date = new Date();
		List<ArticleTags> articleTagsList = new ArrayList<>();
		tagsList.forEach(e->{
			ArticleTags articleTags = new ArticleTags();
			articleTags.setDelState(0);
			articleTags.setAddTime(date);
			articleTags.setArticleId(entity.getId());
			articleTags.setTagsId(e.getId());
			articleTagsList.add(articleTags);
		});
		articleTagsDao.insertBatch(articleTagsList);
		logger.info("返回结果：" + entity);
		return success(entity);
	}

	/**
	 * 逻辑删除文章
	 */
	@Override
	public ApiResult delete(Long id) {
        logger.info("逻辑删除请求参数--id：" + id);

		Article oldEntity = articleDao.queryById(id);
		if(oldEntity==null){
            logger.info("当前文章不存在");
			return failureWithMsg("当前文章不存在");
		}

		if (articleDao.deleteById(id)>0) {
			articleTagsDao.deleteByArticleId(oldEntity.getId());
            logger.info("删除成功");
			return success("删除成功");
		}

        logger.info("删除失败");
		return failureWithMsg("删除失败");
	}

	/**
	 * 批量删除（逻辑删除）
	 *
	 * @param idList
 	*/
	@Override
	public ApiResult deleteAll(List<Long> idList) {
        logger.info("批量删除（逻辑删除）请求参数--idList：" + idList);
		if (idList != null) {
			int isOk = 0;
			isOk = articleDao.deleteAll(idList);   //删除数据
			if (isOk > 0) {
				idList.forEach(e->{
					articleTagsDao.deleteByArticleId(e);
				});
                logger.info("删除成功");
				return success("删除成功");
			}
		}

        logger.info("删除失败");
		return failureWithMsg("删除失败");
	}

}
