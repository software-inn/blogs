package com.dispenser.blogs.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.dispenser.blogs.common.ApiResult;
import com.dispenser.blogs.model.dto.AttachmentDTO;
import com.dispenser.blogs.model.entity.Attachment;
import com.dispenser.blogs.model.qo.AttachmentQO;
import com.dispenser.blogs.model.vo.AttachmentVO;
import com.github.pagehelper.PageInfo;

import java.util.List;
/**
 * 图片表service
 */
public interface AttachmentService extends IService<Attachment> {

	/**
     * 分页获取图片表列表
     */
	ApiResult<PageInfo<AttachmentVO>> list(AttachmentQO qo);

	/**
     * 根据id查询图片表
     */
	ApiResult<AttachmentVO> get(Long id);

	/**
     * 新增图片表
     */
	ApiResult save(AttachmentDTO dto);

	/**
     * 修改图片表
     */
	ApiResult update(AttachmentDTO dto);

	/**
     * 逻辑删除图片表
     */
	ApiResult delete(Long id);

	/**
	* 批量删除（逻辑删除）
	*
	* @param idList
	*/
	ApiResult deleteAll(List<Long> idList);
}

