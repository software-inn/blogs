package com.dispenser.blogs.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.dispenser.blogs.model.dto.ArticleClassDTO;
import com.dispenser.blogs.model.entity.ArticleClass;
import com.dispenser.blogs.model.qo.ArticleClassQO;
import com.dispenser.blogs.model.vo.ArticleClassVO;
import com.dispenser.blogs.common.ApiResult;
import com.github.pagehelper.PageInfo;
import java.util.List;
/**
 * 分类service
 */
public interface ArticleClassService extends IService<ArticleClass> {

	/**
     * 分页获取分类列表
     */
	ApiResult<PageInfo<ArticleClassVO>> list(ArticleClassQO qo);

	/**
     * 根据id查询分类
     */
	ApiResult<ArticleClassVO> get(Long id);

	/**
     * 新增分类
     */
	ApiResult save(ArticleClassDTO dto);

	/**
     * 修改分类
     */
	ApiResult update(ArticleClassDTO dto);

	/**
     * 逻辑删除分类
     */
	ApiResult delete(Long id);

	/**
	* 批量删除（逻辑删除）
	*
	* @param idList
	*/
	ApiResult deleteAll(List<Long> idList);

	/**
	 * 获取分类所有数据
	 */
    ApiResult getAll(ArticleClassQO qo);
}

