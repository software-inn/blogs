package com.dispenser.blogs.exception;


import com.dispenser.blogs.common.ApiResult;
import com.dispenser.blogs.common.ApiResultUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.NoHandlerFoundException;

/**
 * 异常处理器
 *
 * @author Mark sunlightcs@gmail.com
 */
@RestControllerAdvice
public class RRExceptionHandler {
	private Logger logger = LoggerFactory.getLogger(getClass());

	/**
	 * 处理自定义异常
	 */
	@ExceptionHandler(RRException.class)
	public ApiResult handleRRException(RRException e){
		return ApiResultUtils.failure(e.getCode(),e.getMsg());
	}

	@ExceptionHandler(NoHandlerFoundException.class)
	public ApiResult handlerNoFoundException(Exception e) {
		logger.error(e.getMessage(), e);
		return ApiResultUtils.failureWithMsg("路径不存在，请检查路径是否正确");
	}

	@ExceptionHandler(DuplicateKeyException.class)
	public ApiResult handleDuplicateKeyException(DuplicateKeyException e){
		logger.error(e.getMessage(), e);
		return ApiResultUtils.failureWithMsg("数据库中已存在该记录");
	}

	@ExceptionHandler(Exception.class)
	public ApiResult handleException(Exception e){
		logger.error(e.getMessage(), e);
		return ApiResultUtils.failure();
	}
}
